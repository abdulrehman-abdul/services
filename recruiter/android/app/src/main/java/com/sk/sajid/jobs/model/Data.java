
package com.sk.sajid.jobs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {


    @SerializedName("accessToken")
    @Expose
    private String accessToken;
    @SerializedName("refreshToken")
    @Expose
    private String refreshToken;
    @SerializedName("authorizeToken")
    @Expose
    private String authorizeToken;
    @SerializedName("clientId")
    @Expose
    private String clientId;
    @SerializedName("callbackUrl")
    @Expose
    private Object callbackUrl;
    @SerializedName("principal")
    @Expose
    private Object principal;
    @SerializedName("roles")
    @Expose
    private Object roles;
    @SerializedName("accessTokenCreationTime")
    @Expose
    private long accessTokenCreationTime;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("client")
    @Expose
    private Object client;
    @SerializedName("attributes")
    @Expose
    private Object attributes;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAuthorizeToken() {
        return authorizeToken;
    }

    public void setAuthorizeToken(String authorizeToken) {
        this.authorizeToken = authorizeToken;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Object getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(Object callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public Object getPrincipal() {
        return principal;
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    public Object getRoles() {
        return roles;
    }

    public void setRoles(Object roles) {
        this.roles = roles;
    }

    public long getAccessTokenCreationTime() {
        return accessTokenCreationTime;
    }

    public void setAccessTokenCreationTime(long accessTokenCreationTime) {
        this.accessTokenCreationTime = accessTokenCreationTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Object getClient() {
        return client;
    }

    public void setClient(Object client) {
        this.client = client;
    }

    public Object getAttributes() {
        return attributes;
    }

    public void setAttributes(Object attributes) {
        this.attributes = attributes;
    }

}
