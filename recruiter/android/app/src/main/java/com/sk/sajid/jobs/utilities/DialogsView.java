package com.sk.sajid.jobs.utilities;
/*
 *  @auther Hussnain 1/27/19
 * */

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sk.sajid.jobs.R;

public class DialogsView {

    public static void showDialog(Context context, String message, Boolean isShownSuccess) {

        if (context != null && message != null) {


            final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            View dialogView = View.inflate(context, R.layout.dialog_status, null);
            dialogBuilder.setView(dialogView);
            dialogBuilder.setCancelable(false);

            final AlertDialog alertDialog = dialogBuilder.create();
            if (context instanceof Activity && !((Activity) context).isFinishing()) {
                alertDialog.show();
            }

            TextView msg = (TextView) dialogView.findViewById(R.id.message);
            msg.setText(message);

            //
            ImageView imageView = dialogView.findViewById(R.id.status_imageView);
            if (isShownSuccess) {
                imageView.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_happy));
            }


            dialogView.findViewById(R.id.continues).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    alertDialog.dismiss();
                }
            });

        }// if
    } // showDialog
}
