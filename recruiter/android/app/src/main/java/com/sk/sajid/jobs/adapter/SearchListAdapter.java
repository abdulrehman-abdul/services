package com.sk.sajid.jobs.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.model.applyjob.Data;
import com.sk.sajid.jobs.tools.Tools;

import java.util.List;


/**
 * @author Hussnain on 1/26/19
 */

public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.MyViewHolder> {
    private Context mContext;
    private List<Data> searchResultList;

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView cnicValue, phoneNumberValue, qualificationValue, tradeValue, seenStatusValue;
        private Button viewMoreBtn;
        private ImageView seenImageView;

        MyViewHolder(View view) {
            super(view);
            tradeValue = view.findViewById(R.id.tradeValue);
            cnicValue = view.findViewById(R.id.cnicValue);
            phoneNumberValue = view.findViewById(R.id.phoneNumberValue);
            qualificationValue = view.findViewById(R.id.qualificationValue);
            viewMoreBtn = view.findViewById(R.id.viewMoreBtn);

            seenImageView = view.findViewById(R.id.seenImageView);
            seenStatusValue = view.findViewById(R.id.seenStatusValue);
        }

    }

    public SearchListAdapter(Context mContext, List<Data> searchResultList) {
        this.mContext = mContext;
        this.searchResultList = searchResultList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_result_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {
            Data dataObject = searchResultList.get(position);
            holder.tradeValue.setText(dataObject.getTrade());
            holder.cnicValue.setText(String.valueOf(dataObject.getCnic()));
            holder.phoneNumberValue.setText(String.valueOf(dataObject.getPhoneNum()));
            holder.qualificationValue.setText(String.valueOf(dataObject.getQualification()));

            if (new Tools().hasValue(dataObject.getSeenBy())) {
                holder.seenImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_checked));
                holder.seenStatusValue.setText(mContext.getString(R.string.seen));
            } else {

                holder.seenImageView.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_unseen));
                holder.seenStatusValue.setText(mContext.getString(R.string.unseen));
            }


            holder.viewMoreBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return searchResultList.size();
    }
}

