package com.sk.sajid.jobs.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotificationsData {


    public static final String token = "token";
    public static final String client_id = "client_id";

    @SerializedName("type")
    private float type;

    @SerializedName("message")
    private String message;

    @SerializedName("revisionNo")
    private float revisionNo;

    @SerializedName("errorCode")
    private String errorCode;

    @SerializedName("_explicitType")
    private String _explicitType;

    @SerializedName("data")
    ArrayList<DataObject> data = new ArrayList<DataObject>();

    public float getType() {
        return type;
    }

    public void setType(float type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public float getRevisionNo() {
        return revisionNo;
    }

    public void setRevisionNo(float revisionNo) {
        this.revisionNo = revisionNo;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String get_explicitType() {
        return _explicitType;
    }

    public void set_explicitType(String _explicitType) {
        this._explicitType = _explicitType;
    }

    public ArrayList<DataObject> getData() {
        return data;
    }

    public void setData(ArrayList<DataObject> data) {
        this.data = data;
    }

    public static class DataObject {

        private float id;
        private long created_at;
        private long updated_at;
        private String name;
        private float age;
        private float height;
        private float date_of_birth;
        private float weight;
        private float phone_num;
        private float english_speaking_level;
        private float english_writing_level;
        private float english_listening_level;
        private float cnic;
        private String trade;
        private String qualification;
        private String city;
        private String country;
        private String email;
        private String seen_by;
        private float passport;
        private String passport_expiry = null;
        private String overseas_experience = null;
        private String local_experience = null;
        private float second_phone_num;
        private float social_media_interaction;

        public float getId() {
            return id;
        }

        public void setId(float id) {
            this.id = id;
        }

        public long getCreated_at() {
            return created_at;
        }

        public void setCreated_at(long created_at) {
            this.created_at = created_at;
        }

        public long getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(long updated_at) {
            this.updated_at = updated_at;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public float getAge() {
            return age;
        }

        public void setAge(float age) {
            this.age = age;
        }

        public float getHeight() {
            return height;
        }

        public void setHeight(float height) {
            this.height = height;
        }

        public float getDate_of_birth() {
            return date_of_birth;
        }

        public void setDate_of_birth(float date_of_birth) {
            this.date_of_birth = date_of_birth;
        }

        public float getWeight() {
            return weight;
        }

        public void setWeight(float weight) {
            this.weight = weight;
        }

        public float getPhone_num() {
            return phone_num;
        }

        public void setPhone_num(float phone_num) {
            this.phone_num = phone_num;
        }

        public float getEnglish_speaking_level() {
            return english_speaking_level;
        }

        public void setEnglish_speaking_level(float english_speaking_level) {
            this.english_speaking_level = english_speaking_level;
        }

        public float getEnglish_writing_level() {
            return english_writing_level;
        }

        public void setEnglish_writing_level(float english_writing_level) {
            this.english_writing_level = english_writing_level;
        }

        public float getEnglish_listening_level() {
            return english_listening_level;
        }

        public void setEnglish_listening_level(float english_listening_level) {
            this.english_listening_level = english_listening_level;
        }

        public float getCnic() {
            return cnic;
        }

        public void setCnic(float cnic) {
            this.cnic = cnic;
        }

        public String getTrade() {
            return trade;
        }

        public void setTrade(String trade) {
            this.trade = trade;
        }

        public String getQualification() {
            return qualification;
        }

        public void setQualification(String qualification) {
            this.qualification = qualification;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getSeen_by() {
            return seen_by;
        }

        public void setSeen_by(String seen_by) {
            this.seen_by = seen_by;
        }

        public float getPassport() {
            return passport;
        }

        public void setPassport(float passport) {
            this.passport = passport;
        }

        public String getPassport_expiry() {
            return passport_expiry;
        }

        public void setPassport_expiry(String passport_expiry) {
            this.passport_expiry = passport_expiry;
        }

        public String getOverseas_experience() {
            return overseas_experience;
        }

        public void setOverseas_experience(String overseas_experience) {
            this.overseas_experience = overseas_experience;
        }

        public String getLocal_experience() {
            return local_experience;
        }

        public void setLocal_experience(String local_experience) {
            this.local_experience = local_experience;
        }

        public float getSecond_phone_num() {
            return second_phone_num;
        }

        public void setSecond_phone_num(float second_phone_num) {
            this.second_phone_num = second_phone_num;
        }

        public float getSocial_media_interaction() {
            return social_media_interaction;
        }

        public void setSocial_media_interaction(float social_media_interaction) {
            this.social_media_interaction = social_media_interaction;
        }
    }
}
