package com.sk.sajid.jobs.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.model.Jobs;
import com.sk.sajid.jobs.model.JobsData;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by H.A.R on 1/26/2018.
 */

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.MyViewHolder> {
    private Context mContext;
    private List<JobsData> jobsArrayList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCompanyName, tvSalary, tvLocation;
        public TextView tvName;

        public MyViewHolder(View view) {
            super(view);
            tvName = view.findViewById(R.id.tvName);
            tvCompanyName = view.findViewById(R.id.tvCompanyName);
            tvSalary = view.findViewById(R.id.tvSalary);
            tvLocation = view.findViewById(R.id.tvLocation);
        }

    }

    public JobsAdapter(Context mContext, List<JobsData> jobsArrayList) {
        this.mContext = mContext;
        this.jobsArrayList = jobsArrayList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.jobs_adapter_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        JobsData jobs = jobsArrayList.get(position);
        holder.tvName.setText(jobs.getTitle());
        holder.tvCompanyName.setText(jobs.getCompany());
        holder.tvSalary.setText(jobs.getSalary());
        holder.tvLocation.setText(jobs.getLocation());
    }

    @Override
    public int getItemCount() {
        return jobsArrayList.size();
    }
}

