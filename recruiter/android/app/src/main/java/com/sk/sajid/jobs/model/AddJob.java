
package com.sk.sajid.jobs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddJob {

    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("revisionNo")
    @Expose
    private Integer revisionNo;
    @SerializedName("errorCode")
    @Expose
    private String errorCode;
    @SerializedName("_explicitType")
    @Expose
    private String explicitType;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getRevisionNo() {
        return revisionNo;
    }

    public void setRevisionNo(Integer revisionNo) {
        this.revisionNo = revisionNo;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getExplicitType() {
        return explicitType;
    }

    public void setExplicitType(String explicitType) {
        this.explicitType = explicitType;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
