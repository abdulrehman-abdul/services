package com.sk.sajid.jobs.model.searchjob;

import android.os.Parcel;
import android.os.Parcelable;

/*
 *  @auther Hussnain 1/26/19
 * */
public class SearchJobRequest implements Parcelable {

    private int age;
    private String trade;
    private int english_writing_level;
    private int phone_num = 0;
    private String seen_by;
    private int height;
    private String qualification;
    private int cnic = 0;
    private int english_speaking_level;

    protected SearchJobRequest(Parcel in) {
        age = in.readInt();
        trade = in.readString();
        english_writing_level = in.readInt();
        phone_num = in.readInt();
        seen_by = in.readString();
        height = in.readInt();
        qualification = in.readString();
        cnic = in.readInt();
        english_speaking_level = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(age);
        dest.writeString(trade);
        dest.writeInt(english_writing_level);
        dest.writeInt(phone_num);
        dest.writeString(seen_by);
        dest.writeInt(height);
        dest.writeString(qualification);
        dest.writeInt(cnic);
        dest.writeInt(english_speaking_level);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchJobRequest> CREATOR = new Creator<SearchJobRequest>() {
        @Override
        public SearchJobRequest createFromParcel(Parcel in) {
            return new SearchJobRequest(in);
        }

        @Override
        public SearchJobRequest[] newArray(int size) {
            return new SearchJobRequest[size];
        }
    };

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public int getEnglish_writing_level() {
        return english_writing_level;
    }

    public void setEnglish_writing_level(int english_writing_level) {
        this.english_writing_level = english_writing_level;
    }

    public int getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(int phone_num) {
        this.phone_num = phone_num;
    }

    public String getSeen_by() {
        return seen_by;
    }

    public void setSeen_by(String seen_by) {
        this.seen_by = seen_by;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public int getCnic() {
        return cnic;
    }

    public void setCnic(int cnic) {
        this.cnic = cnic;
    }

    public int getEnglish_speaking_level() {
        return english_speaking_level;
    }

    public void setEnglish_speaking_level(int english_speaking_level) {
        this.english_speaking_level = english_speaking_level;
    }


    public SearchJobRequest(int age, String trade, int english_writing_level, int phone_num, String seen_by, int height, String qualification, int cnic, int english_speaking_level) {
        this.age = age;
        this.trade = trade;
        this.english_writing_level = english_writing_level;
        this.phone_num = phone_num;
        this.seen_by = seen_by;
        this.height = height;
        this.qualification = qualification;
        this.cnic = cnic;
        this.english_speaking_level = english_speaking_level;
    }
}
