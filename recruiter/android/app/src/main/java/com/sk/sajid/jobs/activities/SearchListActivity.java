package com.sk.sajid.jobs.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.adapter.SearchListAdapter;
import com.sk.sajid.jobs.model.applyjob.Data;

import java.util.ArrayList;

public class SearchListActivity extends AppCompatActivity {

    private RecyclerView searchResultRecycler;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_list);

        searchResultRecycler = findViewById(R.id.searchResultRecycler);
        back = findViewById(R.id.back);

        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("list")) {

            Bundle bundle = getIntent().getExtras();
            ArrayList<Data> searchListlist = bundle.getParcelableArrayList("list");

            initialzeAdapter(searchListlist);

            back.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    finish();
                }
            });

        }

    }

    private void initialzeAdapter(ArrayList<Data> searchListlist) {

        SearchListAdapter jobsAdapter = new SearchListAdapter(this, searchListlist);
        searchResultRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        searchResultRecycler.setAdapter(jobsAdapter);
    }
}
