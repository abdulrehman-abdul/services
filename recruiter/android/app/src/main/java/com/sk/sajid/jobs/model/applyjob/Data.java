package com.sk.sajid.jobs.model.applyjob;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 *  @auther Hussnain 1/26/19
 * */
public class Data implements Parcelable {
    private String country;
    private String city;
    @SerializedName("date_of_birth")
    @Expose
    private long dateOfBirth;
    @SerializedName("created_at")
    @Expose
    private long createdAt;
    @SerializedName("overseas_experience")
    @Expose
    private String overseasExperience;
    @SerializedName("seen_by")
    @Expose
    private String seenBy;
    @SerializedName("updated_at")
    @Expose
    private long updatedAt;
    private int passport;
    @SerializedName("phone_num")
    @Expose
    private int phoneNum;
    private int id;
    private String email;
    private int height;
    @SerializedName("english_speaking_level")
    @Expose
    private int englishSpeakingLevel;
    private int weight;
    private int cnic;
    @SerializedName("second_phone_num")
    @Expose
    private int secondPhoneNum;
    @SerializedName("social_media_interaction")
    @Expose
    private int socialMediaInteraction;
    private String qualification;
    @SerializedName("passport_expiry")
    @Expose
    private int passportExpiry;
    private String trade;
    @SerializedName("local_experience")
    @Expose
    private String localExperience;
    @SerializedName("english_listening_level")
    @Expose
    private int englishListeningLevel;
    private String name;
    @SerializedName("english_writing_level")
    @Expose
    private int englishWritingLevel;

    protected Data(Parcel in) {
        country = in.readString();
        city = in.readString();
        dateOfBirth = in.readLong();
        createdAt = in.readLong();
        overseasExperience = in.readString();
        seenBy = in.readString();
        updatedAt = in.readLong();
        passport = in.readInt();
        phoneNum = in.readInt();
        id = in.readInt();
        email = in.readString();
        height = in.readInt();
        englishSpeakingLevel = in.readInt();
        weight = in.readInt();
        cnic = in.readInt();
        secondPhoneNum = in.readInt();
        socialMediaInteraction = in.readInt();
        qualification = in.readString();
        passportExpiry = in.readInt();
        trade = in.readString();
        localExperience = in.readString();
        englishListeningLevel = in.readInt();
        name = in.readString();
        englishWritingLevel = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(country);
        dest.writeString(city);
        dest.writeLong(dateOfBirth);
        dest.writeLong(createdAt);
        dest.writeString(overseasExperience);
        dest.writeString(seenBy);
        dest.writeLong(updatedAt);
        dest.writeInt(passport);
        dest.writeInt(phoneNum);
        dest.writeInt(id);
        dest.writeString(email);
        dest.writeInt(height);
        dest.writeInt(englishSpeakingLevel);
        dest.writeInt(weight);
        dest.writeInt(cnic);
        dest.writeInt(secondPhoneNum);
        dest.writeInt(socialMediaInteraction);
        dest.writeString(qualification);
        dest.writeInt(passportExpiry);
        dest.writeString(trade);
        dest.writeString(localExperience);
        dest.writeInt(englishListeningLevel);
        dest.writeString(name);
        dest.writeInt(englishWritingLevel);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Data> CREATOR = new Creator<Data>() {
        @Override
        public Data createFromParcel(Parcel in) {
            return new Data(in);
        }

        @Override
        public Data[] newArray(int size) {
            return new Data[size];
        }
    };

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getOverseasExperience() {
        return overseasExperience;
    }

    public void setOverseasExperience(String overseasExperience) {
        this.overseasExperience = overseasExperience;
    }

    public String getSeenBy() {
        return seenBy;
    }

    public void setSeenBy(String seenBy) {
        this.seenBy = seenBy;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getPassport() {
        return passport;
    }

    public void setPassport(int passport) {
        this.passport = passport;
    }

    public int getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(int phoneNum) {
        this.phoneNum = phoneNum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getEnglishSpeakingLevel() {
        return englishSpeakingLevel;
    }

    public void setEnglishSpeakingLevel(int englishSpeakingLevel) {
        this.englishSpeakingLevel = englishSpeakingLevel;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCnic() {
        return cnic;
    }

    public void setCnic(int cnic) {
        this.cnic = cnic;
    }

    public int getSecondPhoneNum() {
        return secondPhoneNum;
    }

    public void setSecondPhoneNum(int secondPhoneNum) {
        this.secondPhoneNum = secondPhoneNum;
    }

    public int getSocialMediaInteraction() {
        return socialMediaInteraction;
    }

    public void setSocialMediaInteraction(int socialMediaInteraction) {
        this.socialMediaInteraction = socialMediaInteraction;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public int getPassportExpiry() {
        return passportExpiry;
    }

    public void setPassportExpiry(int passportExpiry) {
        this.passportExpiry = passportExpiry;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getLocalExperience() {
        return localExperience;
    }

    public void setLocalExperience(String localExperience) {
        this.localExperience = localExperience;
    }

    public int getEnglishListeningLevel() {
        return englishListeningLevel;
    }

    public void setEnglishListeningLevel(int englishListeningLevel) {
        this.englishListeningLevel = englishListeningLevel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnglishWritingLevel() {
        return englishWritingLevel;
    }

    public void setEnglishWritingLevel(int englishWritingLevel) {
        this.englishWritingLevel = englishWritingLevel;
    }

    public Data(String country, String city, long dateOfBirth, long createdAt, String overseasExperience, String seenBy, long updatedAt, int passport, int phoneNum, int id, String email, int height, int englishSpeakingLevel, int weight, int cnic, int secondPhoneNum, int socialMediaInteraction, String qualification, int passportExpiry, String trade, String localExperience, int englishListeningLevel, String name) {
        this.country = country;
        this.city = city;
        this.dateOfBirth = dateOfBirth;
        this.createdAt = createdAt;
        this.overseasExperience = overseasExperience;
        this.seenBy = seenBy;
        this.updatedAt = updatedAt;
        this.passport = passport;
        this.phoneNum = phoneNum;
        this.id = id;
        this.email = email;
        this.height = height;
        this.englishSpeakingLevel = englishSpeakingLevel;
        this.weight = weight;
        this.cnic = cnic;
        this.secondPhoneNum = secondPhoneNum;
        this.socialMediaInteraction = socialMediaInteraction;
        this.qualification = qualification;
        this.passportExpiry = passportExpiry;
        this.trade = trade;
        this.localExperience = localExperience;
        this.englishListeningLevel = englishListeningLevel;
        this.name = name;
    }
}
