package com.sk.sajid.jobs.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.model.ManageSearch;
import com.sk.sajid.jobs.model.applyjob.ApplyJobRequest;
import com.sk.sajid.jobs.retrofit.ApiClient;
import com.sk.sajid.jobs.retrofit.ApiInterface;
import com.sk.sajid.jobs.tools.TempData;
import com.sk.sajid.jobs.tools.Tools;
import com.sk.sajid.jobs.utilities.Common;
import com.sk.sajid.jobs.utilities.DialogsView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManageSearchResults extends AppCompatActivity implements View.OnClickListener {

    private ManageSearch manageSearch;

    private EditText etCnic, etName, etPhone, etHeight, etQulification, etSpeeking, etWriting, etTrade;
    private Button btnTrade, btnSpeeking, btnWriting, btnPhone;

    private Spinner spinnerTrade, ageSpinnner, heightSpinnner, qualificationSpinnner, speakingSpinnner, writingSpinnner;


    private Button btnHeight, btnQualification;
    private MaterialSpinner spinnerHeight, spinnerAge, spinnerQualification, spinnerSpeeking, spinnerWriting, spinnerPhone;


    private Button btnApply;
    private ArrayList<String> tradeArray, ageArray, heightArray, qualificationArray, speekingArray, writingArray;

    private RelativeLayout rlBirthWrapper;


    private ApplyJobRequest applyJobRequest;

    private ProgressBar progressBar;

    private String seenUnSeen = "";

    private RadioButton radioButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_search_results);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        manageSearch = new ManageSearch();
        initArrays();

        init();
        initializeSpinnerAdapters();

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    seenUnSeen = "UNSEEN";
                }
            }
        });


    }


    private void initArrays() {
        tradeArray = new ArrayList<>();
        heightArray = new ArrayList<>();
        ageArray = new ArrayList<>();
        qualificationArray = new ArrayList<>();
        speekingArray = new ArrayList<>();
        writingArray = new ArrayList<>();
        addDataInArrays();
    }

    private void addDataInArrays() {
        tradeArray.add("Select Trade");
        tradeArray.add("Security Guard");
        tradeArray.add("Air Craft Cleaner");
        qualificationArray.add("");
    }

    private void init() {

        radioButton = findViewById(R.id.unseen);

        progressBar = findViewById(R.id.progressBar);


        etCnic = findViewById(R.id.etCnic);
        etName = findViewById(R.id.etName);

        qualificationSpinnner = findViewById(R.id.qualificationSpinnner);
        speakingSpinnner = findViewById(R.id.speakingSpinnner);
        writingSpinnner = findViewById(R.id.writingSpinnner);
        heightSpinnner = findViewById(R.id.heightSpinnner);
        ageSpinnner = findViewById(R.id.ageSpinnner);
        etPhone = findViewById(R.id.etPhone);


        // spinners
        spinnerTrade = findViewById(R.id.etTradeSpin);
        spinnerHeight = findViewById(R.id.spinnerHeight);
        spinnerQualification = findViewById(R.id.spinnerQualification);
        spinnerSpeeking = findViewById(R.id.spinnerSpeeking);
        spinnerWriting = findViewById(R.id.spinnerWriting);
        spinnerAge = findViewById(R.id.spinnerAge);


        btnApply = findViewById(R.id.btnSearch);

        btnApply.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSearch: {

                if (isFormValid()) {

                    progressBar.setVisibility(View.VISIBLE);


                    ApiInterface apiInterface = ApiClient.getInstance().getClient().create(ApiInterface.class);
                    Call<ManageSearch> call = apiInterface.manageSearch(Common.token, Common.client_id, manageSearch);
                    call.enqueue(new Callback<ManageSearch>() {
                        @Override
                        public void onResponse(Call<ManageSearch> call, Response<ManageSearch> response) {

                            progressBar.setVisibility(View.GONE);
                            if (response.isSuccessful() && response.body() != null) {
                                //To show ProgressBar
                                /* Log.d("t", "##\n" + response.body() + ""); */
                                Gson gson = new Gson();
                                Toast.makeText(ManageSearchResults.this, "true responseee" + "\n" + gson.toJson(response.body()), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(ManageSearchResults.this, getString(R.string.failure_message_api), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ManageSearch> call, Throwable t) {

                            progressBar.setVisibility(View.GONE);  //To show ProgressBar
                            //   Log.d("t", t + "");
                            Toast.makeText(ManageSearchResults.this, getString(R.string.failure_message_api), Toast.LENGTH_SHORT).show();

                        }


                    });


                } else {

                    DialogsView.showDialog(ManageSearchResults.this, getString(R.string.failure_message_validations), false);
                }
                break;
            }

        }
    }

    private void initializeSpinnerAdapters() {

        initializeTradAdapter();
        initializeQualificationAdapter();
        initializeHeightSpinnnerAdapter();
        initializeSpeakingSpinnnerAdapter();
        initializeWritingSpinnnerAdapter();
        initializeAgeSpinnnerAdapter();

    }


    private void initializeWritingSpinnnerAdapter() {
        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(ManageSearchResults.this, android.R.layout.simple_list_item_1, tempData.selectedArrayList(3));
        writingSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeSpeakingSpinnnerAdapter() {

        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(ManageSearchResults.this, android.R.layout.simple_list_item_1, tempData.selectedArrayList(2));
        speakingSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeQualificationAdapter() {

        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(ManageSearchResults.this, android.R.layout.simple_list_item_1, tempData.selectedArrayList(1));
        qualificationSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeHeightSpinnnerAdapter() {

        List<String> heightList = new ArrayList<String>();

        heightList.add("Select Height");
        for (int i = 1; i <= 101; i++) {
            heightList.add(String.valueOf(i));
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(ManageSearchResults.this, android.R.layout.simple_list_item_1, heightList);
        heightSpinnner.setAdapter(itemsAdapter);

    }

    private void initializeAgeSpinnnerAdapter() {

        List<String> heightList = new ArrayList<String>();

        heightList.add("Select Age");
        for (int i = 18; i <= 30; i++) {
            heightList.add(String.valueOf(i));
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(ManageSearchResults.this, android.R.layout.simple_list_item_1, heightList);
        ageSpinnner.setAdapter(itemsAdapter);

    }

    private void initializeTradAdapter() {
        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(ManageSearchResults.this, android.R.layout.simple_list_item_1, tempData.selectedArrayList(0));
        spinnerTrade.setAdapter(itemsAdapter);

    }

    private boolean isValidString(EditText field) {

        if (field.getText().toString().isEmpty()) {
            return false;
        }
        return true;
    }

    private boolean isValidInt(EditText field) {

        if (Integer.parseInt(field.getText().toString()) < 0) {
            return false;
        }
        return true;
    }

    private boolean isFormValid() {
        Boolean isFormValid = false;
        Tools tools = new Tools();


        if (!(spinnerTrade.getSelectedItem().toString().equalsIgnoreCase("Select Trade"))
                && tools.hasValue(etCnic.getText().toString().trim())
                && !(heightSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Height"))
                && !(ageSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Age"))
                && !(qualificationSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Qaualification"))
                && !(speakingSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Speaking Level"))
                && !(writingSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Writing Level"))
                && tools.hasValue(etPhone.getText().toString().trim())
        ) {

            manageSearch.setAge(stringToInt(ageSpinnner.getSelectedItem().toString()));
            manageSearch.setHeight(stringToInt(heightSpinnner.getSelectedItem().toString()));
            manageSearch.setQualification(qualificationSpinnner.getSelectedItem().toString());
            manageSearch.setEnglish_writing_level(5);
            manageSearch.setEnglish_speaking_level(5);
            manageSearch.setCnic(stringToInt(etCnic.getText().toString()));
            manageSearch.setSeen_by(seenUnSeen);
            manageSearch.setTrade(spinnerTrade.getSelectedItem().toString());
            manageSearch.setPhone_num(stringToInt(etPhone.getText().toString()));

            isFormValid = true;

        }

        return isFormValid;

    }

    public int stringToInt(String s) {
        return Integer.parseInt(s);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

}
