package com.sk.sajid.jobs.model.applyjob;

import android.os.Parcel;
import android.os.Parcelable;

/*
 *  @auther Hussnain 1/26/19
 * */
public class ApplyJobRequest implements Parcelable {
    private int english_speaking_level;
    private String country;
    private String city;
    private int date_of_birth;
    private int weight;
    private int cnic;
    private int second_phone_num;
    private String qualification;
    private int overseas_experience;
    private int passport_expiry;
    private String trade;
    private int local_experience;
    private int passport;
    private String name;
    private int english_writing_level;
    private int phone_num;
    private int age;
    private String email;
    private int height;

    public ApplyJobRequest(int english_speaking_level, String country, String city, int date_of_birth, int weight, int cnic, int second_phone_num, String qualification, int overseas_experience, int passport_expiry, String trade, int local_experience, int passport, String name, int english_writing_level, int phone_num, int age, String email, int height) {
        this.english_speaking_level = english_speaking_level;
        this.country = country;
        this.city = city;
        this.date_of_birth = date_of_birth;
        this.weight = weight;
        this.cnic = cnic;
        this.second_phone_num = second_phone_num;
        this.qualification = qualification;
        this.overseas_experience = overseas_experience;
        this.passport_expiry = passport_expiry;
        this.trade = trade;
        this.local_experience = local_experience;
        this.passport = passport;
        this.name = name;
        this.english_writing_level = english_writing_level;
        this.phone_num = phone_num;
        this.age = age;
        this.email = email;
        this.height = height;
    }

    protected ApplyJobRequest(Parcel in) {
        english_speaking_level = in.readInt();
        country = in.readString();
        city = in.readString();
        date_of_birth = in.readInt();
        weight = in.readInt();
        cnic = in.readInt();
        second_phone_num = in.readInt();
        qualification = in.readString();
        overseas_experience = in.readInt();
        passport_expiry = in.readInt();
        trade = in.readString();
        local_experience = in.readInt();
        passport = in.readInt();
        name = in.readString();
        english_writing_level = in.readInt();
        phone_num = in.readInt();
        age = in.readInt();
        email = in.readString();
        height = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(english_speaking_level);
        dest.writeString(country);
        dest.writeString(city);
        dest.writeInt(date_of_birth);
        dest.writeInt(weight);
        dest.writeInt(cnic);
        dest.writeInt(second_phone_num);
        dest.writeString(qualification);
        dest.writeInt(overseas_experience);
        dest.writeInt(passport_expiry);
        dest.writeString(trade);
        dest.writeInt(local_experience);
        dest.writeInt(passport);
        dest.writeString(name);
        dest.writeInt(english_writing_level);
        dest.writeInt(phone_num);
        dest.writeInt(age);
        dest.writeString(email);
        dest.writeInt(height);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ApplyJobRequest> CREATOR = new Creator<ApplyJobRequest>() {
        @Override
        public ApplyJobRequest createFromParcel(Parcel in) {
            return new ApplyJobRequest(in);
        }

        @Override
        public ApplyJobRequest[] newArray(int size) {
            return new ApplyJobRequest[size];
        }
    };

    public int getEnglish_speaking_level() {
        return english_speaking_level;
    }

    public void setEnglish_speaking_level(int english_speaking_level) {
        this.english_speaking_level = english_speaking_level;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(int date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCnic() {
        return cnic;
    }

    public void setCnic(int cnic) {
        this.cnic = cnic;
    }

    public int getSecond_phone_num() {
        return second_phone_num;
    }

    public void setSecond_phone_num(int second_phone_num) {
        this.second_phone_num = second_phone_num;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public int getOverseas_experience() {
        return overseas_experience;
    }

    public void setOverseas_experience(int overseas_experience) {
        this.overseas_experience = overseas_experience;
    }

    public int getPassport_expiry() {
        return passport_expiry;
    }

    public void setPassport_expiry(int passport_expiry) {
        this.passport_expiry = passport_expiry;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public int getLocal_experience() {
        return local_experience;
    }

    public void setLocal_experience(int local_experience) {
        this.local_experience = local_experience;
    }

    public int getPassport() {
        return passport;
    }

    public void setPassport(int passport) {
        this.passport = passport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnglish_writing_level() {
        return english_writing_level;
    }

    public void setEnglish_writing_level(int english_writing_level) {
        this.english_writing_level = english_writing_level;
    }

    public int getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(int phone_num) {
        this.phone_num = phone_num;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
