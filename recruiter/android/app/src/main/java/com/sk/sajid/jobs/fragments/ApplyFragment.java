package com.sk.sajid.jobs.fragments;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.model.applyjob.ApplyJobRequest;
import com.sk.sajid.jobs.model.applyjob.ApplyJobResponse;
import com.sk.sajid.jobs.retrofit.ApiClient;
import com.sk.sajid.jobs.retrofit.ApiInterface;
import com.sk.sajid.jobs.tools.TempData;
import com.sk.sajid.jobs.tools.Tools;
import com.sk.sajid.jobs.utilities.DialogsView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */


public class ApplyFragment extends Fragment implements View.OnClickListener {


    private EditText etCnic, etName, etEmail, etPhone, etHeight, etQulification, etSpeeking, etWriting, etWeight, etCountry, etCity, etSecondaryPhone, etTrade, etPassport, etExpiry, etExperience, elLocalExperience;
    private Button btnTrade, btnSpeeking, btnWriting, btnPhone;

    private Spinner spinnerTrade, heightSpinnner, qualificationSpinnner, speakingSpinnner, weightSpinnner, countrySpinnner, citySpinnner, experienceSpinnner, localExperienceSpinnner, writingSpinnner;

    private Boolean isBirthSelected = false;
    private Button btnHeight, btnQualification;
    private MaterialSpinner spinnerLocalExperience, spinnerExperience, spinnerSecondPhone, spinnerHeight, spinnerQualification, spinnerSpeeking, spinnerWriting, spinnerPhone, spinnerWeight, spinnerCountry, spinnerCity;

    private Button btnWeight;
    private Button btnCountry;
    private Button btnCity;
    private Button btnSecondaryPhone;
    private Button btnPassportDate, btnExperience;
    private Button btnApply;
    private ArrayList<String> tradeArray, heightArray, qualificationArray, speekingArray, writingArray, weightArray, countryArray, cityArray, experienceArray;

    private RelativeLayout rlBirthWrapper, rlSecondaryPhoneEdit;
    private TextView etBirth, expirayTv;

    private ApplyJobRequest applyJobRequest;

    private ProgressBar progressBar;

    public ApplyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_apply, container, false);
        initArrays();

        init(view);

        initializeSpinnerAdapters();
        return view;
    }

    private void initializeSpinnerAdapters() {

        initializeTradAdapter();
        initializeQualificationAdapter();
        initializeHeightSpinnnerAdapter();
        initializeSpeakingSpinnnerAdapter();
        initializeWritingSpinnnerAdapter();
        initializeWeightSpinnnerAdapter();
        initializeCountrySpinnnerAdapter();
        initializeOverSeaExperienceSpinnnerAdapter();
        initializeLocalExperienceSpinnnerAdapter();
        initializeCitySpinnnerAdapter();
    }

    private void initializeCitySpinnnerAdapter() {
        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tempData.selectedArrayList(4));
        citySpinnner.setAdapter(itemsAdapter);
    }

    private void initializeLocalExperienceSpinnnerAdapter() {

        List<String> localExperienceList = new ArrayList<String>();

        localExperienceList.add("Select Experience");
        for (int i = 0; i <= 99; i++) {
            localExperienceList.add(String.valueOf(i));
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, localExperienceList);
        localExperienceSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeOverSeaExperienceSpinnnerAdapter() {

        List<String> overseaExperienceList = new ArrayList<String>();

        overseaExperienceList.add("Select Experience");
        for (int i = 0; i <= 99; i++) {
            overseaExperienceList.add(String.valueOf(i));
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, overseaExperienceList);
        experienceSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeCountrySpinnnerAdapter() {
        List<String> countryList = new ArrayList<String>();

        countryList.add("Select Country");
        countryList.add("Pakistan");


        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, countryList);
        countrySpinnner.setAdapter(itemsAdapter);

    }


    private void initializeWeightSpinnnerAdapter() {


        List<String> weightList = new ArrayList<String>();

        weightList.add("Select Weight");
        for (int i = 1; i <= 101; i++) {
            weightList.add(String.valueOf(i));
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, weightList);
        weightSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeWritingSpinnnerAdapter() {
        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tempData.selectedArrayList(3));
        writingSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeSpeakingSpinnnerAdapter() {

        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tempData.selectedArrayList(2));
        speakingSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeQualificationAdapter() {

        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tempData.selectedArrayList(1));
        qualificationSpinnner.setAdapter(itemsAdapter);
    }

    private void initializeHeightSpinnnerAdapter() {

        List<String> heightList = new ArrayList<String>();

        heightList.add("Select Height");
        for (int i = 1; i <= 101; i++) {
            heightList.add(String.valueOf(i));
        }

        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, heightList);
        heightSpinnner.setAdapter(itemsAdapter);

    }

    private void initializeTradAdapter() {
        TempData tempData = new TempData();
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, tempData.selectedArrayList(0));
        spinnerTrade.setAdapter(itemsAdapter);

    }


    private void initArrays() {
        tradeArray = new ArrayList<>();
        heightArray = new ArrayList<>();
        qualificationArray = new ArrayList<>();
        speekingArray = new ArrayList<>();
        writingArray = new ArrayList<>();
        weightArray = new ArrayList<>();
        countryArray = new ArrayList<>();
        cityArray = new ArrayList<>();
        experienceArray = new ArrayList<>();
        addDataInArrays();
    }

    private void addDataInArrays() {
        tradeArray.add("Select Trade");
        tradeArray.add("Security Guard");
        tradeArray.add("Air Craft Cleaner");
        qualificationArray.add("");
    }

    private void init(View view) {

        progressBar = view.findViewById(R.id.progressBar);


        etCnic = view.findViewById(R.id.etCnic);
        etName = view.findViewById(R.id.etName);
        etBirth = view.findViewById(R.id.etBirth);
        heightSpinnner = view.findViewById(R.id.heightSpinnner);
        qualificationSpinnner = view.findViewById(R.id.qualificationSpinnner);
        speakingSpinnner = view.findViewById(R.id.speakingSpinnner);
        writingSpinnner = view.findViewById(R.id.writingSpinnner);
        etPhone = view.findViewById(R.id.etPhone);
        weightSpinnner = view.findViewById(R.id.weightSpinnner);
        countrySpinnner = view.findViewById(R.id.countrySpinnner);
        citySpinnner = view.findViewById(R.id.citySpinnner);
        etSecondaryPhone = view.findViewById(R.id.etSecondaryPhone);
        etEmail = view.findViewById(R.id.etEmail);
        etPassport = view.findViewById(R.id.etPassport);
        expirayTv = view.findViewById(R.id.expirayTv);
        experienceSpinnner = view.findViewById(R.id.experienceSpinnner);
        localExperienceSpinnner = view.findViewById(R.id.localExperienceSpinnner);
        spinnerTrade = view.findViewById(R.id.etTradeSpin);

        spinnerHeight = view.findViewById(R.id.spinnerHeight);
        spinnerQualification = view.findViewById(R.id.spinnerQualification);
        spinnerSpeeking = view.findViewById(R.id.spinnerSpeeking);
        spinnerWriting = view.findViewById(R.id.spinnerWriting);

        //spinnerPhone=view.findViewById(R.id.spinnerPhone);

        spinnerWeight = view.findViewById(R.id.spinnerWeight);
        spinnerCountry = view.findViewById(R.id.spinnerCountry);
        spinnerCity = view.findViewById(R.id.spinnerCity);
        btnSecondaryPhone = view.findViewById(R.id.btnSecondaryPhone);
        //spinnerSecondPhone=view.findViewById(R.id.spinnerSecondPhone);

        spinnerExperience = view.findViewById(R.id.spinnerExperience);
        spinnerLocalExperience = view.findViewById(R.id.spinnerLocalExperience);
        btnApply = view.findViewById(R.id.btnApply);


        rlBirthWrapper = view.findViewById(R.id.rlBirthWrapper);
        rlSecondaryPhoneEdit = view.findViewById(R.id.rlSecondaryPhoneEdit);

        btnApply.setOnClickListener(this);
        rlBirthWrapper.setOnClickListener(this);

        etBirth.setOnClickListener(this);
        rlSecondaryPhoneEdit.setOnClickListener(this);
        expirayTv.setOnClickListener(this);
    }

    private boolean isValidString(EditText field) {

        if (field.getText().toString().isEmpty()) {
            return false;
        }
        return true;
    }

    private boolean isValidInt(EditText field) {

        if (Integer.parseInt(field.getText().toString()) < 0) {
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.rlBirthWrapper:
            case R.id.etBirth: {

                showDatePicker(etBirth, 1);

                break;
            }

            case R.id.rlSecondaryPhoneEdit:
            case R.id.expirayTv: {

                showDatePicker(expirayTv, 2);

                break;
            }

            case R.id.btnApply: {


                if (etName.getText().toString().trim() != null && etName.getText().toString().trim().length() < 4) {
                    DialogsView.showDialog(getActivity(), getActivity().getString(R.string.name_error_message), false);
                    break;
                }

                if (isFormValid()) {

                    progressBar.setVisibility(View.VISIBLE);


                    ApiInterface apiInterface = ApiClient.getInstance().getClient().create(ApiInterface.class);
                    Call<ApplyJobResponse> call = apiInterface.applyJob(applyJobRequest);
                    call.enqueue(new Callback<ApplyJobResponse>() {
                        @Override
                        public void onResponse(Call<ApplyJobResponse> call, Response<ApplyJobResponse> response) {

                            progressBar.setVisibility(View.GONE);  //To show ProgressBar
                           /* Log.d("t", "##\n" + response.body() + "");
                            Toast.makeText(getActivity(), "true responseee"+"\n"+response.body(), Toast.LENGTH_LONG).show();*/
                            DialogsView.showDialog(getActivity(), getActivity().getString(R.string.apply_form_message_api), true);

                        }

                        @Override
                        public void onFailure(Call<ApplyJobResponse> call, Throwable t) {

                            progressBar.setVisibility(View.GONE);  //To show ProgressBar
                            //   Log.d("t", t + "");
                            Toast.makeText(getActivity(), getActivity().getString(R.string.failure_message_api), Toast.LENGTH_SHORT).show();

                        }


                    });


                } else {

                    DialogsView.showDialog(getActivity(), getActivity().getString(R.string.failure_message_validations), false);
                }
                break;
            }

        }

    }

    private boolean isFormValid() {
        Boolean isFormValid = false;
        Tools tools = new Tools();


        if (!(spinnerTrade.getSelectedItem().toString().equalsIgnoreCase("Select Trade"))
                && tools.hasValue(etCnic.getText().toString().trim())
                && tools.hasValue(etName.getText().toString().trim())
                && isBirthSelected
                && !(heightSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Height"))
                && !(qualificationSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Qaualification"))
                && !(speakingSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Speaking Level"))
                && !(writingSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Writing Level"))
                && tools.hasValue(etPhone.getText().toString().trim())
                && !(weightSpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Weight"))
                && !(countrySpinnner.getSelectedItem().toString().equalsIgnoreCase("Select Country"))
                && !(citySpinnner.getSelectedItem().toString().equalsIgnoreCase("Select City"))) {

            applyJobRequest = new ApplyJobRequest(0,
                    countrySpinnner.getSelectedItem().toString()
                    , citySpinnner.getSelectedItem().toString()
                    , 1223
                    , 12
                    , 4444444
                    , 123123
                    , qualificationSpinnner.getSelectedItem().toString()
                    , 12
                    , 1231
                    , spinnerTrade.getSelectedItem().toString()
                    , 1
                    , 34
                    , etName.getText().toString().trim()
                    , 2
                    , 123
                    , 22
                    , etEmail.getText().toString().trim()
                    , 2);

            isFormValid = true;

        }

        return isFormValid;

    }

    private void showDatePicker(final TextView forView, final int i) {
        forView.setError(null);

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear++;
                String date = String.format(Locale.US, "%d-%s-%d", dayOfMonth, monthOfYear < 10 ? "0" + monthOfYear : "" + monthOfYear, year);
                forView.setText(date);

                if (i == 1) {
                    isBirthSelected = true;

                }
            }
        }, year, month, day);

        dialog.getDatePicker().setMaxDate(new Date().getTime());
        dialog.show();


    } // showDatePicker


}

