package com.sk.sajid.jobs.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sk.sajid.jobs.AddEditJobActivity;
import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.model.JobsData;



import java.util.List;


/**
 * Created by H.A.R on 1/26/2018.
 */

public class AddJobsAdapter extends RecyclerView.Adapter<AddJobsAdapter.MyViewHolder> implements View.OnClickListener {
    private Context mContext;
    private List<JobsData> jobsList;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnEdit:
                Intent intent=new Intent(mContext, AddEditJobActivity.class);
                mContext.startActivity(intent);
                break;
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private  Button btnEdit;
        private  TextView tvCompanyName,tvSalary,tvLocation;
        public TextView tvName;

        public MyViewHolder(View view) {
            super(view);
            tvName =  view.findViewById(R.id.tvName);
            tvCompanyName =  view.findViewById(R.id.tvCompanyName);
            tvSalary =  view.findViewById(R.id.tvSalary);
            tvLocation =  view.findViewById(R.id.tvLocation);
            btnEdit=view.findViewById(R.id.btnEdit);

        }

    }

    public AddJobsAdapter(Context mContext, List<JobsData> jobsList) {
        this.mContext = mContext;
        this.jobsList = jobsList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.add_jobs_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        JobsData jobs = jobsList.get(position);
        holder.tvName.setText(jobs.getTitle());
        holder.tvCompanyName.setText(jobs.getCompany());
        holder.tvSalary.setText(jobs.getSalary());
        holder.tvLocation.setText(jobs.getLocation());
        holder.btnEdit.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return jobsList.size();
    }
}

