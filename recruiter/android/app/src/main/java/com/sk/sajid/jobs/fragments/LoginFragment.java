package com.sk.sajid.jobs.fragments;


import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sk.sajid.jobs.HomeActivity;
import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.model.User;
import com.sk.sajid.jobs.model.UserObject;
import com.sk.sajid.jobs.retrofit.ApiClient;
import com.sk.sajid.jobs.retrofit.ApiInterface;
import com.sk.sajid.jobs.utilities.Common;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {


    private EditText etEmail, etPass;
    private Button btnLogin;
    private ConstraintLayout loginLayout;
    private ProgressBar progressBar;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        init(view);
        return view;
    }

    private void init(View view) {
        etEmail = view.findViewById(R.id.etEmail);
        etPass = view.findViewById(R.id.etPass);
        btnLogin = view.findViewById(R.id.btnLogin);
        loginLayout = view.findViewById(R.id.loginLayout);
        btnLogin.setOnClickListener(this);
    }

    public void login(String email, String password) {
        ApiInterface apiInterface = ApiClient.getInstance().getClient().create(ApiInterface.class);
        Call<User> call = apiInterface.userLogin("", "", email, password);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null && response.isSuccessful()) {
                    ApiClient.getInstance().data = response.body().getData();
                    User user = response.body();
                    if (getActivity() != null) {
                        ((HomeActivity) getActivity()).enableSideDrawer();

                        Common.client_id = user.getData().getClientId();
                        Common.token = user.getData().getToken();
                    }
                    Toast.makeText(getActivity(), "Login successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();

                }

            }


            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getActivity(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (etEmail.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Email can't be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etPass.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Password can't be empty", Toast.LENGTH_SHORT).show();
                    return;
                }
                login(etEmail.getText().toString().trim(), etPass.getText().toString().trim());
                break;
        }
    }
}
