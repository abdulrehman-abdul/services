package com.sk.sajid.jobs.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.activities.SearchListActivity;
import com.sk.sajid.jobs.model.searchjob.SearchJobRequest;
import com.sk.sajid.jobs.model.searchjob.SearchJobResponse;
import com.sk.sajid.jobs.retrofit.ApiClient;
import com.sk.sajid.jobs.retrofit.ApiInterface;
import com.sk.sajid.jobs.tools.Tools;
import com.sk.sajid.jobs.utilities.DialogsView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultFragment extends Fragment implements View.OnClickListener {


    private EditText phoneEt, cnicEt;
    private Button searchButton;
    private ProgressBar progressBar;

    private SearchJobRequest searchJobRequest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);
        init(view);
        return view;
    }


    private void init(View view) {
        phoneEt = view.findViewById(R.id.phoneEt);
        cnicEt = view.findViewById(R.id.cnicEt);
        searchButton = view.findViewById(R.id.search_button);
        progressBar = view.findViewById(R.id.progressBar);
        searchButton.setOnClickListener(this);
    }

    public void searchJob() {

        progressBar.setVisibility(View.VISIBLE);

        ApiInterface apiInterface = ApiClient.getInstance().getClient().create(ApiInterface.class);
        Call<SearchJobResponse> call = apiInterface.searchJob(searchJobRequest);
        call.enqueue(new Callback<SearchJobResponse>() {
            @Override
            public void onResponse(Call<SearchJobResponse> call, Response<SearchJobResponse> response) {
                response.body().getData();
              // DialogsView.showDialog(getActivity(), getActivity().getString(R.string.jod_success_message), true);

                progressBar.setVisibility(View.GONE);


                // sending search list object to other acitivity
                if (getActivity() != null
                        && response != null
                        && response.body() != null
                        && response.body().getData() != null && response.body().getData().size() > 0) {

                    Intent intent = new Intent(getActivity(), SearchListActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("list", response.body().getData());
                    intent.putExtras(bundle);
                    getActivity().startActivity(intent);
                }

            }


            @Override
            public void onFailure(Call<SearchJobResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_button:


                if (phoneEt.getText().toString().trim().isEmpty() && cnicEt.getText().toString().trim().isEmpty()) {
                    DialogsView.showDialog(getActivity(), getActivity().getString(R.string.search_error_message), false);
                    return;
                }
                if (new Tools().hasValue(cnicEt.getText().toString().trim())) {

                    searchJobRequest = new SearchJobRequest(
                            0
                            , ""
                            , 0
                            , 0
                            , ""
                            , 0
                            , ""
                            , Integer.parseInt(cnicEt.getText().toString().trim())
                            , 0
                    );

                }


                if (new Tools().hasValue(phoneEt.getText().toString().trim())
                        && new Tools().hasValue(cnicEt.getText().toString().trim()
                )) {

                    searchJobRequest = new SearchJobRequest(
                            0
                            , ""
                            , 0
                            , Integer.parseInt(phoneEt.getText().toString().trim())
                            , ""
                            , 0
                            , ""
                            , Integer.parseInt(cnicEt.getText().toString().trim())
                            , 0
                    );

                } else if (new Tools().hasValue(phoneEt.getText().toString().trim())) {

                    searchJobRequest = new SearchJobRequest(
                            0
                            , ""
                            , 0
                            , Integer.parseInt(phoneEt.getText().toString().trim())
                            , ""
                            , 0
                            , ""
                            , 0
                            , 0
                    );
                }

                //API call
                searchJob();
                break;
        }
    }
}

