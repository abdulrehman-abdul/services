
package com.sk.sajid.jobs.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobsResult{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("created_at")
    @Expose
    private long createdAt;
    @SerializedName("updated_at")
    @Expose
    private long updatedAt;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("date_of_birth")
    @Expose
    private Integer dateOfBirth;
    @SerializedName("weight")
    @Expose
    private Integer weight;
    @SerializedName("phone_num")
    @Expose
    private Integer phoneNum;
    @SerializedName("english_speaking_level")
    @Expose
    private Integer englishSpeakingLevel;
    @SerializedName("english_writing_level")
    @Expose
    private Integer englishWritingLevel;
    @SerializedName("english_listening_level")
    @Expose
    private Integer englishListeningLevel;
    @SerializedName("cnic")
    @Expose
    private Integer cnic;
    @SerializedName("trade")
    @Expose
    private String trade;
    @SerializedName("qualification")
    @Expose
    private String qualification;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("seen_by")
    @Expose
    private Object seenBy;
    @SerializedName("passport")
    @Expose
    private Integer passport;
    @SerializedName("passport_expiry")
    @Expose
    private Object passportExpiry;
    @SerializedName("overseas_experience")
    @Expose
    private Object overseasExperience;
    @SerializedName("local_experience")
    @Expose
    private Object localExperience;
    @SerializedName("second_phone_num")
    @Expose
    private Integer secondPhoneNum;
    @SerializedName("social_media_interaction")
    @Expose
    private Integer socialMediaInteraction;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Integer dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(Integer phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Integer getEnglishSpeakingLevel() {
        return englishSpeakingLevel;
    }

    public void setEnglishSpeakingLevel(Integer englishSpeakingLevel) {
        this.englishSpeakingLevel = englishSpeakingLevel;
    }

    public Integer getEnglishWritingLevel() {
        return englishWritingLevel;
    }

    public void setEnglishWritingLevel(Integer englishWritingLevel) {
        this.englishWritingLevel = englishWritingLevel;
    }

    public Integer getEnglishListeningLevel() {
        return englishListeningLevel;
    }

    public void setEnglishListeningLevel(Integer englishListeningLevel) {
        this.englishListeningLevel = englishListeningLevel;
    }

    public Integer getCnic() {
        return cnic;
    }

    public void setCnic(Integer cnic) {
        this.cnic = cnic;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getSeenBy() {
        return seenBy;
    }

    public void setSeenBy(Object seenBy) {
        this.seenBy = seenBy;
    }

    public Integer getPassport() {
        return passport;
    }

    public void setPassport(Integer passport) {
        this.passport = passport;
    }

    public Object getPassportExpiry() {
        return passportExpiry;
    }

    public void setPassportExpiry(Object passportExpiry) {
        this.passportExpiry = passportExpiry;
    }

    public Object getOverseasExperience() {
        return overseasExperience;
    }

    public void setOverseasExperience(Object overseasExperience) {
        this.overseasExperience = overseasExperience;
    }

    public Object getLocalExperience() {
        return localExperience;
    }

    public void setLocalExperience(Object localExperience) {
        this.localExperience = localExperience;
    }

    public Integer getSecondPhoneNum() {
        return secondPhoneNum;
    }

    public void setSecondPhoneNum(Integer secondPhoneNum) {
        this.secondPhoneNum = secondPhoneNum;
    }

    public Integer getSocialMediaInteraction() {
        return socialMediaInteraction;
    }

    public void setSocialMediaInteraction(Integer socialMediaInteraction) {
        this.socialMediaInteraction = socialMediaInteraction;
    }

}
