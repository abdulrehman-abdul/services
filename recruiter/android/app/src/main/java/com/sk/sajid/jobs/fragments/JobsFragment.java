package com.sk.sajid.jobs.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.adapter.JobsAdapter;
import com.sk.sajid.jobs.model.JobsData;
import com.sk.sajid.jobs.model.Jobs;
import com.sk.sajid.jobs.retrofit.ApiClient;
import com.sk.sajid.jobs.retrofit.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobsFragment extends Fragment {


    private RecyclerView rvJobs;
    private RelativeLayout layout;
    private ProgressBar progressBar;

    public JobsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_jobs, container, false);

        init(view);
        return view;
    }

    private void init(View view) {
        rvJobs = view.findViewById(R.id.rvJobs);
        layout = view.findViewById(R.id.layout);
        getJobs();
        //  setJobsData();
    }

    private void getJobs() {
        ApiInterface apiInterface = ApiClient.getInstance().getClient().create(ApiInterface.class);
        Call<Jobs> call = apiInterface.getJobs();
        progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleLarge);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(150, 150);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        layout.addView(progressBar, params);
        progressBar.setVisibility(View.VISIBLE);  //To show ProgressBar

        call.enqueue(new Callback<Jobs>() {
            @Override
            public void onResponse(Call<Jobs> call, Response<Jobs> response) {
                Log.d("jobs", response.raw().toString());
                List<JobsData> jobsArrayList = response.body().getData();
                progressBar.setVisibility(View.GONE);
                if (jobsArrayList != null && jobsArrayList.size() > 0) {
                    JobsAdapter jobsAdapter = new JobsAdapter(getActivity(), jobsArrayList);
                    rvJobs.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                    rvJobs.setAdapter(jobsAdapter);
                }
            }

            @Override
            public void onFailure(Call<Jobs> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
            }
        });
    }

   /* public void setJobsData() {

    }*/
}
