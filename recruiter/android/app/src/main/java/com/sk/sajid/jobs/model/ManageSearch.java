package com.sk.sajid.jobs.model;

public class ManageSearch {



    private int english_speaking_level;
    private String qualification;
    private String trade;
    private int english_writing_level;
    private int phone_num;
    private int age;
    private int height;
    private int cnic;
    private String seen_by;

    public int getEnglish_speaking_level() {
        return english_speaking_level;
    }

    public void setEnglish_speaking_level(int english_speaking_level) {
        this.english_speaking_level = english_speaking_level;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public int getEnglish_writing_level() {
        return english_writing_level;
    }

    public void setEnglish_writing_level(int english_writing_level) {
        this.english_writing_level = english_writing_level;
    }

    public int getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(int phone_num) {
        this.phone_num = phone_num;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getCnic() {
        return cnic;
    }

    public void setCnic(int cnic) {
        this.cnic = cnic;
    }

    public String getSeen_by() {
        return seen_by;
    }

    public void setSeen_by(String seen_by) {
        this.seen_by = seen_by;
    }
}
