package com.sk.sajid.jobs.retrofit;

import com.sk.sajid.jobs.model.AddJob;
import com.sk.sajid.jobs.model.Jobs;
import com.sk.sajid.jobs.model.JobsResult;
import com.sk.sajid.jobs.model.ManageSearch;
import com.sk.sajid.jobs.model.NotificationsData;
import com.sk.sajid.jobs.model.SearchJobs;
import com.sk.sajid.jobs.model.User;
import com.sk.sajid.jobs.model.UserObject;
import com.sk.sajid.jobs.model.applyjob.ApplyJobRequest;
import com.sk.sajid.jobs.model.applyjob.ApplyJobResponse;
import com.sk.sajid.jobs.model.searchjob.SearchJobRequest;
import com.sk.sajid.jobs.model.searchjob.SearchJobResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface ApiInterface {

    @POST("api/user/login")
    Call<User> userLogin(@Header("Authorization") String authorization, @Header("name") String name, @Header("email") String email, @Header("password") String password);

    @GET("api/job/get/all?responseType=JSON")
    Call<Jobs> getJobs();

    @FormUrlEncoded
    @POST("api/job/create")
    Call<AddJob> addJob(@Field("company") String company, @Field("location") String location, @Field("salary") String salary, @Field("title") String title, @Field("requirements") String requirements);

    @POST("api/bio/unauth/search")
    Call<SearchJobs> searchJobs(@Body JobsResult myModal);


    @Headers("Content-Type: application/json")
    @POST("api/bio/create")
    Call<ApplyJobResponse> applyJob(@Body ApplyJobRequest applyJobRequest);


    @Headers("Content-Type: application/json")
    @POST("api/bio/unauth/search")
    Call<SearchJobResponse> searchJob(@Body SearchJobRequest searchJobRequest);


    @GET("api/bio/unseen?responseType=JSON")
    Call<NotificationsData> notificationResult(@Header(NotificationsData.token) String token,
                                               @Header(NotificationsData.client_id) String cleint_Id);

    @Headers("Content-Type: application/json")
    @POST("api/bio/search")
    Call<ManageSearch> manageSearch(
            @Header(NotificationsData.token) String token,
            @Header(NotificationsData.client_id) String client_id,
            @Body ManageSearch manageSearch);


}
