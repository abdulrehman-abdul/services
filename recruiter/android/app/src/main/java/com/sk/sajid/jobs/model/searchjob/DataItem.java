package com.sk.sajid.jobs.model.searchjob;

public class DataItem{
	private String country;
	private String city;
	private long dateOfBirth;
	private long createdAt;
	private Object overseasExperience;
	private Object seenBy;
	private long updatedAt;
	private int passport;
	private int phoneNum;
	private int id;
	private String email;
	private int height;
	private int englishSpeakingLevel;
	private int weight;
	private int cnic;
	private int secondPhoneNum;
	private int socialMediaInteraction;
	private String qualification;
	private Object passportExpiry;
	private String trade;
	private Object localExperience;
	private int englishListeningLevel;
	private String name;
	private int englishWritingLevel;
	private int age;

	public String getCountry(){
		return country;
	}

	public String getCity(){
		return city;
	}

	public long getDateOfBirth(){
		return dateOfBirth;
	}

	public long getCreatedAt(){
		return createdAt;
	}

	public Object getOverseasExperience(){
		return overseasExperience;
	}

	public Object getSeenBy(){
		return seenBy;
	}

	public long getUpdatedAt(){
		return updatedAt;
	}

	public int getPassport(){
		return passport;
	}

	public int getPhoneNum(){
		return phoneNum;
	}

	public int getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public int getHeight(){
		return height;
	}

	public int getEnglishSpeakingLevel(){
		return englishSpeakingLevel;
	}

	public int getWeight(){
		return weight;
	}

	public int getCnic(){
		return cnic;
	}

	public int getSecondPhoneNum(){
		return secondPhoneNum;
	}

	public int getSocialMediaInteraction(){
		return socialMediaInteraction;
	}

	public String getQualification(){
		return qualification;
	}

	public Object getPassportExpiry(){
		return passportExpiry;
	}

	public String getTrade(){
		return trade;
	}

	public Object getLocalExperience(){
		return localExperience;
	}

	public int getEnglishListeningLevel(){
		return englishListeningLevel;
	}

	public String getName(){
		return name;
	}

	public int getEnglishWritingLevel(){
		return englishWritingLevel;
	}

	public int getAge(){
		return age;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"country = '" + country + '\'' + 
			",city = '" + city + '\'' + 
			",date_of_birth = '" + dateOfBirth + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",overseas_experience = '" + overseasExperience + '\'' + 
			",seen_by = '" + seenBy + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",passport = '" + passport + '\'' + 
			",phone_num = '" + phoneNum + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			",height = '" + height + '\'' + 
			",english_speaking_level = '" + englishSpeakingLevel + '\'' + 
			",weight = '" + weight + '\'' + 
			",cnic = '" + cnic + '\'' + 
			",second_phone_num = '" + secondPhoneNum + '\'' + 
			",social_media_interaction = '" + socialMediaInteraction + '\'' + 
			",qualification = '" + qualification + '\'' + 
			",passport_expiry = '" + passportExpiry + '\'' + 
			",trade = '" + trade + '\'' + 
			",local_experience = '" + localExperience + '\'' + 
			",english_listening_level = '" + englishListeningLevel + '\'' + 
			",name = '" + name + '\'' + 
			",english_writing_level = '" + englishWritingLevel + '\'' + 
			",age = '" + age + '\'' + 
			"}";
		}
}
