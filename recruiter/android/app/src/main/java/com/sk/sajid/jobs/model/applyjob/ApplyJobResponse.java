package com.sk.sajid.jobs.model.applyjob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 *  @auther Hussnain 1/26/19
 * */
public class ApplyJobResponse {
    private Data data;
    @SerializedName("_explicitType")
    @Expose
    private String explicitType;
    private String errorCode;
    private int revisionNo;
    private int type;
    private String message;

}
