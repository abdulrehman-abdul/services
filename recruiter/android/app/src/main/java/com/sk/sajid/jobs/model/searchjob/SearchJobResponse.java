package com.sk.sajid.jobs.model.searchjob;

import android.os.Parcel;
import android.os.Parcelable;

import com.sk.sajid.jobs.model.applyjob.Data;

import java.util.ArrayList;
import java.util.List;

public class SearchJobResponse implements Parcelable {
    private List<Data> data;
    private String explicitType;
    private String errorCode;
    private int revisionNo;
    private int type;
    private String message;

    protected SearchJobResponse(Parcel in) {
        data = in.createTypedArrayList(Data.CREATOR);
        explicitType = in.readString();
        errorCode = in.readString();
        revisionNo = in.readInt();
        type = in.readInt();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(data);
        dest.writeString(explicitType);
        dest.writeString(errorCode);
        dest.writeInt(revisionNo);
        dest.writeInt(type);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SearchJobResponse> CREATOR = new Creator<SearchJobResponse>() {
        @Override
        public SearchJobResponse createFromParcel(Parcel in) {
            return new SearchJobResponse(in);
        }

        @Override
        public SearchJobResponse[] newArray(int size) {
            return new SearchJobResponse[size];
        }
    };

    public ArrayList<? extends Parcelable> getData() {
        return (ArrayList<? extends Parcelable>) data;
    }

    public String getExplicitType() {
        return explicitType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public int getRevisionNo() {
        return revisionNo;
    }

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return
                "SearchJobResponse{" +
                        "data = '" + data + '\'' +
                        ",_explicitType = '" + explicitType + '\'' +
                        ",errorCode = '" + errorCode + '\'' +
                        ",revisionNo = '" + revisionNo + '\'' +
                        ",type = '" + type + '\'' +
                        ",message = '" + message + '\'' +
                        "}";
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public void setExplicitType(String explicitType) {
        this.explicitType = explicitType;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public void setRevisionNo(int revisionNo) {
        this.revisionNo = revisionNo;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public SearchJobResponse(List<Data> data, String explicitType, String errorCode, int revisionNo, int type, String message) {
        this.data = data;
        this.explicitType = explicitType;
        this.errorCode = errorCode;
        this.revisionNo = revisionNo;
        this.type = type;
        this.message = message;
    }
}