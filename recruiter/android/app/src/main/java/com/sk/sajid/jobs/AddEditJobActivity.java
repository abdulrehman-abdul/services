package com.sk.sajid.jobs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sk.sajid.jobs.model.AddJob;
import com.sk.sajid.jobs.retrofit.ApiClient;
import com.sk.sajid.jobs.retrofit.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddEditJobActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etTitle;
    private EditText etCompany;
    private EditText etSalary;
    private EditText etLocation;
    private Button btnAddUpdate;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_job);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        init();
    }

    private void init() {
        etTitle = findViewById(R.id.etTitle);
        etCompany = findViewById(R.id.etCompany);
        etSalary = findViewById(R.id.etSalary);
        etLocation = findViewById(R.id.etLocation);
        btnAddUpdate = findViewById(R.id.btnAddUpdate);
        progressBar = findViewById(R.id.progressBar);
        btnAddUpdate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddUpdate:

                addJob();
                break;
        }
    }

    public void addJob() {
        if (etTitle.getText().toString().isEmpty()) {
            Toast.makeText(AddEditJobActivity.this, "Title can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (etCompany.getText().toString().isEmpty()) {
            Toast.makeText(AddEditJobActivity.this, "Company  can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (etSalary.getText().toString().isEmpty()) {
            Toast.makeText(AddEditJobActivity.this, "Salary can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        if (etLocation.getText().toString().isEmpty()) {
            Toast.makeText(AddEditJobActivity.this, "Location can't be empty", Toast.LENGTH_SHORT).show();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        ApiInterface apiInterface = ApiClient.getInstance().getClient().create(ApiInterface.class);
        Call<AddJob> call = apiInterface.addJob(etCompany.getText().toString(), etLocation.getText().toString(), etSalary.getText().toString(), etTitle.getText().toString(), "dddd");
        call.enqueue(new Callback<AddJob>() {
            @Override
            public void onResponse(Call<AddJob> call, Response<AddJob> response) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(AddEditJobActivity.this, "Job Added Successfully", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<AddJob> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(AddEditJobActivity.this, "Job not Added", Toast.LENGTH_SHORT).show();


            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}
