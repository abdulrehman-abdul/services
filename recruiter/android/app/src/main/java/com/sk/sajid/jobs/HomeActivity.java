package com.sk.sajid.jobs;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.internal.NavigationMenuItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sk.sajid.jobs.activities.ManageSearchResults;
import com.sk.sajid.jobs.activities.NotificationsActivity;
import com.sk.sajid.jobs.adapter.NotificationsAdapter;
import com.sk.sajid.jobs.fragments.AddJobFragment;
import com.sk.sajid.jobs.fragments.ApplyFragment;
import com.sk.sajid.jobs.fragments.JobsFragment;
import com.sk.sajid.jobs.fragments.LoginFragment;
import com.sk.sajid.jobs.fragments.SearchResultFragment;
import com.sk.sajid.jobs.model.NotificationsData;
import com.sk.sajid.jobs.retrofit.ApiClient;
import com.sk.sajid.jobs.retrofit.ApiInterface;
import com.sk.sajid.jobs.utilities.Common;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, NavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView navigationView;
    private NavigationView nav_view;
    private Toolbar toolbar;
    private BottomNavigationView navigation;
    private DrawerLayout drawer_layout;
    private TextView tvCounter;
    private TextView tvNotifications;
    private boolean isJobsShow = false;
    private ImageView ivNotifications;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        init();

        ivNotifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(HomeActivity.this, NotificationsActivity.class));
            }
        });


    }

    @SuppressLint("NewApi")
    private void init() {
        nav_view = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        navigation = findViewById(R.id.navigation);
        drawer_layout = findViewById(R.id.drawer_layout);
        nav_view.setNavigationItemSelectedListener(this);
        View header = nav_view.getHeaderView(0);
        tvCounter = header.findViewById(R.id.tv_notification);
        ivNotifications = header.findViewById(R.id.notfication);
        tvNotifications = header.findViewById(R.id.tv_user_email);
        ActionBarDrawerToggle mDrawerToggle;
        setSupportActionBar(toolbar);

        // to lock swipe gesture of side drawer
        drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        //to unlock
        // drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);


        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
            mDrawerToggle = new ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.app_name, R.string.app_name) {

                public void onDrawerClosed(View view) {
                    supportInvalidateOptionsMenu();
                    //drawerOpened = false;
                }

                public void onDrawerOpened(View drawerView) {
                    supportInvalidateOptionsMenu();
                    //drawerOpened = true;
                }
            };
            mDrawerToggle.setDrawerIndicatorEnabled(true);
            drawer_layout.setDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
        }
        navigation.setOnNavigationItemSelectedListener(this);
        replaceFragment(new JobsFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_jobs:
                Intent intent = new Intent(HomeActivity.this, AddEditJobActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.jobs:
                isJobsShow = false;
                replaceFragment(new JobsFragment());
                break;
            case R.id.apply:
                isJobsShow = false;
                replaceFragment(new ApplyFragment());
                break;
            case R.id.results:
                isJobsShow = false;
                replaceFragment(new SearchResultFragment());
                break;
            case R.id.login:
                isJobsShow = false;
                replaceFragment(new LoginFragment());
                break;
            case R.id.manage:
                isJobsShow = true;
                replaceFragment(new AddJobFragment());
                drawer_layout.closeDrawer(GravityCompat.START);
                break;
            case R.id.search:
                Intent intent = new Intent(HomeActivity.this, ManageSearchResults.class);
                startActivity(intent);
                drawer_layout.closeDrawer(GravityCompat.START);
                break;
        }
        return true;
    }

    public void addJobsFragment() {
        isJobsShow = true;
        AddJobFragment addJobFragment = new AddJobFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, addJobFragment, "addjobs").addToBackStack(null).commit();
    }

    public void applyFragment() {
        isJobsShow = false;
        ApplyFragment applyFragment = new ApplyFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, applyFragment, "apply").addToBackStack(null).commit();
    }

    public void jobsFragment() {
        isJobsShow = false;
        JobsFragment jobsFragment = new JobsFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, jobsFragment, "jobs").addToBackStack(null).commit();
    }

    public void LoginFragment() {
        isJobsShow = false;
        LoginFragment loginFragment = new LoginFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, loginFragment, "login").addToBackStack(null).commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }


    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem addJob = menu.findItem(R.id.add_jobs);
        if (isJobsShow) {
            addJob.setVisible(true);

        } else {
            addJob.setVisible(false);
        }
        return true;
    }


    public void enableSideDrawer() {

        if (drawer_layout != null && toolbar != null) {
            actionBar.setHomeButtonEnabled(true);
            toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            callEvery10Seconds();
        }
    }


    @Override
    public void onBackPressed() {

        if (drawer_layout.isDrawerOpen(nav_view)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {

            FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragmentManager.getBackStackEntryCount() > 1) {
                fragmentManager.popBackStack();
            } else {
                super.onBackPressed();
            }

        }


    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }


    public void getNotifications() {

        ApiInterface apiInterface = ApiClient.getInstance().getClient().create(ApiInterface.class);
        Call<NotificationsData> call = apiInterface.notificationResult(Common.token, Common.client_id);
        call.enqueue(new Callback<NotificationsData>() {
            @Override
            public void onResponse(Call<NotificationsData> call, Response<NotificationsData> response) {

                if (response.body() != null && response.isSuccessful()) {
                    NotificationsData notificationsData = response.body();
                    List<NotificationsData.DataObject> dataObjectList = notificationsData.getData();
                    if (dataObjectList != null && dataObjectList.size() > 0) {
                        tvCounter.setText(String.valueOf(dataObjectList.size()));
                    }
                }

            }

            @Override
            public void onFailure(Call<NotificationsData> call, Throwable t) {

            }
        });

    }

    public void callEvery10Seconds() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 20 seconds
                getNotifications();
            }
        }, 10000);
    }
}
