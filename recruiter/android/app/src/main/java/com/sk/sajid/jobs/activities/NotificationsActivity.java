package com.sk.sajid.jobs.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.sk.sajid.jobs.HomeActivity;
import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.adapter.JobsAdapter;
import com.sk.sajid.jobs.adapter.NotificationsAdapter;
import com.sk.sajid.jobs.model.Jobs;
import com.sk.sajid.jobs.model.JobsData;
import com.sk.sajid.jobs.model.NotificationsData;
import com.sk.sajid.jobs.model.UserObject;
import com.sk.sajid.jobs.retrofit.ApiClient;
import com.sk.sajid.jobs.retrofit.ApiInterface;
import com.sk.sajid.jobs.utilities.Common;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private RecyclerView recyclerView;

    private Button btn_manage_notifications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        init();
        progressBar.setVisibility(View.VISIBLE);
        getNotifications();

        btn_manage_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NotificationsActivity.this, ManageSearchResults.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void init() {

        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.notifications_list);
        btn_manage_notifications = findViewById(R.id.btn_manage_notifications);

    }


    public void getNotifications() {

        ApiInterface apiInterface = ApiClient.getInstance().getClient().create(ApiInterface.class);
        Call<NotificationsData> call = apiInterface.notificationResult(Common.token, Common.client_id);
        call.enqueue(new Callback<NotificationsData>() {
            @Override
            public void onResponse(Call<NotificationsData> call, Response<NotificationsData> response) {

                if (response.body() != null && response.isSuccessful()) {
                    NotificationsData notificationsData = response.body();

                    List<NotificationsData.DataObject> dataObjectList = notificationsData.getData();

                    progressBar.setVisibility(View.GONE);
                    if (dataObjectList != null && dataObjectList.size() > 0) {
                        NotificationsAdapter notificationsAdapter = new NotificationsAdapter(NotificationsActivity.this, dataObjectList);
                        recyclerView.setLayoutManager(new LinearLayoutManager(NotificationsActivity.this, LinearLayoutManager.VERTICAL, false));
                        recyclerView.setAdapter(notificationsAdapter);
                    } else {
                        Toast.makeText(NotificationsActivity.this, " o result", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationsData> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(NotificationsActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

}
