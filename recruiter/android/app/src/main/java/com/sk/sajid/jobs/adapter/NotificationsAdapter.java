package com.sk.sajid.jobs.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.sk.sajid.jobs.AddEditJobActivity;
import com.sk.sajid.jobs.R;
import com.sk.sajid.jobs.model.JobsData;
import com.sk.sajid.jobs.model.NotificationsData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


/**
 * Created by H.A.R on 1/26/2018.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {
    private Context mContext;
    private List<NotificationsData.DataObject> jobsList;
    Integer today_day = 0;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private Button btnEdit;
        private TextView tvNotification;
        public RelativeTimeTextView tvTime;

        public MyViewHolder(View view) {
            super(view);
            tvNotification = view.findViewById(R.id.tv_notification);
            tvTime = view.findViewById(R.id.tv_notification_time);


        }

    }

    public NotificationsAdapter(Context mContext, List<NotificationsData.DataObject> jobsList) {
        this.mContext = mContext;
        this.jobsList = jobsList;
        Calendar cal = Calendar.getInstance();
        today_day = cal.get(Calendar.DAY_OF_MONTH);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notifications_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        NotificationsData.DataObject dataObject = jobsList.get(position);
        holder.tvNotification.setText(dataObject.getName());
        holder.tvTime.setReferenceTime(dataObject.getCreated_at());

    }

    @Override
    public int getItemCount() {
        return jobsList.size();
    }

    // change the date into (today ,yesterday and date)
    public String ChangeDate(String date) {
        //current date in millisecond
        long currenttime = System.currentTimeMillis();

        //database date in millisecond
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        long databasedate = 0;
        Date d = null;
        try {
            d = f.parse(date);
            databasedate = d.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        long difference = currenttime - databasedate;
        if (difference < 86400000) {
            int chatday = Integer.parseInt(date.substring(0, 2));
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            if (today_day == chatday)
                return "Just Now " + sdf.format(d);
            else if ((today_day - chatday) == 1)
                return "Yesterday " + sdf.format(d);
        } else if (difference < 172800000) {
            int chatday = Integer.parseInt(date.substring(0, 2));
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            if ((today_day - chatday) == 1)
                return "Yesterday " + sdf.format(d);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy hh:mm a");
        return sdf.format(d);
    }
}

