// abdul | Convo Inc. | 16-Jul-2018 @ 11:26:34 AM

package com.org.service.common;

/**
 * @author abdul
 * @version 1.0 ResponseType holds the different response types
 */
public final class ResponseType {
	public static final String JSON = "JSON";
	public static final String XLSX = "XLSX";
	public static final String PDF = "PDF";
	public static final String XML = "XML";
	public static final String DOC = "DOC";
	public static final String TEXT = "TEXT";

}
