// abdul | Convo Inc. | 09-Apr-2018 @ 12:22:21 PM

package com.org.service.api.rest.response;

/**
 * @author abdul
 * @version 1.0 ResponseModel holds the response message structure used
 *          throughout the whole application for REST APIs
 */
public class ExtendedResponseModel extends ResponseModel {

	String kpi = "";
	String startTimStamp = "";
	String endTimStamp = "";

	/**
	 * @param type
	 *            {@link Integer} default 1
	 * @param message
	 *            {@link String} default empty
	 * @param revisionNo
	 *            {@link Long} default -1
	 * @param errorCode
	 *            {@link String} default null
	 * @param _explicitType
	 *            {@link String} default CONSTANT.SCRYBE_RESPONSE
	 * @param data
	 *            {@link IResponseData} data implementation will be passed
	 */
	public ExtendedResponseModel(int type, String message, long revisionNo, String errorCode, String _explicitType,
			String KPI, Object data) {
		super(type, message, revisionNo, errorCode, _explicitType, data);
		this.kpi = KPI;

	}

	public ExtendedResponseModel(int type, String message, long revisionNo, String errorCode, String _explicitType,
			String KPI, String from, String to, Object data) {
		super(type, message, revisionNo, errorCode, _explicitType, data);
		this.kpi = KPI;
		this.startTimStamp = from;
		this.endTimStamp = to;

	}

	/**
	 * @return the startTimStamp
	 */
	public String getStartTimStamp() {
		return startTimStamp;
	}

	/**
	 * @param startTimStamp
	 *            the startTimStamp to set
	 */
	public void setStartTimStamp(String startTimStamp) {
		this.startTimStamp = startTimStamp;
	}

	/**
	 * @return the endTimStamp
	 */
	public String getEndTimStamp() {
		return endTimStamp;
	}

	/**
	 * @param endTimStamp
	 *            the endTimStamp to set
	 */
	public void setEndTimStamp(String endTimStamp) {
		this.endTimStamp = endTimStamp;
	}

	/**
	 * @return the kpi
	 */
	public String getKpi() {
		return kpi;
	}

	/**
	 * @param kpi
	 *            the kpi to set
	 */
	public void setKpi(String kpi) {
		this.kpi = kpi;
	}
}
