// abdul | Convo Inc. | 09-Apr-2018 @ 12:33:23 PM

package com.org.service.api.rest;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.service.api.rest.response.ExtendedResponseModel;
import com.org.service.api.rest.response.IResponse;
import com.org.service.api.rest.response.IResponseFactory;
import com.org.service.api.rest.response.ResponseFactory;
import com.org.service.api.rest.response.ResponseModel;
import com.org.service.common.KPI;
import com.org.service.common.ResponseType;
import com.org.service.dao.repository.ApplicantRepository;
import com.org.service.dao.service.ApplicantService;
import com.org.service.locale.CONSTANT;
import com.org.service.locale.i18Messages;
import com.org.service.logger.LogManager;
import com.org.service.model.Applicant;
import com.org.service.security.authentication.AuthenticationManager;
import com.org.service.selfhealer.ResourceNotFoundException;

/**
 * @author Abdulrehman abdul
 * @version 1.0 ContentAPIController holds the all APIs for user
 *          analytics @revision-1 : Message structure changes recommended by
 *          client team for unified rest interface to support the unfication.
 *          The resulted response data: object will contain key name in key and
 *          value object in value key. JSON FORMAT
 * @author abdul 4/May/2018
 */
@RestController
@RequestMapping("/api/bio")
public class ApplicantAPIController {
	LogManager logger = new LogManager(this.getClass().getName());
	IResponseFactory responseFactory = ResponseFactory.getInstance();

	private final String fileName = " [ " + this.getClass().getName() + " ] ";

	@Autowired
	ApplicantRepository userRepo;

	@Autowired
	ApplicantService applicantService;

	@GetMapping("/test")
	public Object getTestMessage(@RequestParam(value = "responseType", defaultValue = "JSON") String responseType) {
		final String methodName = "getTestMessage";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			String data = "Application is running successfully :)";
			if (responseType.equals(ResponseType.XLSX)) {
				responseModel = new ExtendedResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						KPI.USER_OVERVIEW_PLOTED, data);
			} else {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE, data);
			}
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}

	@GetMapping("/get/all")
	public Object getAllUsers(@RequestParam(value = "responseType", defaultValue = "JSON") String  responseType,
			@RequestParam(value = "client_id", defaultValue = "") String clientId,
			@RequestParam(value = "token", defaultValue = "") String token) {
		
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		boolean isLogedin = AuthenticationManager.authenticateByAccessToken(clientId,token);
		if(!isLogedin) {
			responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
			responseModel.setMessage(CONSTANT.UNAUTHORISED);
			IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
			return responseObj.getResponse();			
		}

		final String methodName = "getAllUsers";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		 responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			if (responseType.equals(ResponseType.XLSX)) {
				responseModel = new ExtendedResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						KPI.ALL_USERS, userRepo.findAll());
			} else {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						userRepo.findAll());
			}
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}
	
	@GetMapping("/unseen")
	public Object getUnseen(
			@RequestParam(value = "responseType", defaultValue = "JSON") String responseType,
			@RequestHeader(value = "client_id", defaultValue = "") String clientId,
			@RequestHeader(value = "token", defaultValue = "") String token) {
		
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		boolean isLogedin = AuthenticationManager.authenticateByAccessToken(clientId,token);
		if(!isLogedin) {
			responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
			responseModel.setMessage(CONSTANT.UNAUTHORISED);
			IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
			return responseObj.getResponse();			
		}
		
		

		Applicant applicant = new Applicant();
		applicant.setSeen_by("UNSEEN");


		final String methodName = "getUnseen";
		List<Applicant> users = applicantService.searchUser(applicant);
		List<Applicant> updatedApplicants = new ArrayList<>();

		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		try {
			if (responseType.equals(ResponseType.XLSX)) {
				responseModel = new ExtendedResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						KPI.ALL_USERS, users);
			} else {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE, users);
			}
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}
	
	
	

	public void update(List<Applicant> users, String clientId) {
		List<Applicant> updatedApplicants = new ArrayList<>();
			for (int i = 0; i < users.size(); i++) {
				Applicant app = users.get(i);
				app.setSeen_by(clientId);
				updatedApplicants.add(app);
			}
			userRepo.saveAll(updatedApplicants);
	}
	
	
	

	@GetMapping("/search2")
	public Object search2(@RequestParam(value = "trade", defaultValue = "") String trade,
			@RequestParam(value = "age") long age, @RequestParam(value = "height", defaultValue = "0") long height,
			@RequestParam(value = "qualification", defaultValue = "") String qualification,
			@RequestParam(value = "english_speaking_level", defaultValue = "0") long english_speaking_level,
			@RequestParam(value = "english_writing_level", defaultValue = "0") long english_writing_level,
			@RequestParam(value = "phone_num", defaultValue = "0") long phone_num,
			@RequestParam(value = "second_phone_num", defaultValue = "0") long second_phone_num,
			@RequestParam(value = "cnic", defaultValue = "0") long cnic,
			@RequestParam(value = "seen_by", defaultValue = "") String seen_by,
			@RequestParam(value = "responseType", defaultValue = "JSON") String responseType,
			@RequestParam(value = "client_id", defaultValue = "") String clientId,
			@RequestParam(value = "token", defaultValue = "") String token) {
		
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		boolean isLogedin = AuthenticationManager.authenticateByAccessToken(clientId,token);
		if(!isLogedin) {
			responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
			responseModel.setMessage(CONSTANT.UNAUTHORISED);
			IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
			return responseObj.getResponse();			
		}

		Applicant applicant = new Applicant();
		applicant.setAge(age);
		applicant.setTrade(trade);
		applicant.setHeight(height);
		applicant.setQualification(qualification);
		applicant.setEnglish_speaking_level(english_speaking_level);
		applicant.setEnglish_writing_level(english_writing_level);
		applicant.setPhone_num(phone_num);
		applicant.setSecond_phone_num(second_phone_num);
		applicant.setCnic(cnic);		
		if(seen_by.equals("UNSEEN")) {
			applicant.setSeen_by("UNSEEN");
		}else {
			applicant.setSeen_by("");
		}

		final String methodName = "search2";
		List<Applicant> users = applicantService.searchUser(applicant);

		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		try {
			if (responseType.equals(ResponseType.XLSX)) {
				responseModel = new ExtendedResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						KPI.ALL_USERS, users);
			} else {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE, users);
			}
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}
	
	

	@PostMapping("/search")
	public Object findAllMatch(@RequestBody Applicant user,
			@RequestHeader(value = "client_id", defaultValue = "") String clientId,
			@RequestHeader(value = "token", defaultValue = "") String token) {
		
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		boolean isLogedin = AuthenticationManager.authenticateByAccessToken(clientId,token);
		if(!isLogedin) {
			responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
			responseModel.setMessage(CONSTANT.UNAUTHORISED);
			IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
			return responseObj.getResponse();			
		}
		
		final String methodName = "findAllMatch";

		List<Applicant> users = applicantService.searchUser(user);

		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		 responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE, users);
			if(user.getSeen_by().equals("UNSEEN")) {
				update(users, clientId);
			}
			
			
		} catch (ConstraintViolationException ex) {
			responseModel.setMessage(ex.getMessage());
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}
	
	
	@PostMapping("/unauth/search")
	public Object unAuthSearch(@RequestBody Applicant user) {
		
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		
		final String methodName = "findAllMatch";
		if(user.getCnic() < 1 && user.getPhone_num()<1) {
			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE, null);
			
		}else {
			List<Applicant> users = applicantService.searchUser(user);

			logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
			 responseModel = new ResponseModel(1, "", -1,
					String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
					null);
			try {
				responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
						CONSTANT.REST_RESPONSE, users);
			} catch (ConstraintViolationException ex) {
				responseModel.setMessage(ex.getMessage());
			} catch (Exception e) {
				responseModel.setMessage(e.getMessage());
			}
		}

		
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}
	
	

	@PostMapping("/create")
	public Object createUser(@RequestBody Applicant applicant) {
		final String methodName = "createUser";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		Applicant user = new Applicant();
		user.setCnic(applicant.getCnic());
		List<Applicant> list = applicantService.searchUser(user);
		try {
			if (list.size() > 0) {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						list.get(0));
			} else {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						userRepo.save(applicant));
			}

		} catch (ConstraintViolationException ex) {
			responseModel.setMessage(ex.getMessage());
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}

	@GetMapping("/get/{id}")
	public Object getUserById(@PathVariable(value = "id") Long id,
			@RequestParam(value = "responseType", defaultValue = "JSON") String responseType) {
		final String methodName = "getUserById";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE,
					userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id)));
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}

	@GetMapping("/getby/{property}/{value}")
	public Object getUserByPhoneNumber(@PathVariable(value = "property") String property,
			@PathVariable(value = "value") String value,
			@RequestParam(value = "responseType", defaultValue = "JSON") String responseType) {
		final String methodName = "getUserById";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			List<Applicant> userList = new ArrayList<Applicant>();
			switch (property) {
			case "phone":
				userList = userRepo.findByPhoneNumber(value, value);
				break;

			case "email":
				userList = userRepo.findByEmail(value);
				break;
			case "trade":
				userList = userRepo.findByTrade(value);
				break;
			case "age":
				userList = userRepo.findByAge(Long.parseLong(value));
				break;
			default:
				break;
			}
			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE, userList);
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}

	@PutMapping("/update/{id}")
	public Object updateUser(@PathVariable(value = "id") Long id, @Valid @RequestBody Applicant applicant) {

		final String methodName = "updateUser";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			Applicant user = userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

			applicant.setId(user.getId());

			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE, userRepo.save(applicant));
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();
	}

	@DeleteMapping("/delete/{id}")
	public Object deleteUser(@PathVariable(value = "id") Long id) {
		final String methodName = "deleteUser";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			Applicant user = userRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
			userRepo.delete(user);

			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE, null);
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}

}
