package com.org.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.org.service.locale.i18Messages;
import com.org.service.logger.LogManager;
import com.org.service.security.authentication.AuthenticationManager;
import com.org.service.util.UTIL;

/**
 * @author abdul
 * @version 1.0 AppInitializer holds the initialization for Analytics REST
 *          API
 */

@SuppressWarnings("deprecation")
@SpringBootApplication
@EnableJpaAuditing
@EnableSpringConfigured
public class AppInitializer extends SpringBootServletInitializer {
	private static LogManager logger = new LogManager(AppInitializer.class.getName());

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AppInitializer.class);
	}

	@Bean
	public HttpMessageConverters customConverters() {
		ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
		return new HttpMessageConverters(arrayHttpMessageConverter);
	}

	public static void main(String[] args) throws Exception {
		logger.info(i18Messages.APP_INIT);
		AuthenticationManager.init(new DBAuthorizeManager());
		//AppInitializer.init();
		ApplicationContext ctx = SpringApplication.run(AppInitializer.class, args);
		UTIL.setApplicationContext(ctx);
		logger.info("Application is started successfully :)");
	}

	// @SuppressWarnings("deprecation")
	// @Bean
	// public WebMvcConfigurer corsConfigurer() {
	//
	// return new WebMvcConfigurerAdapter() {
	// @Override
	// public void addCorsMappings(CorsRegistry registry) {
	//
	// registry.addMapping("/api/**")
	// .allowedOrigins("http://localhost.convo.com/analytics")
	// .allowCredentials(true).maxAge(3600);
	// }
	//
	// };
	//
	// }
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE", "OPTIONS").allowedOrigins("*")
						.allowCredentials(true);
			}

		};

	}

}