package com.org.service.api.rest.reports.view.views.xlsx;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import com.org.service.common.KPI;
import com.org.service.logger.LogManager;

/**
 * @author abdul
 * @version 1.0
 * 
 *          XLSXView holds the implementation for .xlsx file
 */

public class XLSXView extends AbstractXlsxView {
	String className = this.getBeanName();
	LogManager logger = new LogManager(this.getClass().getName());
	private IExcelRendererFactory factory = RendererFactory.getInstance();

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) {
		String methodName = "buildExcelDocument";

		try {
			@SuppressWarnings("unchecked")
			Map<String, Object> data = (Map<String, Object>) model.get("data");
			IReportRenderer rendrer = factory.getRenderer(String.valueOf(data.get("kpi")));
			
			response.setHeader("Content-Disposition", "attachment; filename="+"\""+String.valueOf(KPI.FILE_NAME.get(data.get("kpi"))+".xlsx\""));
			rendrer.renderReport(workbook, data);
		} catch (Exception e) {
			// We are logging here because its the end-point execution and can not throw
			// otherwise we have to throw the exception to the end-point execution
			logger.error(className + methodName);
		}

	}

}