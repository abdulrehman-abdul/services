// abdul | Convo Inc. | 07-Dec-2018 @ 7:25:19 PM

package com.org.service.dao.repository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.org.service.model.Applicant;

/**
 * @author abdul
 * @version 1.0 ApplicantRepository holds the
 */
@Repository
public interface ApplicantRepository extends JpaRepository<Applicant, Long> {

	/**
	 * This methods 
	 * @param bioForm
	 * @return Applicant
	 */
	@SuppressWarnings("unchecked")
	Applicant save(@Valid Applicant bioForm);
	
	@Query(value = "SELECT * FROM APPLICANT u WHERE u.phone_num = ?1 OR u.second_phone_num = ?2", nativeQuery = true)
	List<Applicant> findByPhoneNumber(String phone_num, String second_phone_num);
	
	@Query(value = "SELECT * FROM APPLICANT u WHERE u.email = ?1", nativeQuery = true)
	List<Applicant> findByEmail(String email);
	
	@Query(value = "SELECT * FROM APPLICANT u WHERE u.age <= ?1", nativeQuery = true)
	List<Applicant> findByAge(long age);
	
	
	@Query(value = "SELECT * FROM APPLICANT u WHERE u.trade = ?1", nativeQuery = true)
	List<Applicant> findByTrade(String trade);

	//@Query("SELECT f FROM Foo f WHERE LOWER(f.name) = LOWER(:name)")
    //Foo retrieveByName(@Param("name") String name);
}
