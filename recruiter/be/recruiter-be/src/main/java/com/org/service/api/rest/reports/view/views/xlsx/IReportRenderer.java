// abdul | Convo Inc. | 16-Jul-2018 @ 3:17:37 PM

package com.org.service.api.rest.reports.view.views.xlsx;

import org.apache.poi.ss.usermodel.Workbook;

/**
 * @author abdul
 * @version 1.0 IReportRenderer holds structure to render the excel report
 */
public interface IReportRenderer {
	/**
	 * Renders the .xls/.xlsx report from provided data
	 * 
	 * @param data
	 *            may be any object holding the data
	 * @param workbook
	 *            ref. of {@link Workbook}
	 * @param data
	 *            void
	 */
	abstract public void renderReport(Workbook workbook, Object data) throws Exception;
}
