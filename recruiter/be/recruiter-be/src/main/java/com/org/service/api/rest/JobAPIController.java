// abdul | Convo Inc. | 09-Apr-2018 @ 12:33:23 PM

package com.org.service.api.rest;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.service.api.rest.response.ExtendedResponseModel;
import com.org.service.api.rest.response.IResponse;
import com.org.service.api.rest.response.IResponseFactory;
import com.org.service.api.rest.response.ResponseFactory;
import com.org.service.api.rest.response.ResponseModel;
import com.org.service.common.KPI;
import com.org.service.common.ResponseType;
import com.org.service.dao.repository.JobRepository;
import com.org.service.dao.service.ApplicantService;
import com.org.service.locale.CONSTANT;
import com.org.service.locale.i18Messages;
import com.org.service.logger.LogManager;
import com.org.service.model.Job;
import com.org.service.security.authentication.AuthenticationManager;
import com.org.service.selfhealer.ResourceNotFoundException;

/**
 * @author Abdulrehman abdul
 * @version 1.0 ContentAPIController holds the all APIs for user
 *          analytics @revision-1 : Message structure changes recommended by
 *          client team for unified rest interface to support the unfication.
 *          The resulted response data: object will contain key name in key and
 *          value object in value key. JSON FORMAT
 * @author abdul 4/May/2018
 */
@RestController
@RequestMapping("/api/job")
public class JobAPIController {
	LogManager logger = new LogManager(this.getClass().getName());
	IResponseFactory responseFactory = ResponseFactory.getInstance();

	private final String fileName = " [ " + this.getClass().getName() + " ] ";

	@Autowired
	JobRepository jobRepo;

	@Autowired
	ApplicantService applicantService;

	@GetMapping("/test")
	public Object getTestMessage(@RequestParam(value = "responseType", defaultValue = "JSON") String responseType) {
		final String methodName = "getTestMessage";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			String data = "Application is running successfully :)";
			if (responseType.equals(ResponseType.XLSX)) {
				responseModel = new ExtendedResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						KPI.USER_OVERVIEW_PLOTED, data);
			} else {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE, data);
			}
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}

	@GetMapping("/get/all")
	public Object getJobs(@RequestParam(value = "responseType", defaultValue = "JSON") String  responseType,
			@RequestParam(value = "client_id", defaultValue = "") String clientId,
			@RequestParam(value = "token", defaultValue = "") String token) {
		
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
//		boolean isLogedin = AuthenticationManager.authenticateByAccessToken(clientId,token);
//		if(!isLogedin) {
//			responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
//			responseModel.setMessage(CONSTANT.UNAUTHORISED);
//			IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
//			return responseObj.getResponse();			
//		}

		final String methodName = "getJobs";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		 responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			if (responseType.equals(ResponseType.XLSX)) {
				responseModel = new ExtendedResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						KPI.ALL_USERS, jobRepo.findAll());
			} else {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						jobRepo.findAll());
			}
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}


	
	

	
	

	@PostMapping("/create")
	public Object createJob(@RequestBody Job job,
			@RequestHeader(value = "client_id", defaultValue = "") String clientId,
			@RequestHeader(value = "token", defaultValue = "") String token) {
		
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		boolean isLogedin = AuthenticationManager.authenticateByAccessToken(clientId,token);
		if(!isLogedin) {
			responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
			responseModel.setMessage(CONSTANT.UNAUTHORISED);
			IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
			return responseObj.getResponse();			
		}
		
		
		
		final String methodName = "createJob";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		 responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		
		try {
			responseModel = new ResponseModel(1, "", -1,
					String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
					jobRepo.save(job));

		} catch (ConstraintViolationException ex) {
			responseModel.setMessage(ex.getMessage());
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}




	@PutMapping("/update/{id}")
	public Object updateUser(@PathVariable(value = "id") Long id, @Valid @RequestBody Job applicant) {

		final String methodName = "updateUser";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			Job user = jobRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Job", "id", id));

			applicant.setId(user.getId());

			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE, jobRepo.save(applicant));
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();
	}

	@DeleteMapping("/delete/{id}")
	public Object deleteUser(@PathVariable(value = "id") Long id,
			@RequestHeader(value = "client_id", defaultValue = "") String clientId,
			@RequestHeader(value = "token", defaultValue = "") String token) {
		
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		boolean isLogedin = AuthenticationManager.authenticateByAccessToken(clientId,token);
		if(!isLogedin) {
			responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
			responseModel.setMessage(CONSTANT.UNAUTHORISED);
			IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
			return responseObj.getResponse();			
		}
		
		
		final String methodName = "deleteUser";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		 responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			Job user = jobRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
			jobRepo.delete(user);

			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE, null);
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}

}
