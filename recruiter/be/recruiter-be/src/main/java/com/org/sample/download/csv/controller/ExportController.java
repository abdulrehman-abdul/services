package com.org.sample.download.csv.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.org.sample.download.csv.service.UserService;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-excel-csv-pdf
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/02/18
 * Time: 06.32
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class ExportController {
    @Autowired
    UserService userService;

    /**
     * Handle request to download an Excel document
     */
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public Object download(@RequestParam(value = "accountID", defaultValue = "") String accountID,
			@RequestParam(value = "authToken", defaultValue = "") String authToken,
			@RequestParam(value = "userID", defaultValue = "") String userID,
			@RequestParam(value = "fieldName", defaultValue = "") String fieldName,
			@RequestParam(value = "fieldValue", defaultValue = "") String fieldValue, 
			@RequestParam(value = "responseType", defaultValue = "JSON") String responseType, 
			HttpServletRequest request, HttpServletResponse response,Model model) {
    	System.out.println("request recieved");
        model.addAttribute("users", userService.findAllUsers());
        return "";
    }
}
