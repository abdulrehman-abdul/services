// abdul | Convo Inc. | 12-Dec-2018 @ 6:59:58 PM
 
package com.org.service;

import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import com.org.service.dao.repository.UserRepository;
import com.org.service.model.User;
import com.org.service.security.authorization.IAuthorizeManager;

/**
 * @author abdul
 * @version 1.0
 * DBAuthorizeManager holds the
 */
@Service
public class DBAuthorizeManager implements IAuthorizeManager, ApplicationContextAware {
//	@Autowired
	private static  ApplicationContext applicationContext;
	
//	@Autowired
//	private  UserRepository regUserRepo;

	/* (non-Javadoc)
	 * @see com.org.service.security.authorization.IAuthorizeManager#isUserAuthorised(java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean isUserAuthorised(String userName, String operation) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.org.service.security.authorization.IAuthorizeManager#isUserPresent(java.lang.String)
	 */
	@Override
	public Boolean isUserPresent(String userName) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.org.service.security.authorization.IAuthorizeManager#authorizeUser(java.util.Map, java.lang.Object)
	 */
	@Override
	public Boolean authorizeUser(Map<String, String> props, Object servisInjObjt) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.org.service.security.authorization.IAuthorizeManager#authorizeUser(java.lang.String, java.lang.String)
	 */
	@Override
	public Boolean authorizeUser(String userName, String password) {
		  UserRepository regUserRepo = applicationContext.getBean(UserRepository.class);;
		User reUser = regUserRepo.findByEmail(userName);
		if(reUser == null) {
			return false;
		}
		else if(!password.equals(reUser.getPassword())) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.org.service.security.authorization.IAuthorizeManager#authorizeUser(java.lang.String, java.lang.String, java.util.Map)
	 */
	@Override
	public String authorizeUser(String userName, String password, Map<String, String> optAttributes) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		applicationContext=arg0;
		
	}

}
