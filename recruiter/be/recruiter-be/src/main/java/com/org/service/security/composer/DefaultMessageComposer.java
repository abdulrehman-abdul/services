package com.org.service.security.composer;

/**
 * @author Abdul-Rehman This is default message composer class which works as
 *         ADAPTEE for the {@link ResponseComposer}. If any custom response
 *         composer is not provided then {@link DefaultMessageComposer} works as
 *         default response composer
 * 
 */
public class DefaultMessageComposer implements MessageComposerI {
	private static final String RESPONSE_TAG = "response";
	private static final String STATUS_TAG = "status";
	private static final String OK = "ok";
	private static final String WARNING = "warning";
	private static final String EXCEPTION = "exception";
	private static final String TYPE_TAG = "type";
	private static final String value_TAG = "value";

	/**
	 * For detailed comment please review {@link MessageComposerI}
	 */
	public String composeErrorResponse(Object object, String dataType) {
		if (object == null || dataType == null || dataType.isEmpty())
			return composeErrorResponse("Empty Arg",
					"JSON");
		try {
//			if (dataType.equalsIgnoreCase(RestFulConstant.JSON)) {
			if (dataType.equalsIgnoreCase("JSON")) {
				com.google.gson.JsonObject jsonObject = new com.google.gson.JsonObject();
				com.google.gson.JsonObject respJsonObject = new com.google.gson.JsonObject();
				if (object.getClass().getName().equals(String.class.getName())) {
					respJsonObject.addProperty(STATUS_TAG, WARNING);
					respJsonObject.addProperty(value_TAG, object.toString());
				} else if (object instanceof Exception) {
					respJsonObject.addProperty(STATUS_TAG, EXCEPTION);
					respJsonObject.addProperty(TYPE_TAG, object.getClass()
							.getName());
					respJsonObject.addProperty(value_TAG,
							((Exception) object).getMessage());
				}
				jsonObject.add(RESPONSE_TAG, respJsonObject);
				return jsonObject.toString();
			} else
				return composeErrorResponse("DataType Mismatch", dataType);
		} catch (Exception e) {
			return composeErrorResponse(e, dataType);
		}

	}

	/**
	 * For detailed comment please review {@link MessageComposerI}
	 */
	public String composeOKResponse(String message, String dataType) {
		if (dataType == null || dataType.isEmpty())
				return composeErrorResponse("Empty Arg",
						"JSON");
			try {
//				if (dataType.equalsIgnoreCase(RestFulConstant.JSON)) {
				if (dataType.equalsIgnoreCase("JSON")) {
				com.google.gson.JsonObject jsonObject = new com.google.gson.JsonObject();
				com.google.gson.JsonObject respJsonObject = new com.google.gson.JsonObject();
				respJsonObject.addProperty(STATUS_TAG, OK);
				respJsonObject.addProperty(value_TAG, message);
				jsonObject.add(RESPONSE_TAG, respJsonObject);
				return jsonObject.toString();
			} else
				return composeErrorResponse("DataType Mismatch", dataType);
		} catch (Exception e) {
			return composeErrorResponse(e, dataType);
		}
	}

	/**
	 * For detailed comment please review {@link MessageComposerI}
	 */
	@Override
	public String composeOKResponse(Object obj, String dataType) {
		// TODO Auto-generated method stub
		return null;
	}

}
