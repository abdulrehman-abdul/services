// abdul | Convo Inc. | 14-Apr-2018 @ 12:57:30 AM

package com.org.service.util;

/**
 * @author abdul
 * @version 1.0 TypeObject holds the
 */
public class TypeObject {
	String order;
	DataType type;

	/**
	 * @param order
	 * @param type
	 * @param value
	 */
	public TypeObject(String order, DataType type, Object value) {
		super();
		this.order = order;
		this.type = type;
		this.value = value;
	}

	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}

	/**
	 * @param order
	 *            the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	/**
	 * @return the type
	 */
	public DataType getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(DataType type) {
		this.type = type;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	Object value;

}
