// abdul | Convo Inc. | 07-Dec-2018 @ 7:25:19 PM

package com.org.service.dao.repository;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.org.service.model.User;

/**
 * @author abdul
 * @version 1.0 ApplicantRepository holds the
 */
@Service
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	/**
	 * This methods
	 * 
	 * @param bioForm
	 * @return Applicant
	 */
	@SuppressWarnings("unchecked")
	User save(@Valid User user);

	@Query(value = "SELECT * FROM USER u WHERE u.email = ?1", nativeQuery = true)
	User findByEmail(String email);

}
