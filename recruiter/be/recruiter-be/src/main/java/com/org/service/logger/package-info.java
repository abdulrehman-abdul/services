// abdul | Convo Inc. | 08-Apr-2018 @ 5:41:16 PM
/**
 * @author abdul
 * @version 1.0
 * package-info holds the information related to the logger package. 
 * logger will hold the all logic for logger component
 */
package com.org.service.logger;