// abdul | Convo Inc. | 16-Jul-2018 @ 10:29:13 AM

package com.org.service.api.rest.response;

/**
 * @author abdul
 * @version 1.0 IResponseFactory holds the factory abstraction for an response
 *          factory. ResponseFactory will generate the different responses
 */
public interface IResponseFactory {
	/**
	 * This method will return reponse Object
	 * @return Object | NULL
	 */
	public abstract IResponse getResponse(String type, Object Data);
}
