// abdul | Convo Inc. | 16-Jul-2018 @ 3:23:06 PM

package com.org.service.api.rest.reports.view.views.xlsx.rendrer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.org.service.api.rest.reports.view.views.xlsx.IReportRenderer;
import com.org.service.common.KPI;
import com.org.service.model.Applicant;

/**
 * @author abdul
 * @version 1.0 UserOverViewRenderer renders the report for user-overview
 *          endpoint
 */
public class AllUsersRenderer implements IReportRenderer {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.org.service.api.rest.reports.view.views.xlsx.IReportRenderer#renderReport(
	 * java.lang.Object)
	 */
	@Override
	public void renderReport(Workbook workbook, Object data) throws Exception {
		String methodName = " [ renderReport ] ";
		try {

			@SuppressWarnings("unchecked")
			Map<String, Object> dataMap = (Map<String, Object>) data;
			String kpi = (String) dataMap.get("kpi");
			@SuppressWarnings("unchecked")
			List<Applicant> dataObj = (List<Applicant>) dataMap.get("data");

			Sheet sheet = workbook.createSheet(KPI.filNames().get(kpi));
			sheet.setDefaultColumnWidth(19);

			// create style for header cells
			CellStyle style = workbook.createCellStyle();
			Font font = workbook.createFont();
			font.setFontName("Calibri");
			short height = 11;
			font.setFontHeightInPoints(height);

			style.setFont(font);

			int rowIndex = 0;
			// create header row
			Row header = sheet.createRow(rowIndex);
			header.createCell(0).setCellValue("Filter ");
			header.getCell(0).setCellStyle(style);
			header.createCell(1).setCellValue("Overall");
			header.getCell(1).setCellStyle(style);

			rowIndex++;
			Row header2 = sheet.createRow(rowIndex);
			header2.createCell(0).setCellValue("Date ");
			header2.getCell(0).setCellStyle(style);

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			header2.createCell(1).setCellValue(dtf.format(now));
			header2.getCell(1).setCellStyle(style);

			rowIndex++;
			Row header3 = sheet.createRow(rowIndex);
			header3.createCell(0).setCellValue("User ID");
			header3.getCell(0).setCellStyle(style);
			header3.createCell(1).setCellValue("Name");
			header3.getCell(1).setCellStyle(style);
			header3.createCell(2).setCellValue("Age");
			header3.getCell(2).setCellStyle(style);
			header3.createCell(3).setCellValue("Date Of Birth");
			header3.getCell(3).setCellStyle(style);
			header3.createCell(4).setCellValue("Height");
			header3.getCell(4).setCellStyle(style);
			header3.createCell(5).setCellValue("Weight");
			header3.getCell(5).setCellStyle(style);
			header3.createCell(6).setCellValue("English Writing Level");
			header3.getCell(6).setCellStyle(style);
			header3.createCell(7).setCellValue("English Speaking Level");
			header3.getCell(7).setCellStyle(style);
			header3.createCell(8).setCellValue("qualification");
			header3.getCell(8).setCellStyle(style);
			header3.createCell(9).setCellValue("Trade");
			header3.getCell(9).setCellStyle(style);
			header3.createCell(10).setCellValue("Phone Number");
			header3.getCell(10).setCellStyle(style);
			header3.createCell(11).setCellValue("Secondary Phone Number");
			header3.getCell(11).setCellStyle(style);
			header3.createCell(12).setCellValue("Email");
			header3.getCell(12).setCellStyle(style);
			header3.createCell(13).setCellValue("Passport Number");
			header3.getCell(13).setCellStyle(style);
			header3.createCell(14).setCellValue("Passport Expiry");
			header3.getCell(14).setCellStyle(style);
			header3.createCell(15).setCellValue("Applied Date");
			header3.getCell(15).setCellStyle(style);
			header3.createCell(16).setCellValue("Local Experience");
			header3.getCell(16).setCellStyle(style);
			header3.createCell(17).setCellValue("Overseas Experience");
			header3.getCell(17).setCellStyle(style);
			header3.createCell(18).setCellValue("Social Media");
			header3.getCell(18).setCellStyle(style);
			header3.createCell(19).setCellValue("Seen By");
			header3.getCell(19).setCellStyle(style);

			rowIndex++;
			for (int i = 0; i < dataObj.size(); i++) {
				Row userRow = sheet.createRow(rowIndex++);
				Applicant userObj = dataObj.get(i);
				userRow.createCell(0).setCellValue(String.valueOf(userObj.getId()));
				userRow.createCell(1).setCellValue(String.valueOf(userObj.getName()));
				userRow.createCell(2).setCellValue(String.valueOf(userObj.getAge()));
				userRow.createCell(3).setCellValue(String.valueOf(userObj.getDate_of_birth()));
				userRow.createCell(4).setCellValue(String.valueOf(userObj.getHeight()));
				userRow.createCell(5).setCellValue(String.valueOf(userObj.getWeight()));
				userRow.createCell(6).setCellValue(String.valueOf(userObj.getEnglish_writing_level()));
				userRow.createCell(7).setCellValue(String.valueOf(userObj.getEnglish_speaking_level()));
				userRow.createCell(8).setCellValue(String.valueOf(userObj.getQualification()));
				userRow.createCell(9).setCellValue(String.valueOf(userObj.getTrade()));
				userRow.createCell(10).setCellValue(String.valueOf(userObj.getPhone_num()));
				userRow.createCell(11).setCellValue(String.valueOf(userObj.getSecond_phone_num()));
				userRow.createCell(12).setCellValue(String.valueOf(userObj.getEmail()));
				userRow.createCell(13).setCellValue(String.valueOf(userObj.getPassport()));
				userRow.createCell(14).setCellValue(String.valueOf(userObj.getPassport_expiry()));
				userRow.createCell(15).setCellValue(String.valueOf(userObj.getCreated_at()));
				userRow.createCell(16).setCellValue(String.valueOf(userObj.getLocal_experience()));
				userRow.createCell(17).setCellValue(String.valueOf(userObj.getOverseas_experience()));
				userRow.createCell(18).setCellValue(String.valueOf(userObj.getSocial_media_interaction()));
				userRow.createCell(19).setCellValue(String.valueOf(userObj.getSeen_by()));

			}

		} catch (Exception e) {
			throw new Exception("Exception at: " + this.getClass().getName() + methodName);
		}
	}

}
