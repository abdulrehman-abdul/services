// abdul | Convo Inc. | 16-Jul-2018 @ 3:20:21 PM

package com.org.service.api.rest.reports.view.views.xlsx;

import com.org.service.api.rest.reports.view.views.xlsx.rendrer.AllUsersRenderer;
import com.org.service.common.KPI;

/**
 * @author abdul
 * @version 1.0 RendererFactory holds implementation for RendererFactory. This
 *          factory will provide the objects of {@link IReportRenderer}
 * 
 *          PATTERN: ABSTRACT-FACTORY, SINGLETON
 */
public class RendererFactory implements IExcelRendererFactory {

	static IExcelRendererFactory factory = null;

	/**
	 * Private contructor for SIGLETON to limit the objects creation
	 */
	private RendererFactory() {
	};

	/**
	 * Provides the initialized object
	 * 
	 * Pattern: Singleton
	 * 
	 * @return IExcelRendererFactory | NUll
	 */
	public static IExcelRendererFactory getInstance() {
		if (factory == null) {
			factory = new RendererFactory();
		}
		return factory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.api.rest.reports.view.views.xlsx.IExcelRendererFactory#
	 * getRenderer()
	 */
	@Override
	public IReportRenderer getRenderer(String type) {
		// USER_TOP_PLOTED
		switch (type) {
		case KPI.ALL_USERS:
			return new AllUsersRenderer();
		default:
			return null;
		}
	}

}
