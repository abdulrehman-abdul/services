// abdul | Convo Inc. | 08-Jun-2018 @ 10:07:32 PM
 
package com.org.service;

import javax.annotation.PostConstruct;

import com.org.service.locale.i18Messages;
import com.org.service.logger.LogManager;

/**
 * @author abdul
 * @version 1.0
 * StartUpInit holds the
 */
//@Component
public class StartUpInit {
	private static LogManager logger = new LogManager(AppInitializer.class.getName());
  @PostConstruct
  public void init(){
     // init code goes here
	  logger.info(i18Messages.APP_INIT);
  }
}