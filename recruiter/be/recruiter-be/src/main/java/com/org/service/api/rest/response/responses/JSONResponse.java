// abdul | Convo Inc. | 16-Jul-2018 @ 11:38:50 AM

package com.org.service.api.rest.response.responses;

import com.org.service.api.rest.response.IResponse;

/**
 * @author abdul
 * @version 1.0 JSONResponse holds the JSON response metainfo
 */
public class JSONResponse implements IResponse {
	private Object response = null;// response Object

	public JSONResponse(Object localResponse) {
		this.response = localResponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.api.rest.response.IResponse#getResponse()
	 */
	@Override
	public Object getResponse() {
		return this.response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.api.rest.response.IResponse#getResponseType()
	 */
	@Override
	public Object getResponseType() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.api.rest.response.IResponse#getResponseMetaInfo()
	 */
	@Override
	public Object getResponseMetaInfo() {
		// TODO Auto-generated method stub
		return null;
	}

}
