package com.org.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;

import com.org.service.api.rest.reports.view.resolver.xlsx.XLSXResolver;
import com.org.service.locale.i18Messages;
import com.org.service.logger.LogManager;

/**
 * User: Abdul
 */

@SuppressWarnings("deprecation")
@Configuration
@EnableScheduling
@EnableJpaRepositories
@EnableTransactionManagement
public class SpringConfig extends WebMvcConfigurerAdapter {
	private LogManager logger = new LogManager(this.getClass().getName());
	private final String fileName = " [ " + this.getClass().getName() + " ] ";

	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.defaultContentType(MediaType.APPLICATION_JSON).favorPathExtension(true);
	}

	/*
	 * Configure ContentNegotiatingViewResolver
	 */
	// @Bean
	public ViewResolver contentNegotiatingViewResolver(ContentNegotiationManager manager) {
		final String methodName = "contentNegotiatingViewResolver";
		ContentNegotiatingViewResolver resolver = new ContentNegotiatingViewResolver();
		resolver.setContentNegotiationManager(manager);

		// Define all possible view resolvers
		List<ViewResolver> resolvers = new ArrayList<>();

		resolvers.add(xlsxResolver());
		// resolvers.add(pdfViewResolver());
		logger.info(fileName+"->"+methodName+i18Messages.VIEWS_REGISTRATION+i18Messages.SUCCESSFUL);

		return resolver;
	}

	/*
	 * Configure PdfView resolver to provide XLS output using Apache POI library to
	 * generate XLS output for an object content
	 */
	@Bean
	public ViewResolver xlsxResolver() {
		return new XLSXResolver();
	}

	/*
	 * Configure PdfView resolver to provide Pdf output using iText library to
	 * generate pdf output for an object content
	 */
	@Bean
	public ViewResolver pdfViewResolver() {
		return new XLSXResolver();
		// when implementation is provided then we will replace XLSXResolver with
		// PDFResolver
	}

	// @Scheduled(cron = "0 */1 * * * ?") every minute
	// @Scheduled(cron = "0 0 13 * * ?") 1:00 PM daily
	// @Scheduled(cron = "0 */1 * * * ?")
	// public void everyAlgoCRON() {
	// final String methodName = " [ everyAlgoCRON ] ";
	// try {
	// System.err.println("CRON is running---");
	// logger.info(fileName + methodName + i18Messages.CRON_STARTED);
	// } catch (Exception e) {
	// logger.error(fileName + methodName + i18Messages.CRON_FAIlED +
	// e.getMessage());
	//
	// }
	//
	// }

}
