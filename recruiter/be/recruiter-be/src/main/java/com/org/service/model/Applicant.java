// abdul | Convo Inc. | 07-Dec-2018 @ 6:19:23 PM

package com.org.service.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author abdul
 * @version 1.0
 * BioFormModel holds the
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by rajeevkumarsingh on 27/06/17.
 */
@Entity
@Table(name = "applicant")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "created_at", "updated_at" }, allowGetters = true)
public class Applicant {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date created_at;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	private Date updated_at;

	@NotBlank(message="Blank name is not allowed")
	@Size(min = 4, max = 40, message 
		    = "Name must be between 10 and 40 characters")
	private String name;
	private long age;
	
	@NotNull(message="Blank Height is not allowed")
	private long height;
	
	@NotNull(message="Blank Date of birth is not allowed")
	private Date date_of_birth;
	
//	@Min(value=40, message="Less than 40 Weight is not allowed")
	@Max(value=200, message="More than 200 Weight is not allowed")
	private long weight;
	
	@NotNull(message="Blank phone_num is not allowed")
	private long phone_num;
	
	@NotNull(message="Blank English Speaking is not allowed")
	private long english_speaking_level;
	
	@NotNull(message="Blank English Writhing is not allowed")
	private long english_writing_level;
	
	private long english_listening_level;
	
	private long cnic;
	

	
	/**
	 * @return the cnic
	 */
	public long getCnic() {
		return cnic;
	}

	/**
	 * @param cnic the cnic to set
	 */
	public void setCnic(long cnic) {
		this.cnic = cnic;
	}

	@NotBlank(message="Trade is blank")
	private String trade;
	
	@NotBlank(message="Qualification is Blank")
	private String qualification;
	
	@NotBlank(message="City is blank")
	private String city;
	
	@NotBlank(message="Country is blank")
	private String country;
	
	
	
//	@NotBlank(message="Email is blank")
	private String email;
	private String seen_by = "UNSEEN";
	/**
	 * @return the seen_by
	 */
	public String getSeen_by() {
		return seen_by;
	}

	/**
	 * @param seen_by the seen_by to set
	 */
	public void setSeen_by(String seen_by) {
		this.seen_by = seen_by;
	}

	private String passport;
	private Date passport_expiry;
	private String overseas_experience;
	private String local_experience;
	private long second_phone_num;
	private long social_media_interaction;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the created_at
	 */
	public Date getCreated_at() {
		return created_at;
	}

	/**
	 * @param created_at
	 *            the created_at to set
	 */
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	/**
	 * @return the updated_at
	 */
	public Date getUpdated_at() {
		return updated_at;
	}

	/**
	 * @param updated_at
	 *            the updated_at to set
	 */
	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public long getAge() {
		return age;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(long age) {
		this.age = age;
	}

	/**
	 * @return the height
	 */
	public long getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            the height to set
	 */
	public void setHeight(long height) {
		this.height = height;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the trade
	 */
	public String getTrade() {
		return trade;
	}

	/**
	 * @param trade
	 *            the trade to set
	 */
	public void setTrade(String trade) {
		this.trade = trade;
	}

	/**
	 * @return the qualification
	 */
	public String getQualification() {
		return qualification;
	}

	/**
	 * @param qualification
	 *            the qualification to set
	 */
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	/**
	 * @return the date_of_birth
	 */
	public Date getDate_of_birth() {
		return date_of_birth;
	}

	/**
	 * @param date_of_birth
	 *            the date_of_birth to set
	 */
	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	/**
	 * @return the weight
	 */
	public long getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            the weight to set
	 */
	public void setWeight(long weight) {
		this.weight = weight;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the passport
	 */
	public String getPassport() {
		return passport;
	}

	/**
	 * @param passport
	 *            the passport to set
	 */
	public void setPassport(String passport) {
		this.passport = passport;
	}

	/**
	 * @return the passport_expiry
	 */
	public Date getPassport_expiry() {
		return passport_expiry;
	}

	/**
	 * @param passport_expiry
	 *            the passport_expiry to set
	 */
	public void setPassport_expiry(Date passport_expiry) {
		this.passport_expiry = passport_expiry;
	}

	/**
	 * @return the overseas_experience
	 */
	public String getOverseas_experience() {
		return overseas_experience;
	}

	/**
	 * @param overseas_experience
	 *            the overseas_experience to set
	 */
	public void setOverseas_experience(String overseas_experience) {
		this.overseas_experience = overseas_experience;
	}

	/**
	 * @return the local_experience
	 */
	public String getLocal_experience() {
		return local_experience;
	}

	/**
	 * @param local_experience
	 *            the local_experience to set
	 */
	public void setLocal_experience(String local_experience) {
		this.local_experience = local_experience;
	}

	/**
	 * @return the phone_num
	 */
	public long getPhone_num() {
		return phone_num;
	}

	/**
	 * @param phone_num
	 *            the phone_num to set
	 */
	public void setPhone_num(long phone_num) {
		this.phone_num = phone_num;
	}

	/**
	 * @return the second_phone_num
	 */
	public long getSecond_phone_num() {
		return second_phone_num;
	}

	/**
	 * @param second_phone_num
	 *            the second_phone_num to set
	 */
	public void setSecond_phone_num(long second_phone_num) {
		this.second_phone_num = second_phone_num;
	}

	/**
	 * @return the english_speaking_level
	 */
	public long getEnglish_speaking_level() {
		return english_speaking_level;
	}

	/**
	 * @param english_speaking_level
	 *            the english_speaking_level to set
	 */
	public void setEnglish_speaking_level(long english_speaking_level) {
		this.english_speaking_level = english_speaking_level;
	}


	/**
	 * @return the english_writing_level
	 */
	public long getEnglish_writing_level() {
		return english_writing_level;
	}

	/**
	 * @param english_writing_level the english_writing_level to set
	 */
	public void setEnglish_writing_level(long english_writing_level) {
		this.english_writing_level = english_writing_level;
	}

	/**
	 * @return the english_listening_level
	 */
	public long getEnglish_listening_level() {
		return english_listening_level;
	}

	/**
	 * @param english_listening_level the english_listening_level to set
	 */
	public void setEnglish_listening_level(long english_listening_level) {
		this.english_listening_level = english_listening_level;
	}

	/**
	 * @return the social_media_interaction
	 */
	public long getSocial_media_interaction() {
		return social_media_interaction;
	}

	/**
	 * @param social_media_interaction
	 *            the social_media_interaction to set
	 */
	public void setSocial_media_interaction(long social_media_interaction) {
		this.social_media_interaction = social_media_interaction;
	}

}
