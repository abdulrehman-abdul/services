// abdul | Convo Inc. | 16-Jul-2018 @ 3:27:38 PM
 
package com.org.service.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @author abdul
 * @version 1.0
 * KPI holds the
 */
public interface KPI {
	//iom data
	public static String ALL_USERS = "ALL_USERS";
	
	
	public static String USER_OVERVIEW_PLOTED = "USER_OVERVIEW_PLOTED";
	public static String USER_OVERVIEW_DOWNLOAD = "USER_OVERVIEW_DOWNLOAD";
	public static String USER_ADDED_PLOTED = "USER_ADDED_PLOTED";
	public static String USER_ADDED_DOWNLOAD = "USER_ADDED_DOWNLOAD";
	public static String USER_REMOVED_PLOTED = "USER_REMOVED_PLOTED";
	public static String USER_REMOVED_DOWNLOAD = "USER_REMOVED_DOWNLOAD";
	public static String USER_TOP_PLOTED = "USER_TOP_PLOTED";
	public static String USER_TOP_DOWNLOAD = "USER_TOP_DOWNLOAD";
	public static String USER_SEARCH_BY_FIELD_PLOTED = "USER_SEARCH_BY_FIELD_PLOTED";
	public static String USER_SEARCH_BY_FIELD_DOWNLOAD = "USER_SEARCH_BY_FIELD_DOWNLOAD";
	
	public static String CONTENT_TOTAL_ENGAGEMENT_PLOTED = "CONTENT_TOTAL_ENGAGEMENT_PLOTED";
	public static String CONTENT_TOTAL_ENGAGEMENT_DOWNLOAD = "CONTENT_TOTAL_ENGAGEMENT_DOWNLOAD";
	
	public static String CONTENT_POSTS_PLOTED = "CONTENT_POSTS_PLOTED";
	public static String CONTENT_POSTS_DOWNLOAD = "CONTENT_POSTS_DOWNLOAD";
	public static String CONTENT_GROUPS_BY_USER_PLOTED = "CONTENT_GROUPS_BY_USER_PLOTED";
	public static String CONTENT_GROUPS_BY_USER_DOWNLOAD = "CONTENT_GROUPS_BY_USER_DOWNLOAD";
	public static String CONTENT_GROUPS_BY_CONTENT_PLOTED = "CONTENT_GROUPS_BY_CONTENT_PLOTED";
	public static String CONTENT_GROUPS_BY_CONTENT_DOWNLOAD = "CONTENT_GROUPS_BY_CONTENT_DOWNLOAD";
	
	public static final Map<String, String> FILE_NAME = filNames();
	
	public static Map<String, String> filNames (){
		Map<String, String> FILE_NAME = new HashMap<String, String>();
		FILE_NAME.put(ALL_USERS, "Users List");
		
		
		FILE_NAME.put(USER_OVERVIEW_PLOTED, "Users OverView Plotted");
		FILE_NAME.put(USER_OVERVIEW_DOWNLOAD, "Users OverView-Overall");
		FILE_NAME.put(USER_ADDED_PLOTED, "Users Added - Plotted");
		FILE_NAME.put(USER_ADDED_DOWNLOAD, "Users Added - Overall");
		FILE_NAME.put(USER_REMOVED_PLOTED, "Users Removed - Plotted");
		FILE_NAME.put(USER_REMOVED_DOWNLOAD, "Users Removed - Overall");
		FILE_NAME.put(USER_TOP_PLOTED, "Top Contributors-Plotted");
		FILE_NAME.put(USER_TOP_DOWNLOAD, "Top Contributors-Overall");
		FILE_NAME.put(USER_SEARCH_BY_FIELD_PLOTED, "Users Search by field");
		FILE_NAME.put(USER_SEARCH_BY_FIELD_DOWNLOAD, "Users Search by field");
		FILE_NAME.put(CONTENT_TOTAL_ENGAGEMENT_PLOTED, "Total Engagements Plotted");
		FILE_NAME.put(CONTENT_TOTAL_ENGAGEMENT_DOWNLOAD, "Total Engagements Overall");
		FILE_NAME.put(CONTENT_POSTS_PLOTED, "Top Posts Plotted");
		FILE_NAME.put(CONTENT_POSTS_DOWNLOAD, "Top Posts Overall");
		FILE_NAME.put(CONTENT_GROUPS_BY_USER_PLOTED, "Top Groups By Users Plotted");
		FILE_NAME.put(CONTENT_GROUPS_BY_USER_DOWNLOAD, "Top Groups By Users Overall");
		FILE_NAME.put(CONTENT_GROUPS_BY_CONTENT_PLOTED, "Top Groups By Engagements - Plotted");
		FILE_NAME.put(CONTENT_GROUPS_BY_CONTENT_DOWNLOAD, "Top Groups By Engagements Overall");
		
		
		
		return FILE_NAME;
	}
}
