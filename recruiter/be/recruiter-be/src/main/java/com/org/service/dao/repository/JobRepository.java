// abdul | Convo Inc. | 07-Dec-2018 @ 7:25:19 PM

package com.org.service.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.org.service.model.Job;

/**
 * @author abdul
 * @version 1.0 ApplicantRepository holds the
 */
@Service
@Repository
public interface JobRepository extends JpaRepository<Job, Long> {
}
