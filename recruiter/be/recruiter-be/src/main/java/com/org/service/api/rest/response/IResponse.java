// abdul | Convo Inc. | 16-Jul-2018 @ 11:17:49 AM
 
package com.org.service.api.rest.response;

/**
 * @author abdul
 * @version 1.0
 * IResponse represents to main response object
 */
public interface IResponse {
	
	/**This method returns reponse object
	 * @return Object
	 */
	public abstract Object getResponse();

	/**This method returns reponse object
	 * @return Object
	 */
	public abstract Object getResponseType();
	
	/**This method returns reponse info
	 * @return Object
	 */
	public abstract Object getResponseMetaInfo();
}
