/*@author Abdul-Rehman
 *Abstract Overview: This file
 *Revision#1: 
 */
package com.org.service.security.composer;

/**
 * @author Abdul-Rehman | CONVO Inc.
 * 
 *         This composes the response message which is returned to the Client
 *         for specific resource. You can provide your own message composer by
 *         implementing the interface {@link MessageComposerI}. If any custom
 *         response composer is not provided then {@link DefaultMessageComposer}
 *         works as default response composer.
 */
public class ResponseComposer {
	private static MessageComposerI composer = new DefaultMessageComposer();

	/**
	 * Initializes the MessageComposerI with singleton  DesignPattern. If
	 * custom implementation is not used then default implementation
	 * {@link DefaultMessageComposer} will be used
	 * 
	 * @param messageComposer implementation of {@link MessageComposerI}
	 */
	public static void init(MessageComposerI messageComposer) {
		if (messageComposer != null
				&& composer.getClass().getName()
						.equals(DefaultMessageComposer.class.getName()))
			composer = messageComposer;
	}

	/**
	 * This method composes the response message if we want to return an
	 * error/warning status message in response
	 * 
	 * @param object
	 *            {@link Exception} object or error message
	 * @param dataType
	 *            XML/JSON etc
	 * @return {@link String}/NULL
	 */
	public static final String composeErrorResponse(Object object,
			String dataType) {
		return composer.composeErrorResponse(object, dataType);
	}

	/**
	 * This method composes the response message if we want to return an OK
	 * status message in response with value
	 * 
	 * @param message
	 *            message value
	 * @param dataType
	 *            XML/JSON etc
	 * @return {@link String}/NULL
	 */
	public static final String composeOKResponse(String message, String dataType) {
		return composer.composeOKResponse(message, dataType);
	}

	/**
	 * This method composes the response message if we want to return an OK
	 * status message in response with value
	 * 
	 * @param object
	 *            message {@link Object}
	 * @param dataType
	 *            XML/JSON etc
	 * @return {@link String}/NULL
	 */
	public static final String composeOKResponse(Object object, String dataType) {
		return composer.composeOKResponse(object, dataType);
	}
}
