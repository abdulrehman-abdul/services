// abdul | Convo Inc. | 16-Jul-2018 @ 3:13:03 PM
 
package com.org.service.api.rest.reports.view.views.xlsx;

/**
 * @author abdul
 * @version 1.0
 * IExcelRendererFactory holds structure for main report rendering factory
 * 
 * Pattern: ABSTRACT-FACTORY
 */
public interface IExcelRendererFactory {
	/**Provides the instance to excel report renderer
	 * @return Object
	 */
	abstract IReportRenderer getRenderer(String type);
}
