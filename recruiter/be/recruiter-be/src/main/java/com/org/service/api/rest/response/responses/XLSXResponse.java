// abdul | Convo Inc. | 16-Jul-2018 @ 11:38:50 AM

package com.org.service.api.rest.response.responses;

import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.springframework.web.servlet.ModelAndView;

import com.org.service.api.rest.reports.view.views.xlsx.XLSXView;
import com.org.service.api.rest.response.ExtendedResponseModel;
import com.org.service.api.rest.response.IResponse;
import com.org.service.util.UTIL;

/**
 * @author abdul
 * @version 1.0 JSONResponse holds the JSON response metainfo
 */
public class XLSXResponse implements IResponse {
	private Object response = null;// response Object

	public XLSXResponse(Object localResponse) {
		ExtendedResponseModel responseModel = (ExtendedResponseModel) localResponse;
		Map<String, Object> dataMap = new HashedMap<>();
		String bigger = UTIL.getBigger(responseModel.getStartTimStamp(), responseModel.getEndTimStamp());
		String smaller = UTIL.getSmaller(responseModel.getStartTimStamp(), responseModel.getEndTimStamp());
		dataMap.put("kpi", responseModel.getKpi());
		dataMap.put("from", smaller);
		dataMap.put("to", bigger);
		dataMap.put("data", responseModel.getData());
		
		
		
		ModelAndView modelView = new ModelAndView(new XLSXView(), "data", dataMap);
		this.response = modelView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.api.rest.response.IResponse#getResponse()
	 */
	@Override
	public Object getResponse() {
		return this.response;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.api.rest.response.IResponse#getResponseType()
	 */
	@Override
	public Object getResponseType() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.api.rest.response.IResponse#getResponseMetaInfo()
	 */
	@Override
	public Object getResponseMetaInfo() {
		// TODO Auto-generated method stub
		return null;
	}

}
