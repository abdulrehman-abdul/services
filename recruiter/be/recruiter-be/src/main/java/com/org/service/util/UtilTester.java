// abdul | Convo Inc. | 14-Apr-2018 @ 12:20:17 AM

package com.org.service.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author abdul
 * @version 1.0 UtilTester holds the
 */
public class UtilTester {
	public static String ConvertMilliSecondsToFormattedDate(String milliSeconds,String dateFormat, int hours) {
		if(dateFormat == null || dateFormat.isEmpty()) {
			dateFormat = "yyyy-MM-dd";
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(milliSeconds));
		 String dateWithoutHours = simpleDateFormat.format(calendar.getTime());
		
		 dateWithoutHours = dateWithoutHours.concat(" "+hours+":00:00");
		try {
			 simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date date = simpleDateFormat.parse(dateWithoutHours);
			System.out.println(date.getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateWithoutHours;
	}

	/**
	 * This methods
	 * 
	 * @param args
	 *            void
	 */
	public static void main(String[] args) {
		try {
//			String date = ConvertMilliSecondsToFormattedDate(String.valueOf(System.currentTimeMillis()),"",0);
//			System.out.println(date);
			
			


String someDate = "2018-09-02";
SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
Date date = sdf.parse(someDate);
System.out.println(date.getTime());


			
//			Calendar cal = Calendar.getInstance();
//			long milliDiff = cal.get(Calendar.ZONE_OFFSET);
//			// Got local offset, now loop through available timezone id(s).
//			String [] ids = TimeZone.getAvailableIDs();
//			String name = null;
//			for (String id : ids) {
//			  TimeZone tz = TimeZone.getTimeZone(id);
//			  if (tz.getRawOffset() == milliDiff) {
//			    // Found a match.
//			    name = id;
//			    break;
//			  }
//			}
//			System.out.println(name);
//			
//			TimeZone timeZone = TimeZone.getDefault();
//			System.out.println(timeZone.getID());
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
