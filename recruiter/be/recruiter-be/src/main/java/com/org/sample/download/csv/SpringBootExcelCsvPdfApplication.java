package com.org.sample.download.csv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
/**
 * @author abdul
 * @version 1.0
 * SpringBootExcelCsvPdfApplication holds the spring-boot booting class.
 * To run uncomment the blow line //@SpringBootApplication
 */
//@SpringBootApplication
public class SpringBootExcelCsvPdfApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootExcelCsvPdfApplication.class, args);
	}

    @Bean
    public HttpMessageConverters customConverters() {
        ByteArrayHttpMessageConverter arrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
        return new HttpMessageConverters(arrayHttpMessageConverter);
    }
}
