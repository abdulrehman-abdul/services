package com.org.service.security.composer;

/**
 * @author Abdul-Rehman This is structural interface for message composer. This works as interface adapter
 * 
 */
public interface MessageComposerI {
	/**
	 * This method composes the response message if we want to return an
	 * error/warning status message in response
	 * 
	 * @param object
	 *            {@link Exception} object or error message
	 * @param dataType
	 *            XML/JSON etc
	 * @return {@link String}/NULL
	 */
	String composeErrorResponse(Object object, String dataType);

	/**
	 * This method composes the response message if we want to return an OK
	 * status message in response with value
	 * 
	 * @param message
	 *            message value
	 * @param dataType
	 *            XML/JSON etc
	 * @return {@link String}/NULL
	 */
	String composeOKResponse(String message, String dataType);

	/**
	 * This method composes the response message if we want to return an OK
	 * status message in response with value
	 * 
	 * @param obj
	 *            message Object
	 * @param dataType
	 *            XML/JSON etc
	 * @return {@link String}/NULL
	 */
	String composeOKResponse(Object obj, String dataType);
}
