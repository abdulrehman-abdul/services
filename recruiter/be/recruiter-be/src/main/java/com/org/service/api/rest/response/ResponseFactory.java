// abdul | Convo Inc. | 16-Jul-2018 @ 11:23:44 AM

package com.org.service.api.rest.response;

import com.org.service.api.rest.response.responses.JSONResponse;
import com.org.service.api.rest.response.responses.XLSXResponse;
import com.org.service.common.ResponseType;

/**
 * @author abdul
 * @version 1.0
 * 
 *          ResponseFactory holds the responsibility to create the responses by
 *          implementing {@link IResponseFactory}.
 * 
 *          ABSTRACT-FACTORY and SIGLETON design patterns are used
 */
public class ResponseFactory implements IResponseFactory {
	private static IResponseFactory singleFctory = null;

	private ResponseFactory() {
	}

	/**
	 * 
	 * This method initialize the factory by limiting its obejcts to control the
	 * object creation procxess
	 * 
	 * @return IResponseFactory
	 */
	public static IResponseFactory getInstance() {
		if (singleFctory == null) {
			singleFctory = new ResponseFactory();
		}
		return singleFctory;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.org.service.api.rest.response.IResponseFactory#getResponse(java.lang.
	 * String, java.lang.Object)
	 */
	@Override
	public IResponse getResponse(String type, Object Data) {
		switch (type) {
		case ResponseType.XLSX:
			return new XLSXResponse(Data);
		case ResponseType.JSON:
			return new JSONResponse(Data);
		default:
			return new JSONResponse(Data);
		}
	}

}
