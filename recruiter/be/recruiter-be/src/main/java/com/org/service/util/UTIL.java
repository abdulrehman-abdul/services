// abdul | Convo Inc. | 14-Apr-2018 @ 2:20:48 PM

package com.org.service.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.dbutils.DbUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;

import com.org.service.locale.CONSTANT;
import com.org.service.selfhealer.exceptionhandler.StorageException;

/**
 * @author abdul
 * @version 1.0 UTIL holds the all utility methods used throught the application
 */
public class UTIL {
	private static ApplicationContext ctx;

	public static void setApplicationContext(ApplicationContext appContext) throws BeansException {
		ctx = appContext;

	}

	public static ApplicationContext getApplicationContext() throws StorageException {
		return ctx;
	}
	
	
	
	
	public static String convertDateFormat(String dateString, String currentDateFormat, String dateFormat) {
		if (dateString == null || dateString.isEmpty()) {
			return "";
		}
		
		try {
			if (dateFormat == null || dateFormat.isEmpty()) {
				dateFormat = "MM/dd/yyyy";
			}
			if (currentDateFormat == null || currentDateFormat.isEmpty()) {
				currentDateFormat = "yyyy-MM-dd";
			}
			//String someDate = "2018-09-02";
			SimpleDateFormat sdf = new SimpleDateFormat(currentDateFormat);
			Date date = sdf.parse(dateString);
			return ConvertMilliSecondsToFormattedDate(String.valueOf(date.getTime()), dateFormat);
		} catch (ParseException e) {
			return "MM/dd/yyyy";
		}
	}

	/**
	 * @author Abdul This methods converts the millis to date string like
	 *         1521072000000 to '2018-03-15 00:00:00'. The default format is
	 *         "yyyy-MM-dd ss:ss:ss"
	 * 
	 * @param milliSeconds
	 *            millis of time
	 * @param dateFormat
	 *            string like "yyyy-MM-dd ss:ss:ss"
	 * @return String like '2018-03-15 00:00:00'
	 */
	public static String ConvertMilliSecondsToFormattedDate(String milliSeconds, String dateFormat) {
		if (milliSeconds == null || milliSeconds.isEmpty() || milliSeconds.equals("0")) {
			return "";
		}
		if (dateFormat == null || dateFormat.isEmpty()) {
			dateFormat = "yyyy-MM-dd ss:ss:ss";
		}
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Long.parseLong(milliSeconds));
		return simpleDateFormat.format(calendar.getTime());
	}

	/**
	 * @author Abdul This methods returns mills
	 * @return long
	 */
	public static long getCurrentMillis() {
		return System.currentTimeMillis();
	}

	/**
	 * @author Abdul This methods returns millis for next week
	 * @return long
	 */
	public static long getNextWeekMillis() {
		return getCurrentMillis() + CONSTANT.WEEK_MILLIS;
	}

	public static boolean isValidString(String str) {
		if (str == null || str.isEmpty()) {
			return false;
		}
		return true;
	}
	
	public static String formatToValidSTring(String str) {
		if (str == null || str.isEmpty() || str.equals("null") || str.equals("NULL")) {
			return "";
		}
		return str;
	}

	/**
	 * This method cleans the DB dependencies like {@link Connection}
	 * {@link Statement} {@link ResultSet}
	 * 
	 * @param conn
	 *            {@link Connection}
	 * @param stmt
	 *            {@link Statement}
	 * @param rs
	 *            {@link ResultSet} void
	 */
	public static void clean(Connection conn, Statement stmt, ResultSet rs) {
		DbUtils.closeQuietly(conn, stmt, rs);
	}

	/**
	 * This method converts strings to longs and then returns the larger value by
	 * comparing
	 * 
	 * @param from
	 *            Integral string
	 * @param to
	 *            Integral string
	 * @return String
	 */
	public static String getBigger(String from, String to) {
		if (!isValidString(from) || !isValidString(to)) {
			return "0";
		}
		try {
			long fromLong = Long.parseLong(from);
			long toLong = Long.parseLong(to);
			if (fromLong > toLong) {
				return from;
			}
			return to;

		} catch (Exception e) {
			return "0";
		}
	}

	/**
	 * This method converts strings to longs and then returns the smaller value by
	 * comparing
	 * 
	 * @param from
	 *            Integral string
	 * @param to
	 *            Integral string
	 * @return String
	 */
	public static String getSmaller(String from, String to) {
		String bigger = getBigger(from, to);
		if (bigger.equals(from)) {
			return to;
		}
		return from;
	}

	public static void printDates(String from, String to) {
		String start_date = UTIL.ConvertMilliSecondsToFormattedDate(String.valueOf(from), "yyyy-MM-dd");
		String end_date = UTIL.ConvertMilliSecondsToFormattedDate(String.valueOf(to), "yyyy-MM-dd");

		System.out.println("---------time-stamps-----------");
		System.out.println("from: " + from);
		System.out.println("to: " + to);
		System.out.println("---------dates-----------");
		System.out.println("from: " + start_date);
		System.out.println("to: " + end_date);
	}

	/**
	 * This method finds the provided key's value
	 * 
	 * @param list
	 *            List<Map<String, String>>
	 * @param key
	 *            any string
	 * @return String \ ""
	 */
	public static String getValue(List<Map<String, String>> list, String key) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).get("key").equals(key)) {
				return list.get(i).get("value");
			}
		}
		return "";
	}

	/**
	 * This sorts the map by comparator
	 * 
	 * @param unsortMap
	 *            Map<String, List<Map<String, String>>>
	 * @return Map<String,List<Map<String,String>>>
	 */
	public static Map<String, List<Map<String, String>>> sortByValue(Map<String, List<Map<String, String>>> unsortMap) {
		// 1. Convert Map to List of Map
		List<Map.Entry<String, List<Map<String, String>>>> list = new LinkedList<Map.Entry<String, List<Map<String, String>>>>(
				unsortMap.entrySet());

		// 2. Sort list with Collections.sort(), provide a custom Comparator
		// Try switch the o1 o2 position for a different order
		Collections.sort(list, new Comparator<Map.Entry<String, List<Map<String, String>>>>() {
			public int compare(Map.Entry<String, List<Map<String, String>>> o1,
					Map.Entry<String, List<Map<String, String>>> o2) {
				return Integer.valueOf((getValue(o2.getValue(), "score")))
						.compareTo(Integer.valueOf(getValue(o1.getValue(), "score")));
			}
		});

		// 3. Loop the sorted list and put it into a new insertion order Map
		// LinkedHashMap
		Map<String, List<Map<String, String>>> sortedMap = new LinkedHashMap<String, List<Map<String, String>>>();
		for (Map.Entry<String, List<Map<String, String>>> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	/**
	 * Returns timezone
	 * 
	 * @return String
	 */
	public static String getTimezone() {
//		return (CONSTANT.Property.TIMEZONE != null || !CONSTANT.Property.TIMEZONE.isEmpty())
//				? CONSTANT.Property.TIMEZONE
//				: "UTC";
		TimeZone timeZone = TimeZone.getDefault();
		return timeZone.getID();
	}

	/**
	 * returns the millis by adding to hours to date like if you have provided
	 * current time suppose (2018-08-17 08:20:00) and hour like 11 then method will
	 * return the millis of date like 2018-08-17 11:00:00
	 * 
	 * @param milliSeconds millis of date
	 * @param hours
	 * @return long
	 */
	public static long getTimeToHour(long milliSeconds, int hours) {
		final String dateFormat = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		String dateWithoutHours = simpleDateFormat.format(calendar.getTime());

		dateWithoutHours = dateWithoutHours.concat(" " + hours + ":00:00");
		try {
			simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			Date date = simpleDateFormat.parse(dateWithoutHours);
			return date.getTime();
		} catch (ParseException e) {
			return 0;
		}
	}

}
