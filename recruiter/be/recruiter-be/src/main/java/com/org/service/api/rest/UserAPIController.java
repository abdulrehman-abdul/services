// abdul | Convo Inc. | 09-Apr-2018 @ 12:33:23 PM

package com.org.service.api.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.service.api.rest.response.ExtendedResponseModel;
import com.org.service.api.rest.response.IResponse;
import com.org.service.api.rest.response.IResponseFactory;
import com.org.service.api.rest.response.ResponseFactory;
import com.org.service.api.rest.response.ResponseModel;
import com.org.service.common.KPI;
import com.org.service.dao.repository.UserRepository;
import com.org.service.dao.service.UserService;
import com.org.service.locale.CONSTANT;
import com.org.service.locale.i18Messages;
import com.org.service.logger.LogManager;
import com.org.service.model.User;
import com.org.service.security.authentication.AuthenticationManager;
import com.org.service.security.authentication.oauth2.template.OAuth2TokenI;

/**
 * @author Abdulrehman abdul
 * @version 1.0 ContentAPIController holds the all APIs for user
 *          analytics @revision-1 : Message structure changes recommended by
 *          client team for unified rest interface to support the unfication.
 *          The resulted response data: object will contain key name in key and
 *          value object in value key. JSON FORMAT
 * @author abdul 4/May/2018
 */
@RestController
@RequestMapping("/api/user")
public class UserAPIController {
	LogManager logger = new LogManager(this.getClass().getName());
	IResponseFactory responseFactory = ResponseFactory.getInstance();

	private final String fileName = " [ " + this.getClass().getName() + " ] ";

	@Autowired
	UserRepository regUserRepo;

	@Autowired
	UserService userService;
	
	
	@GetMapping("/test")
	public Object getTestMessage() {
		final String methodName = "getTestMessage";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			String data = "Application is running successfully :)";
			responseModel = new ExtendedResponseModel(1, "", -1,
					String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
					KPI.USER_OVERVIEW_PLOTED, data);
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();
	}

	@PostMapping("/register")
	public Object register(@RequestHeader("name") String name, @RequestHeader("email") String email,
			@RequestHeader("password") String password) {
		final String methodName = "register";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			User reUser = new User();
			reUser.setEmail(email);
			List<User> userList = userService.searchUser(reUser);
//			User reUser = regUserRepo.findByEmail(email);
			if(userList.size() < 1) {
				 reUser = new User();
				 reUser.setEmail(email);
				 reUser.setPassword(password);
				 reUser.setName(name);
				 reUser = regUserRepo.save(reUser);
				 responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
							CONSTANT.REST_RESPONSE,reUser );
			}else {
				responseModel.setMessage("Already Registered with this Email");				
			}
			
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}

	@PostMapping("/login")
	public Object login(@RequestHeader("email") String email, @RequestHeader("password") String password) {
		final String methodName = "login";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		logger.info("request recieved at /login");
		try {
			logger.info("going to authorize at /login");
			//Boolean isLogin = AuthenticationManager.authorizeUser(email, password);
			
			User user = new User();
			user.setEmail(email);
			List<User> userList = userService.searchUser(user);
			
			if (userList.size() < 1 || !userList.get(0).getPassword().equals(password) ) {
				responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
				responseModel.setMessage(i18Messages.UN_AUTHORIZED);
				
				logger.error(i18Messages.UN_AUTHORIZED +"at /login");
			} else {
				OAuth2TokenI oAuthToken = AuthenticationManager.getAuthToken(email, "SECRET");
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						oAuthToken);
			}

		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}
	
	@PostMapping("/authorize")
	public Object authorize(@RequestHeader("clientId") String clientId, @RequestHeader("refreshToken") String refreshToken) {
		final String methodName = "login";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			OAuth2TokenI oAuthToken = AuthenticationManager.getAccessToken(clientId);

			//User reUser = regUserRepo.findByEmail(email);
			if (oAuthToken == null || !oAuthToken.getRefreshToken().equals(refreshToken)) {
				responseModel.setErrorCode(String.valueOf(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED));
				responseModel.setMessage(i18Messages.UN_AUTHORIZED);
			} else {
				oAuthToken = AuthenticationManager.getAuthToken(clientId, "SECRET");
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						oAuthToken);
			}

		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();

	}

}
