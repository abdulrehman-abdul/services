// abdul | Convo Inc. | 08-Apr-2018 @ 11:34:38 PM
 
package com.org.service.selfhealer.exceptionhandler;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author abdul
 * @version 1.0
 * StorageException holds the exceptions related to the StorageAPI
 */
public class StorageException extends Exception {

	private static final long serialVersionUID = 1L;
	private String code = "200 OK";
	/**
     * Constructs a new exception with {@code null} as its detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     */
    public StorageException() {
        super();
    }
    public StorageException(String message) {
        super("Exception in storage api:"+message);
    }
    
    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }
    public StorageException(String message, Throwable cause, String code) {
        super(message, cause);
        this.code = code;
    }
    public StorageException(Throwable cause, String code) {
        super(cause);
        this.code = code;
    }
    
    public String getStrackTrace() {
		final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     this.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}

}
