// abdul | Convo Inc. | 17-Dec-2018 @ 8:21:05 PM

package com.org.service.dao.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.service.dao.repository.ApplicantRepository;
import com.org.service.model.Applicant;

/**
 * @author abdul
 * @version 1.0 ApplicantService holds the
 */
@Service
public class ApplicantService {
	@Autowired
	ApplicantRepository userRepo;

	@Autowired
	EntityManager entityManager;

	public List<Applicant> searchUser(Applicant user) {
		Session session = entityManager.unwrap(Session.class);
		@SuppressWarnings("deprecation")
		Criteria cr = session.createCriteria(Applicant.class);
		if (user.getEmail() != null && !user.getEmail().isEmpty()) {
			cr.add(Restrictions.eq("email", user.getEmail()));
		}
		if (user.getAge() > 0) {
			cr.add(Restrictions.eq("age", user.getAge()));
		}
		if (user.getTrade() != null && !user.getTrade().isEmpty()) {
			cr.add(Restrictions.eq("trade", user.getTrade()));
		}
		if (user.getHeight() > 0) {
			cr.add(Restrictions.eq("height", user.getHeight()));
		}
		if (user.getQualification() != null && !user.getQualification().isEmpty()) {
			cr.add(Restrictions.eq("qualification", user.getQualification()));
		}
		if (user.getEnglish_writing_level() > 0) {
			cr.add(Restrictions.eq("english_writing_level", user.getEnglish_writing_level()));
		}
		if (user.getEnglish_speaking_level() > 0) {
			cr.add(Restrictions.eq("english_speaking_level", user.getEnglish_speaking_level()));
		}
		if (user.getPhone_num() > 0) {
			cr.add(Restrictions.eq("phone_num", user.getPhone_num()));
		}
		if (user.getSecond_phone_num() > 0) {
			cr.add(Restrictions.eq("second_phone_num", user.getSecond_phone_num()));
		}
		if (user.getCnic() > 0) {
			cr.add(Restrictions.eq("cnic", user.getCnic()));
		}
		if (user.getSeen_by()!= null && !user.getSeen_by().isEmpty()) {
			cr.add(Restrictions.eq("seen_by", user.getSeen_by()));
		}
		List<Applicant> users = cr.list();
		return users;
	}

}
