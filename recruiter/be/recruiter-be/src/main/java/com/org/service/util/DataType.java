// abdul | Convo Inc. | 14-Apr-2018 @ 12:58:58 AM
 
package com.org.service.util;

/**
 * @author abdul
 * @version 1.0
 * DataType holds the
 */
public enum DataType {
	String, Date, TimeStamp, Long, Boolean
}
