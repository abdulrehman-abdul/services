// abdul | Convo Inc. | 17-Dec-2018 @ 8:21:05 PM

package com.org.service.dao.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.service.model.User;

/**
 * @author abdul
 * @version 1.0 ApplicantService holds the
 */
@Service
public class UserService {
	@Autowired
	EntityManager entityManager;

	public List<User> searchUser(User user) {
		Session session = entityManager.unwrap(Session.class);
		@SuppressWarnings("deprecation")
		Criteria cr = session.createCriteria(User.class);
//		if (user.getUserId() > 0) {
//			cr.add(Restrictions.eq("userId", user.getUserId()));
//		}
		if (user.getEmail() != null && !user.getEmail().isEmpty()) {
			cr.add(Restrictions.eq("email", user.getEmail()));
		}
		if (user.getPhone_num() > 0) {
			cr.add(Restrictions.eq("phone_num", user.getPhone_num()));
		}
		if (user.getSecond_phone_num() > 0) {
			cr.add(Restrictions.eq("second_phone_num", user.getSecond_phone_num()));
		}
		List<User> users = cr.list();
		return users;
	}

}
