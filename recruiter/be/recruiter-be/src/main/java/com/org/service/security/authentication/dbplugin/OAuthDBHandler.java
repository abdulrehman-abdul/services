/*@author Abdul-Rehman 
 *Abstract Overview: This file handles the data management (CRUD) of OAuth Objects (Keys,
 *        Tokens) in data base
 *Revision#1: @author Abdul-Rehman 
 */
package com.org.service.security.authentication.dbplugin;

import java.util.Collection;
import java.util.Vector;

import com.org.service.security.authentication.OAuthMetaDataHandlerI;
import com.org.service.security.authentication.oauth2.template.OAuth2ClientI;
import com.org.service.security.authentication.oauth2.template.OAuth2TokenI;

/**
 * @author Abdul-Rehman | CONVO Inc.
 * 
 *         This class handles the data management (CRUD) of OAuth Objects (Keys,
 *         Tokens) in data base. This class works as ADAPTER for
 *         AuthMetaDataHandler. OAuthDBHandler class handles the OAuth OAuth
 *         objects using WebNMS persistence mechanism
 */
@SuppressWarnings("unchecked")
// Added for storage API unchecked arguments
public class OAuthDBHandler implements OAuthMetaDataHandlerI {
	/**
	 * Detailed Comments are available in {@link OAuthMetaDataHandlerI}
	 */
	@Override
	public OAuth2ClientI getClientByKey_DB(String clientId) {
		java.util.Properties props = new java.util.Properties();
		props.put("clientId", clientId);
		Vector<OAuth2ClientI> ptasks;
		//
		return null;
	}

	/**
	 * Detailed Comments are available in {@link OAuthMetaDataHandlerI}
	 */
	@Override
	public OAuth2ClientI insertClientByKey_DB(OAuth2ClientI client) {

		OAuth2ClientI clientObj = new OAuthClient_POJO(client.getId(), client.getSecret(), client.getOwner(), null);

		String name = clientObj.getClass().getName();

		java.util.Properties props = new java.util.Properties();
		props.put("clientId", client.getId());
		// db logic here
		return null;
	}

	/**
	 * Detailed Comments are available in {@link OAuthMetaDataHandlerI}
	 */
	@Override
	public Collection<OAuth2ClientI> getClientList() {
		// db logic here
		return null;
	}

	/**
	 * Detailed Comments are available in {@link OAuthMetaDataHandlerI}
	 */
	@Override
	public OAuth2TokenI getOAuthTokenByKey_DB(String clientId) {
		java.util.Properties props = new java.util.Properties();
		props.put("clientId", clientId);
		Vector<OAuth2TokenI> ptasks;
		// db logic here
		return null;
	}

	/**
	 * Detailed Comments are available in {@link OAuthMetaDataHandlerI}
	 */
	@Override
	public Collection<OAuth2TokenI> getOAuthTokenList() {
		// db logic here
		return null;
	}

	Integer a = null;

	/**
	 * Detailed Comments are available in {@link OAuthMetaDataHandlerI}
	 */
	@Override
	public boolean insertOrUpdateOAuthToken_DB(String clientId, OAuth2TokenI token) {
		// db logic here
		return false;
	}

}
