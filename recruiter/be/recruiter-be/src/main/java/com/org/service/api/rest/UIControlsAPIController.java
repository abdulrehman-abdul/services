// abdul | Convo Inc. | 09-Apr-2018 @ 12:33:23 PM

package com.org.service.api.rest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.service.api.rest.response.ExtendedResponseModel;
import com.org.service.api.rest.response.IResponse;
import com.org.service.api.rest.response.IResponseFactory;
import com.org.service.api.rest.response.ResponseFactory;
import com.org.service.api.rest.response.ResponseModel;
import com.org.service.common.KPI;
import com.org.service.common.ResponseType;
import com.org.service.dao.repository.ApplicantRepository;
import com.org.service.dao.service.ApplicantService;
import com.org.service.locale.CONSTANT;
import com.org.service.locale.i18Messages;
import com.org.service.logger.LogManager;
import com.org.service.model.Applicant;
import com.org.service.security.authentication.AuthenticationManager;
import com.org.service.selfhealer.ResourceNotFoundException;

/**
 * @author Abdulrehman abdul
 * @version 1.0 ContentAPIController holds the all APIs for user
 *          analytics @revision-1 : Message structure changes recommended by
 *          client team for unified rest interface to support the unfication.
 *          The resulted response data: object will contain key name in key and
 *          value object in value key. JSON FORMAT
 * @author abdul 4/May/2018
 */
@RestController
@RequestMapping("/api/ui/control/")
public class UIControlsAPIController {
	LogManager logger = new LogManager(this.getClass().getName());
	IResponseFactory responseFactory = ResponseFactory.getInstance();

	private final String fileName = " [ " + this.getClass().getName() + " ] ";


	@GetMapping("/test")
	public Object getTestMessage(@RequestParam(value = "responseType", defaultValue = "JSON") String responseType) {
		final String methodName = "getTestMessage";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			String data = "UI API is running successfully :)";
			if (responseType.equals(ResponseType.XLSX)) {
				responseModel = new ExtendedResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE,
						KPI.USER_OVERVIEW_PLOTED, data);
			} else {
				responseModel = new ResponseModel(1, "", -1,
						String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK), CONSTANT.REST_RESPONSE, data);
			}
		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse(responseType, responseModel);
		return responseObj.getResponse();
	}

	String[] trages = new String[] { "Doctor", "Engineer", "Security Guards", "Taxi drivers", "Cash custodian",
			" Aviation loader", " House keeper", " Cargo loader", " Ware house assistant", " Cleaner",
			" Aircraft cleaner", " Nursing", " Electrican", " AC techniciton", " Plumber", " Duct man", " Welder",
			" Mason", " Carpenter", " Steel fixer", " Scuff folder", " Labour", " MEP Fourman", " Civil fourman",
			"Draftsman", "Office administeration", " Gardener", "HTV driver", "Heavy equipment operator",
			"Crane operator", "Kitchen Steward", "Accountant", "Waiter", "Surveyer", "Sales man",
			" Public area cleaners", "Mosque cleaners", "Office boy", "Warehouse assistant", "Sales executive" };

	
	

	@GetMapping("/get/all")
	public Object getAllControlls() {
		 List<String> tradeList = Arrays.asList(trages);
		tradeList.sort(String::compareToIgnoreCase);
		
		String[] qualification = new String[] { "Primary","Middle", "Matriculation", "Intermediate", "Graduation", "Master", "M.Phil","PHD"};
		
		List<Integer> heightWeight = new ArrayList<>();
		for (int i = 10; i < 101; i++) {
			heightWeight.add(i);
		}
		
		List<Map<String, Object>> languageLave = new ArrayList<Map<String, Object>>();
		Map<String, Object> low = new HashMap<>();
		low.put("level", "Low");
		low.put("value", 0);
		languageLave.add(low);
	
		Map<String, Object> average = new HashMap<>();
		average.put("level", "Average");
		average.put("value", 1);
		languageLave.add(average);
		
		Map<String, Object> good = new HashMap<>();
		good.put("level", "Good");
		good.put("value", 2);
		languageLave.add(good);
		
		Map<String, Object> excelent = new HashMap<>();
		excelent.put("level", "Excelent");
		excelent.put("value", 3);
		languageLave.add(excelent);
		
		Map<String, Object> controlls = new HashMap<>();
		controlls.put("trade", tradeList);
		controlls.put("speaking_level", languageLave);
		controlls.put("writing_level", languageLave);
		controlls.put("height", heightWeight);
		controlls.put("weight", heightWeight);
		controlls.put("qualification", qualification);
		
		
		
		

		ResponseModel responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);

		final String methodName = "getAllControlls";
		logger.info(fileName + "->" + methodName + ":" + i18Messages.REQUEST_RECIEVED);
		responseModel = new ResponseModel(1, "", -1,
				String.valueOf(javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR), CONSTANT.REST_RESPONSE,
				null);
		try {
			responseModel = new ResponseModel(1, "", -1, String.valueOf(javax.servlet.http.HttpServletResponse.SC_OK),
					CONSTANT.REST_RESPONSE, controlls);

		} catch (Exception e) {
			responseModel.setMessage(e.getMessage());
		}
		IResponse responseObj = responseFactory.getResponse("JSON", responseModel);
		return responseObj.getResponse();
	}

}
