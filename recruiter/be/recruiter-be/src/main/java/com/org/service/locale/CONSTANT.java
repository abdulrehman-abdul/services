// abdul | Convo Inc. | 08-Apr-2018 @ 5:50:39 PM

package com.org.service.locale;

import java.util.Properties;

import com.org.service.util.AppProps;

/**
 * @author abdul
 * @version 1.0 CONSTANT interface holds the all constants used through the
 *          whole application
 */
public interface CONSTANT {
	static Properties appProps = AppProps.readProps();

	/**
	 * Property holds the all .peroperties values
	 * 
	 * @author abdul
	 * @version 1.0
	 */
	public static interface Property {
		public static final String TIMEZONE = appProps.getProperty("analytics.tmezone");
		public static final String ANALYTICS_API_DB = (appProps.getProperty("analytics.api.db") != null)
				? appProps.getProperty("analytics.api.db").toUpperCase()
				: appProps.getProperty("analytics.api.db");
		
		public static interface DATABASE {
			public static interface MYSQL {
				public static final String CONNECTION_STRING = appProps.getProperty("app.db.mysql.connection-string");
				public static final String USER = appProps.getProperty("app.db.mysql.user");
				public static final String PASSWORD = appProps.getProperty("app.db.mysql.password");
				public static final int CONNECTION_POOL_SIZE = Integer
						.parseInt(appProps.getProperty("app.db.mysql.pool-size"));
			}
		}

	}

	public static final String APP_NAME = "analytics";
	public static final String NULL_OBJECT = "Provided object is null";
	public static final String REST_RESPONSE = "application.communication.Response";

	public static final long WEEK_MILLIS = 604800000L;
	public static String UNAUTHORISED = "UNAUTHORISED";
	public static String REQUEST_RECIEVED = " Request recieved susscessfully ";
	public static String RESPONSE_SERVED = " Response served susscessfully ";
	public static String SERVED_TIME = " Served Time ";

	public static final Long APP_START_TIME = Long.parseLong("1262286000000");// 2010/01/01
	public static final int DAY_MILLI = 24 * 60 * 60 * 1000;

	public static interface SUPPORTED_DATASE {
		public static final String MYSQL = "MYSQL";
	}

	public static interface ORM {
		public static final String MYSQL = "MYSQL";
	}

	/**
	 * 
	 * @author abdul
	 * @version 1.0 API holds the constants all API endpoints
	 */
	public static interface API {
		public static interface KEYS {
			public static final String ACCOUNT_ID = "ACCOUNT_ID";
			public static final String START_TIMESTAMP = "START_TIMESTAMP";
			public static final String END_TIMESTAMP = "END_TIMESTAMP";
			public static final String FIELD_NAME = "FIELD_NAME";
			public static final String FIELD_VALUE = "FIELD_VALUE";
			public static final String COUNT = "COUNT";
			public static final String FIELDS = "FIELDS";
		}

		/**
		 * All user APIs
		 */
		public static interface USER {
			public static interface PLOTTED {
				public static final String USER_OVERVIEW = "USER_OVERVIEW";
				public static final String USER_ADDED = "USER_ADDED";
				public static final String USER_REMOVED = "USER_REMOVED";
				public static final String USER_TOP = "USER_TOP";
				public static final String USER_BLOCKED = "USER_BLOCKED";
			}

			public static interface OVERALL {
				public static final String USER_OVERVIEW = "OVERALL_USER_OVERVIEW";
				public static final String USER_ADDED = "OVERALL_USER_ADDED";
				public static final String USER_REMOVED = "OVERALL_USER_REMOVED";
				public static final String USER_TOP = "OVERALL_USER_TOP";
				public static final String USER_BLOCKED = "OVERALL_USER_BLOCKED";
				public static final String OVERALL_FIELD_SEARCH = "OVERALL_FIELD_SEARCH";
			}
		}

	}

}
