// abdul | Convo Inc. | 19-May-2018 @ 8:04:44 PM

package com.org.service.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author abdul
 * @version 1.0 AppProps holds the
 */
public class AppProps {
	public static Properties readProps() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			InputStream propsFileStream = AppProps.class.getClassLoader().getResourceAsStream("application.properties");
			if (propsFileStream == null) {
				//LOGGER.error("Cannot load properties file: {}, exiting", PROPERTIES_FILE);
				//System.exit(1);
				return null;
			}
			prop.load(propsFileStream);
			
			//input = new FileInputStream("application.properties");
			//prop.load(input);
			return prop;

		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("application.properties");

			// load a properties file
			prop.load(input);

			// get the property value and print it out
			System.out.println(prop.getProperty("app.db.connection"));
			System.out.println(prop.getProperty("app.db.user"));
			System.out.println(prop.getProperty("app.db.password"));

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
}
