package com.org.service.api.rest.reports.view.resolver.xlsx;

import java.util.Locale;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import com.org.service.api.rest.reports.view.views.xlsx.XLSXView;

/**
 * @author abdul
 * @version 1.0
 * 
 *          ExcelViewResolver holds the XLXS view resolver
 */
public class XLSXResolver implements ViewResolver {
	@Override
	public View resolveViewName(String s, Locale locale) {
		XLSXView view = new XLSXView();
		return view;
	}
}