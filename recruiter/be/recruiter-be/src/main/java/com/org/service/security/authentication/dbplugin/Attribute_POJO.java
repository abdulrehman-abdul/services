package com.org.service.security.authentication.dbplugin;

/**
 * @author Abdul-Rehman This Class manages the authentication attributes by
 *         attributeId-value pairs. Attributes for OAuth client and token both
 *         are managed in the same Class. The same attribute table will manage
 *         the attributes by referenceKey of parent record
 */
public class Attribute_POJO {
	private int attributeId;
	private String value;

	public Attribute_POJO(int fieldID, String name) {
		this.attributeId = fieldID;
		this.value = name;
	}

	public Attribute_POJO(String value) {
		this.value = value;
	}

	public int getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(int fieldId) {
		this.attributeId = fieldId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String fieldValue) {
		this.value = fieldValue;
	}

}
