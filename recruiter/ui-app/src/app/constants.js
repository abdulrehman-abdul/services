'use strict';

var app = angular.module('BlurAdmin');
/**@author Abdul
 * All application constant strings and constant variables will be defined here
 */
app.constant('constants', {
  appName: 'Performance Analyser',
  appVersion: 1.0,
});
