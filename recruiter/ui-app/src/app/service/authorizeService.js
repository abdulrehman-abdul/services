'use strict';

var app = angular.module('BlurAdmin');

app.service('authorizeService', function ($rootScope,$http, config) {
  $rootScope.isLoged = false;//represents the login status in whole application, its only set in this service
    
  this.authorizeCredentials = function (email,token) { 
      var password_storage = localStorage.getItem('token');
      var email_storage = localStorage.getItem('email');
      console.log("email: "+email_storage+" toke: "+password_storage);
      if(password_storage === token && email === email_storage){
        $rootScope.isLoged = true;
        return true;
      }else{
        $rootScope.isLoged = false;
        return false;
      }
    };
    var addToken = function (email,token, refreshToken) { 
      localStorage.setItem( 'token', token);
      localStorage.setItem( 'email', email );
      localStorage.setItem( 'refreshToken', refreshToken );
    };
    this.storeToken = function (email,token, refreshToken) { 
      addToken(email,token, refreshToken);
    };

    this.authorizeToken = function (callBack) {
      console.log("Authorize User");
      var refreshToken = localStorage.getItem('refreshToken');
      var email_storage = localStorage.getItem('email');
        var req = {
          method: 'POST',
          url: config.BASE_URL+'api/user/authorize',
          headers: {
            'Content-Type': 'application/json',
            'clientId':email_storage,
            'refreshToken':refreshToken
          }
          
        }
        $http(req).then(function successCallback(response) {
            if(response.data.errorCode === "200"){
              addToken(response.data.data.clientId,response.data.data.accessToken, response.data.data.refreshToken);
              $rootScope.isLoged = true;
               callBack(true)
            }else{
              $rootScope.isLoged = false;
              callBack(false);
            }
          }, function errorCallback(response) {
            $rootScope.isLoged = false;
            callBack(false);
        });
    };



    this.authorizeToken2 = function () { 
      var password_storage = localStorage.getItem('token');
      var email_storage = localStorage.getItem('email');
      console.log("email: "+email_storage+" token: "+password_storage);
      if(password_storage && email_storage){
        $rootScope.isLoged = true;
        return true;
      }else{
        $rootScope.isLoged = false;
        return false;
      }
    };
    this.getToken = function () { 
      var password_storage = localStorage.getItem('token');
      if(password_storage){
        return password_storage;
      }else{
        return null;
      }
    };
    this.getClientID = function () { 
      var email = localStorage.getItem('email');
      if(email){
        return email;
      }else{
        return null;
      }
    };
    
    this.removeToken = function (email,token) { 
      console.log("removing from local storage");
      localStorage.removeItem( 'token');
      localStorage.removeItem( 'email');
      $rootScope.isLoged = false;
    };

});