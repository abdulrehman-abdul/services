'use strict';

var app = angular.module('BlurAdmin');
/**This service will contails the all http calls to the server and will return the response in callback(response) */
app.service('httpService', function ($http,authorizeService) {
  var baseUrl = 'http://34.205.246.52:8080/';
  var validatedPost = function (url,data,callBack) {
    try {
      var req = {
        method: 'POST',
        url: url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authorizeService.getToken(),
          'clientId': authorizeService.getClientID()
        },
        data: data
      };
      $http(req).then(function successCallback(response) {
        callBack(response);
      }, function errorCallback(response) {
        callBack(null);
      });

    } catch (error) {
      throw error;
    }
  }


  var validatedGet = function (url,callBack) {
    try {
      var req = {
        method: 'GET',
        url: url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': authorizeService.getToken(),
          'clientId': authorizeService.getClientID()
        }
      }
      $http(req).then(function successCallback(response) {
        callBack(response);
      }, function errorCallback(response) {
        callBack(null);
      });

    } catch (error) {
      throw error;
    }
  }


  this.getKPIs = function (callBack) {
    try {
      var url= baseUrl+'analytics/kpi/count'
      validatedGet(url, callBack);

    } catch (error) {
      throw error;
    }
  }

  this.getTopTeamKPI = function (callBack) {
    try {
      var url= baseUrl+'analytics/kpi/topteam'
      validatedGet(url, callBack);

    } catch (error) {
      throw error;
    }
  }

  this.submitContactQuery = function (data,callBack) {
    try {
      var url= baseUrl+'portal/query'
      validatedPost(url,data, callBack);

    } catch (error) {
      throw error;
    }
  }

  this.getPortalTeam = function (callBack) {
    try {
      var url= baseUrl+'portal/team'
      validatedGet(url, callBack);

    } catch (error) {
      throw error;
    }
  }


});