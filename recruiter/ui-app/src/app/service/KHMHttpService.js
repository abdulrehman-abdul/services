'use strict';

var app = angular.module('BlurAdmin');
/**This service will contails the all http calls to the server and will return the response in callback(response) */
app.service('KHMHttpService', function ($http,authorizeService, config) {
  var baseUrl = config.BASE_URL;

  var validatedDelete = function (url,data,headers,callBack) {
    var headersObj = headers?headers:{
      'Content-Type': 'application/json',
      'token': authorizeService.getToken(),
      'client_id': authorizeService.getClientID()
    };

    try {
      var req = {
        method: 'DELETE',
        url: url,
        headers: headersObj,
        data: data
      };
      $http(req).then(function successCallback(response) {
        callBack(response);
      }, function errorCallback(response) {
        callBack(null);
      });

    } catch (error) {
      throw error;
    }
  }



  var validatedPost = function (url,data,headers,callBack) {
    var headersObj = headers?headers:{
      'Content-Type': 'application/json',
      'token': authorizeService.getToken(),
      'client_id': authorizeService.getClientID()
    };

    try {
      var req = {
        method: 'POST',
        url: url,
        headers: headersObj,
        data: data
      };
      $http(req).then(function successCallback(response) {
        callBack(response);
      }, function errorCallback(response) {
        callBack(null);
      });

    } catch (error) {
      throw error;
    }
  }


  var validatedGet = function (url,callBack) {
    try {
      var req = {
        method: 'GET',
        url: url,
        headers: {
          'Content-Type': 'application/json',
          'token': authorizeService.getToken(),
          'client_id': authorizeService.getClientID()
        }
      }
      $http(req).then(function successCallback(response) {
        callBack(response);
      }, function errorCallback(response) {
        callBack(null);
      });

    } catch (error) {
      throw error;
    }
  }

  //-----------User Login/register/auth


  this.login = function (userName, password, callBack) {
    var headers = {
      'Content-Type': 'application/json',
      'email': userName,
      'password': password
    }
    try {
      var url= baseUrl+'api/user/login'
      validatedPost(url,null, headers, callBack);

    } catch (error) {
      throw error;
    }
  };

 


  this.register = function (userName, email, password, callBack) {
    var headers = {
      'Content-Type': 'application/json',
      'email': email,
      'password': password,
      'name':userName
    }
    try {
      var url= baseUrl+'api/user/register'
      validatedPost(url,null, headers, callBack);

    } catch (error) {
      throw error;
    }
  };

  //---Applicants search
  this.searchApplicant = function (data, isResultSearch, callBack) {
    try {
      var url= baseUrl+ '/api/bio/search'
      if(isResultSearch == true){
        url= baseUrl+ '/api/bio/unauth/search'
      }
      validatedPost(url,data, null, callBack);

    } catch (error) {
      throw error;
    }
  };

    //---getUIControls
    this.getUIControls = function (callBack) {
      try {
        var url= baseUrl+ '/api/ui/control/get/all'
        validatedGet(url,callBack);
  
      } catch (error) {
        throw error;
      }
    };



  this.getUnSeenApplicants = function (callBack) {
    try {
      var url= baseUrl+'/api/bio/unseen'
      validatedGet(url, callBack);
    } catch (error) {
      throw error;
    }
  }





  //--DOWNLOAD CSV reports
  this.downloadSearchApplicants = function(searchObj){
    searchObj = validateObj(searchObj);
    var url = baseUrl+"/api/bio/search2"+prepareSearchParams(searchObj,"XLSX")+prepareAuthParams();
    window.location.href=url;
  
  }

  this.downloadAllApplicants = function(){
    window.location.href=baseUrl+'/api/bio/get/all?responseType=XLSX'+prepareAuthParams();
  }

  var validateObj = function(searchObj){
    if(!searchObj.age){
      searchObj.age = 0;
    }
    if(!searchObj.height){
      searchObj.height = 0;
    }
    if(!searchObj.cnic){
      searchObj.cnic = 0;
    }
    if(!searchObj.english_speaking_level){
      searchObj.english_speaking_level = 0;
    }
    if(!searchObj.english_writing_level){
      searchObj.english_writing_level = 0;
    }
    return searchObj;
  }
  console.log("preparing download url");
  var prepareSearchParams = function(searchObj,responseTypej){
    console.log("preparing download url");
    var writing = searchObj.english_writing_level?searchObj.english_writing_level.value:0;
    var speaking = searchObj.english_speaking_level?searchObj.english_speaking_level.value:0;
    var uri = "?trade="+searchObj.trade+"&age="+searchObj.age+"&height="+searchObj.height+"&qualification="+searchObj.qualification+"&english_speaking_level="+speaking+
    "&english_writing_level="+writing+"&phone_num="+searchObj.phone_num+"&cnic="+searchObj.cnic+"&seen_by="+searchObj.seenLevel+"&responseType="+responseTypej;
    return uri;
  }
  var prepareAuthParams = function(){
    var uri = "&token="+authorizeService.getToken()+"&client_id="+authorizeService.getClientID();
    return uri;
  }






  this.submitContactQuery = function (data,callBack) {
    try {
      var url= baseUrl+'portal/query'
      validatedPost(url,data,null, callBack);

    } catch (error) {
      throw error;
    }
  }

  this.getPortalTeam = function (callBack) {
    try {
      var url= baseUrl+'portal/team'
      validatedGet(url, callBack);

    } catch (error) {
      throw error;
    }
  }

  /*-----------------------------------KHM REST APIs----------------------------------------- */
  this.submitApply = function (data,callBack) {
    try {
      var url= baseUrl+'/api/bio/create'
      validatedPost(url,data, null, callBack);

    } catch (error) {
      throw error;
    }
  }


  this.addJob = function (data, callBack) {
    try {
      var url= baseUrl+ '/api/job/create'
      validatedPost(url,data, null, callBack);

    } catch (error) {
      throw error;
    }
  };

  this.getJobs = function (callBack) {
    try {
      var url= baseUrl+'/api/job/get/all'
      validatedGet(url, callBack);

    } catch (error) {
      throw error;
    }
  }

  this.deleteJob = function (data, callBack) {
    try {
      var url= baseUrl+ '/api/job/delete/'+data.id;
      validatedDelete(url,data, null, callBack);

    } catch (error) {
      throw error;
    }
  };


  


});