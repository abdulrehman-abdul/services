'use strict';

var app = angular.module('BlurAdmin');

app.service('util', function ($http) {

this.isValidEmail = function(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

this.specialChar = function(string) {
    // return true;
    var re = /[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/;
  return re.test(string);
}
this.replaceSpecialChars = function(str, replaceChar){
    if(!str){
        return "";
    }
    return str.replace(/[&\/\\#,@+()$~%.'":*?<>{}]/g, replaceChar);
}

});