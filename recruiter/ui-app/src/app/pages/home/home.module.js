/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.home', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, ) {
    $stateProvider.state('home', {
      url: '/home',
      templateUrl: 'app/pages/home/home.html',
      controller: 'HomeCtrl'
    }).state('jobs', {
      url: '/jobs',
      title: 'Jobs',
      templateUrl: 'app/pages/management/widgets/openings.html',

      resolve: {
        'jobs': function ($q, $state, authorizeService, importerAnalyzerService, KHMHttpService) {
          console.log("validating user");
          var deferred = $q.defer();
          var jobs = [];
          KHMHttpService.getJobs(function successCallback(response) {
            if (response.data.errorCode === "401") {
              $state.go('signin');
            } else if (response.data.errorCode === "200" && response.data.message === "") {
              jobs = response.data.data;
              deferred.resolve(jobs);
            } else {
              console.dir(response);
              deferred.resolve(jobs);
            }
          }, function errorCallback(response) {
            console.log(prefix + ".importProjectRest:" + response)
            $scope.loading = 0;
            callBack(false)
          }
          )

          return deferred.promise;
        }
      },
      controller: 'ManagementCtrl'
    }).state('apply', {
      url: '/apply',
      title: 'Apply ',
      templateUrl: 'app/pages/home/apply.html',
      controller: 'HomeCtrl'
    }).state('result', {
      url: '/result',
      title: 'Result ',
      templateUrl: 'app/pages/management/search/resultSearch.html',
      resolve: {
        'jobs': function ($q, $state, authorizeService, importerAnalyzerService, KHMHttpService) {
          console.log("validating user");
          var deferred = $q.defer();
          var jobs = [];
          KHMHttpService.getJobs(function successCallback(response) {
            if (response.data.errorCode === "401") {
              $state.go('signin');
            } else if (response.data.errorCode === "200" && response.data.message === "") {
              jobs = response.data.data;
              deferred.resolve(jobs);
            } else {
              console.dir(response);
              deferred.resolve(jobs);
            }
          }, function errorCallback(response) {
            console.log(prefix + ".importProjectRest:" + response)
            $scope.loading = 0;
            callBack(false)
          }
          )

          return deferred.promise;
        }
      },
      controller: 'ManagementCtrl'
    }).state('panalyser', {
      url: '/panalyser',
      title: 'Performance Analyser ',
      templateUrl: 'app/pages/home/panalyser.html',
      controller: 'HomeCtrl'
    }).state('panalytics', {
      url: '/panalytics',
      title: 'Perdictive Analitcs ',
      templateUrl: 'app/pages/home/panalytics.html',
      controller: 'HomeCtrl'
    }).state('team', {
      url: '/team',
      title: 'Team',
      templateUrl: 'app/pages/home/team.html',
      controller: 'HomeCtrl'
    })
      // .state('about', {
      //   url: '/about',
      //   title: 'About',
      //   templateUrl: 'app/pages/home/about.html'
      // })
      ;
  }

})();
