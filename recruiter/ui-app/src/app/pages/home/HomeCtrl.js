/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.home')
    .controller('HomeCtrl', HomeCtrl);

  /** @ngInject */
  function HomeCtrl($scope, fileReader, $filter, $uibModal, $http,$state,authorizeService,util, httpService, KHMHttpService, city ) {

    /*-------------------------------------------Globals for all pages----------------------------------------------- */
    $scope.isApplied = false;
    $scope.info = "";

/*-------------------------------------------Apply Page----------------------------------------------- */
    /**
     * The followings are data models
     */
    //--DATA-MODELS for Apply Form

    //Trade List
    $scope.tradeList = [
      "Security guard", "Air Craft Cleaner"
    ];

    //Countries and Cities mappings
    // $scope.countryCityList = [
    //   {
    //     name: "Pakistan",
    //     options: ["TobaTeg Singh", "Rajana", "Gojra", "Pir Mahal", "Faisalabad", "Lahore",  "Sahiwal", "Okara",  "Islamabad", "Karach"]
    //   }, 
    //   {
    //     name: "New Pakistan",
    //     options: ["New TobaTeg Seigh", "New Rajana", "New Gojra", "New Pir Mahal", "New Faisalabad", "New Lahore",  "New Sahiwal", "New Okara",  "New Islamabad", "New Karach"]
    //   }
    // ];
    

    $scope.countryCityList = city.LIST;

    //English Level
    $scope.englishLevel = [
      {"level":"Low","value":1},
      {"level":"Average","value":2},
      {"level":"Good","value":3},
      {"level":"Excelent","value":2}
    ];
    //Qualification List
    $scope.qualifictionList = [
      "Under Matriculation", "Matriculation", "Enter", "Graduation", "Masters", "Mphil","PHD"
    ];

   
  /**@author AbdulRehman
   * Method generates any ints array till the last int is provided
   * @param {lastInt} lastInt Last int till you want to generate
   */
    var generateInts = function(lastInt){
      var intArr = [];
      for(var i = 0; i < lastInt; i++){
        intArr[i] = i;
      }
      return intArr;
    }
     //Integers to be used anywher in the application
    $scope.integers =  generateInts(100);




    //--MODEL-OBJECTS bound with UI
    $scope.date_of_birth;
    $scope.passport_expiry;
    $scope.weight;
    $scope.passport;
    $scope.age;
    $scope.height;
    $scope.phone_num;
    $scope.second_phone_num;
    $scope.english_speaking_level;
    $scope.english_writing_level;
    $scope.social_media_interaction;
    $scope.name;
    $scope.email= "";
    $scope.trade;
    $scope.qualification;
    $scope.city;
    $scope.country;
    $scope.overseas_experience;
    $scope.local_experience;
    $scope.cnic;


    $scope.submitApplyForm = function(isValidated){
      if(!isValidated){
        alert("Please fill mendatory fields");
        return;
      }
      var applyForm = {
        date_of_birth : Date.parse($scope.date_of_birth),
        passport_expiry : Date.parse($scope.passport_expiry),
        weight : $scope.weight,
        passport : $scope.passport,
        age : ((new Date().getTime()-Date.parse($scope.date_of_birth))/(1000*60*60*24*30*12)),
        height : $scope.height,
        phone_num : $scope.phone_num,
        second_phone_num : $scope.second_phone_num,
        english_speaking_level: $scope.english_speaking_level.value,
        english_writing_level : $scope.english_writing_level.value,
        social_media_interaction : $scope.social_media_interaction,
        name : $scope.name,
        email : $scope.email,
        trade : $scope.trade,
        qualification : $scope.qualification,
        city  : $scope.city.city,
        country : $scope.country.name,
        overseas_experience : $scope.overseas_experience,
        local_experience : $scope.local_experience,
        cnic : $scope.cnic
      }
    //   /**
    //  * @author Abdul
    //  * Will submit the form to DB */
    KHMHttpService.submitApply(applyForm, function (response) {
      $scope.loading = 1;      
       if (response.data.errorCode === "200" && response.data.message === "") {
         $scope.info = "Congratulations, We will contact you on Phone Number and email :)";
         $scope.loading = 0;
         $scope.isApplied = true;
       } else {
        $scope.info = "Error during form submission";
        $scope.loading = 0; 
       }
     });


    }
   


    /*-------------------------------------------Contact us Page----------------------------------------------- */
    $scope.loading = 1;
    $scope.team = [];//json array of objects for team info
    //@author Shahid bhy
    //The FOLLOWING MODEL variables are for contact-us
    $scope.name = "";
    $scope.email = "";
    $scope.phone = "";
    $scope.message = "";
    $scope.submitContactForm = function(){
      var contactUsForm = {
        name: $scope.name,
        email: $scope.email,
        phone: $scope.phone,
        message: $scope.message
      }
      /**
     * @author Abdul
     * Here we are getting the data from server side for top team KPIs, currently its dummy 
     * data which will be further refined as we get the orginal data
     */
    httpService.submitContactQuery(contactUsForm, function (response) {
      $scope.loading = 1;      
       if (response.data.status === 200) {
         $scope.info = response.data.message;
         console.dir(response.data)
         $scope.loading = 0;
       } else {
        $scope.info = response.data.message;
         $scope.loading = 0;
 
       }
     });
    }



    var getTeamJSON = function (response) {
      $scope.loading = 1;
       if (response.data.status === 200) {
         $scope.team = response.data.data
         console.dir($scope.team)
         $scope.loading = 0;
       } else {
        $scope.team = response.data.data;
         $scope.loading = 0;
       }
     }
     console.log("getting team");

    //explaning a complete callback lifecycle
    httpService.getPortalTeam(getTeamJSON);

    $scope.slides = [
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49721891_1066602806877662_8627799212660097024_o.jpg?_nc_cat=106&_nc_eui2=AeF9uHsnhT3YzvvNNrANdpwmadc4yvRPkgcVzMsw6VEnIaGbbagnjXNKE0xiEzUZkeItSdg4gupkCVFBcSvUbjfyD5DxaYJCbPC3my_-GyB_ftGKJibIbo5xnY79vmFMfMU&_nc_ht=scontent.fisb3-1.fna&oh=ed2688478e0ae99bf158836914723bf5&oe=5CC4C793',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49261883_1066602836877659_5497608776544419840_o.jpg?_nc_cat=108&_nc_eui2=AeHOdeGfXziJMZbYHOgQAMOyMCV9zHQoPTazwv3kDjwk0lTCpAswJZ6DckHbBkbV3NcYl82F1SCqs_HZkPwKuRfwlDRJ23QzJtsG1KZxGAx1D34xLM5YKu3Ib9dW_lwoXg0&_nc_ht=scontent.fisb3-1.fna&oh=ed18519feac38ab103a82f464a287981&oe=5CC41A88',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49643266_1066602796877663_4658420897604960256_o.jpg?_nc_cat=108&_nc_eui2=AeHN5dvrUJgwaUvNg-sDx_THYMpmsYyBtIUGQy9NZyc6-4COREWdaDyduT7SLmPuw6tA57kJhuBiutbeusjq9kJp1_zZItyv6OlJ_YqSafmawNhe3m4uqx8FMvGwwSzsZDo&_nc_ht=scontent.fisb3-1.fna&oh=86d68389878cd990b0beb0f4efe414ec&oe=5CD5D032',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49346171_1066602946877648_1026316146577506304_o.jpg?_nc_cat=102&_nc_eui2=AeG2EF-CeyLggkwnwwbTHKAziPRxNW7d59iDmi6lF96wVWxEyKQqZpfZropASLgKdnxERQJAz4h1FqbBLTSy4KP-dE6ycw68N-zE7MNFr_1FL3qpUiUz20PI0zU3ZOp2qvw&_nc_ht=scontent.fisb3-1.fna&oh=6a4aebfdedaae3fa28a382a79b6eb6f6&oe=5C8C5C4B',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49442049_1066602986877644_8738617258751819776_o.jpg?_nc_cat=100&_nc_eui2=AeGHw5DFMmjo6aQWVyWzKJyXAF-ulU4djYFUC4aBs1uedKvH-G4F674PUBNmGGvFXjMVRv6eaxRpWlt_4sicg3edvg0oib-EYQ5yK7zeOXDLDH9MBK_tyOfPlPKOSDn6WT4&_nc_ht=scontent.fisb3-1.fna&oh=c69290479b197d9c8434cbd745d3a42b&oe=5CCA12C7',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49678704_1066603026877640_8909086159997501440_o.jpg?_nc_cat=102&_nc_eui2=AeHXj-WUGFjD971B362ngvhPepbEC8dD0zipSD0aKXWtc39MoVAYalBbQPyECsXWPenCWIHSBlcUbdcjSZO1gxfFKtN9uQ0kIuqX_W1_IrVL0tI1YGo2Pr3BruErJFL0xXU&_nc_ht=scontent.fisb3-1.fna&oh=61518a8c25c9df892f9748600f5a1894&oe=5CCB5E1D',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49739218_1066603103544299_4285948994254798848_o.jpg?_nc_cat=102&_nc_eui2=AeFVy-cvOMePH7bD9Vuljn8ZsJSmnAWXKz1NbKAfxNkED7KIpaW34irLbtMdWcShdvnfZG7NyjvYW88OlqAjE4IrmacKZNRdK_jjiEMYKpdq4WuDr0S7oeLcKyYtGXeEj_c&_nc_ht=scontent.fisb3-1.fna&oh=1a7a7ff9abc7caed010972624c4d62ae&oe=5CD5617C',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49704119_1066603213544288_5565334075725578240_o.jpg?_nc_cat=111&_nc_eui2=AeGGFvBVGNsWX__x-SklL6hS7I5T_v_P8BqWVyzJRiA5gdCjauTkhsYZwesMB8vsNOc6xLsLy7vUYPJGt9e4xVktBGtmP8y2yKRpuG8NbFO1uQtFcAg-k2C9TvD6bNwM32E&_nc_ht=scontent.fisb3-1.fna&oh=74ab6e4a1b3ec08a12fb4d36b6c60675&oe=5CC5AA38',
        caption: '' 
      }
    ];


    //Updateing UI controls
    KHMHttpService.getUIControls(function successCallback(response) {
      if (response.data.errorCode === "401") {
        alert("Please refresh the page");
      } else if (response.data.errorCode === "200" && response.data.message === "") {
        var data = response.data.data;
        $scope.tradeList = data.trade;
        $scope.englishLevel = data.speaking_level;
        $scope.integers = data.height;
        $scope.qualifictionList = data.qualification;
        
      } else {
        console.dir(response);
        deferred.resolve(jobs);
      }
    }, function errorCallback(response) {
      console.log(prefix + ".importProjectRest:" + response)
      $scope.loading = 0;
      callBack(false)
    }
    )
    
  }

})();
