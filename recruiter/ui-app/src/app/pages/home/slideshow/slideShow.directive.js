/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.home')
    .directive('slideShow', slideShow);

  /** @ngInject */
  function slideShow() {
    return {

      restrict: 'EA',
      replace: true,
      transclude: true,
      scope: {
        slides: '=',
        animation: '=',
        interval: '='
      },
      controller: 'slideShowCtrl',
      templateUrl: 'app/pages/home/slideshow/slideShow.html'
    


      // restrict: 'E',
      // transclude: true,
      // scope: {
      //   slides: '='
      // },
      // template: `
      //   <div class="slideshow">
      //     <ul class="slideshow-slides">
      //     <li ng-repeat="slide in slides" ng-class="{ active: $index == activeIndex }">
      //       <figure>
      //         <img ng-src="{{ slide.imageUrl}}" />
      //         <figcaption ng-show="slide.caption">{{ slide.caption }}</figcaption>
      //       </figure>
      //     </li>
      //     </ul>
      //     <ul class="slideshow-dots">
      //       <li ng-repeat="slide in slides" ng-class="{ active: $index == activeIndex }">
      //         <a ng-click="jumpToSlide($index)">{{ $index + 1 }}</a>
      //       </li>
      //     </ul>
      //   </div>
      // `,
      // link: function ($scope, element, attrs) {
      //   var timer = null;
      //   $scope.activeIndex = 0;

      //   $scope.jumpToSlide = function (index) {
      //     $scope.activeIndex = index;
      //     restartTimer();
      //   };

      // }
    };
  }
})();