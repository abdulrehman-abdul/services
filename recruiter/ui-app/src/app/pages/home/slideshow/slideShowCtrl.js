/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.home')
      .controller('slideShowCtrl', slideShowCtrl);

  /** @ngInject */
  function slideShowCtrl($scope, $timeout, baConfig, baUtil) {


    $scope.slides = [
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49721891_1066602806877662_8627799212660097024_o.jpg?_nc_cat=106&_nc_eui2=AeF9uHsnhT3YzvvNNrANdpwmadc4yvRPkgcVzMsw6VEnIaGbbagnjXNKE0xiEzUZkeItSdg4gupkCVFBcSvUbjfyD5DxaYJCbPC3my_-GyB_ftGKJibIbo5xnY79vmFMfMU&_nc_ht=scontent.fisb3-1.fna&oh=ed2688478e0ae99bf158836914723bf5&oe=5CC4C793',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49261883_1066602836877659_5497608776544419840_o.jpg?_nc_cat=108&_nc_eui2=AeHOdeGfXziJMZbYHOgQAMOyMCV9zHQoPTazwv3kDjwk0lTCpAswJZ6DckHbBkbV3NcYl82F1SCqs_HZkPwKuRfwlDRJ23QzJtsG1KZxGAx1D34xLM5YKu3Ib9dW_lwoXg0&_nc_ht=scontent.fisb3-1.fna&oh=ed18519feac38ab103a82f464a287981&oe=5CC41A88',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49643266_1066602796877663_4658420897604960256_o.jpg?_nc_cat=108&_nc_eui2=AeHN5dvrUJgwaUvNg-sDx_THYMpmsYyBtIUGQy9NZyc6-4COREWdaDyduT7SLmPuw6tA57kJhuBiutbeusjq9kJp1_zZItyv6OlJ_YqSafmawNhe3m4uqx8FMvGwwSzsZDo&_nc_ht=scontent.fisb3-1.fna&oh=86d68389878cd990b0beb0f4efe414ec&oe=5CD5D032',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49346171_1066602946877648_1026316146577506304_o.jpg?_nc_cat=102&_nc_eui2=AeG2EF-CeyLggkwnwwbTHKAziPRxNW7d59iDmi6lF96wVWxEyKQqZpfZropASLgKdnxERQJAz4h1FqbBLTSy4KP-dE6ycw68N-zE7MNFr_1FL3qpUiUz20PI0zU3ZOp2qvw&_nc_ht=scontent.fisb3-1.fna&oh=6a4aebfdedaae3fa28a382a79b6eb6f6&oe=5C8C5C4B',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49442049_1066602986877644_8738617258751819776_o.jpg?_nc_cat=100&_nc_eui2=AeGHw5DFMmjo6aQWVyWzKJyXAF-ulU4djYFUC4aBs1uedKvH-G4F674PUBNmGGvFXjMVRv6eaxRpWlt_4sicg3edvg0oib-EYQ5yK7zeOXDLDH9MBK_tyOfPlPKOSDn6WT4&_nc_ht=scontent.fisb3-1.fna&oh=c69290479b197d9c8434cbd745d3a42b&oe=5CCA12C7',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49678704_1066603026877640_8909086159997501440_o.jpg?_nc_cat=102&_nc_eui2=AeHXj-WUGFjD971B362ngvhPepbEC8dD0zipSD0aKXWtc39MoVAYalBbQPyECsXWPenCWIHSBlcUbdcjSZO1gxfFKtN9uQ0kIuqX_W1_IrVL0tI1YGo2Pr3BruErJFL0xXU&_nc_ht=scontent.fisb3-1.fna&oh=61518a8c25c9df892f9748600f5a1894&oe=5CCB5E1D',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49739218_1066603103544299_4285948994254798848_o.jpg?_nc_cat=102&_nc_eui2=AeFVy-cvOMePH7bD9Vuljn8ZsJSmnAWXKz1NbKAfxNkED7KIpaW34irLbtMdWcShdvnfZG7NyjvYW88OlqAjE4IrmacKZNRdK_jjiEMYKpdq4WuDr0S7oeLcKyYtGXeEj_c&_nc_ht=scontent.fisb3-1.fna&oh=1a7a7ff9abc7caed010972624c4d62ae&oe=5CD5617C',
        caption: '' 
      },
      { src: 'https://scontent.fisb3-1.fna.fbcdn.net/v/t1.0-9/49704119_1066603213544288_5565334075725578240_o.jpg?_nc_cat=111&_nc_eui2=AeGGFvBVGNsWX__x-SklL6hS7I5T_v_P8BqWVyzJRiA5gdCjauTkhsYZwesMB8vsNOc6xLsLy7vUYPJGt9e4xVktBGtmP8y2yKRpuG8NbFO1uQtFcAg-k2C9TvD6bNwM32E&_nc_ht=scontent.fisb3-1.fna&oh=74ab6e4a1b3ec08a12fb4d36b6c60675&oe=5CC5AA38',
        caption: '' 
      }
    ];



    
    var settings = {
      animation: $scope.animation || 'animate-fade',
      interval: $scope.interval || 3000
    };

    $scope.activeIndex = 0;

    $scope.setActiveIndex = function(index) {

      $scope.isPrev = index < $scope.activeIndex;

      $scope.activeIndex = index;
    };

    $scope.setAnimation = function(animation) {

      return animation || settings.animation;
    };

    $scope.isActiveIndex = function(index) {

      return $scope.activeIndex === index;
    };

    $scope.next = function() {

      $scope.isPrev = false;

      $scope.activeIndex = $scope.activeIndex < $scope.slides.length - 1 ? $scope.activeIndex + 1 : 0;
    };

    $scope.prev = function() {

      $scope.isPrev = true;

      $scope.activeIndex = $scope.activeIndex > 0 ? $scope.activeIndex - 1 : $scope.slides.length - 1;
    };

    if (settings.interval) {

      var interval;

      $scope.play = function() {

        interval = $timeout(function() {

           $scope.next();
        }, settings.interval);
      };

      $scope.pause = function() {

        $timeout.cancel(interval);
      };

      $scope.$watch('activeIndex', function() {

        $scope.pause();
        $scope.play();
      });
    }
  

    

    // $timeout(function () {
    //   loadPieCharts();
    //   updatePieCharts();
    // }, 1000);
  }
})();