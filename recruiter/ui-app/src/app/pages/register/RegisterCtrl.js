/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.register')
    .controller('RegisterCtrl', RegisterCtrl);

  /** @ngInject */
  function RegisterCtrl($scope, fileReader, $filter, $uibModal,$http,config,util, KHMHttpService) {
    $scope.name = "";
    $scope.email = "";
    $scope.password = "";
    $scope.registered = false;

    $scope.validateMessage = "";
    $scope.succesfulRegistered = "";

/** @unused
 * Returns true if valid credentials else false
 * */
    var validateCredentials = function(){
      $scope.validateMessage = "Please enter valid credeatials";
      if($scope.email === "" || $scope.password === "" || $scope.password === ""){
        return false;
      }else{
        if(util.isValidEmail($scope.email) === true && util.specialChar($scope.password) === false&& util.specialChar($scope.name) === false){
          $scope.validateMessage = "";
          return true;
        }else{
          return false;
        }
      }
      
    }

    $scope.signup = function () {
      //returnning because we are disabling regisration
      return;
      KHMHttpService.register( $scope.name, $scope.email,$scope.password,function successCallback(response) {
        console.log(response)
          if(response.data.errorCode === "200"){
            $scope.registered = true;
            $scope.succesfulRegistered = "Successfully registered!";
          }else if(response.data.status === 202 && response.data.message === "EXIST"){
            $scope.registered = true;
            $scope.succesfulRegistered = "Already registered, please login!";
          }else{
            $scope.validateMessage = "Please enter valid credeatials";
          }
        }, function errorCallback(response) {
          $scope.registered = false;
          $scope.validateMessage = "Please enter valid credeatials";
      })
      
    };



    
  }

})();
