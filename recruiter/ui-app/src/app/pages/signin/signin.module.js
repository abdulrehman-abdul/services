/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.signin', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('signin', {
          url: '/signin',
          title: 'SignIn',
          templateUrl: 'app/pages/signin/signin.html',
          controller: 'SignInCtrl',
          // sidebarMeta: {
          //   icon: 'ion-ios-location-outline',
          //   order: 600,
          // }
        }).state('signout', {
          url: '/signout',
          title: 'Signout',
          templateUrl: 'app/pages/signin/signout.html',
          controller: 'SignInCtrl'
        });
  }

})();
