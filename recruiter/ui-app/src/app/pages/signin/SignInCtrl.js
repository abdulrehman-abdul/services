/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.signin')
    .controller('SignInCtrl', SignInCtrl);

  /** @ngInject */
  function SignInCtrl($rootScope, $scope, fileReader, $filter, $uibModal, $http,$state,authorizeService,util, KHMHttpService) {
    $scope.email = "";
    $scope.password = "";
    $scope.validateMessage = "";

/**@unused 
 * Returns true if valid credentials else false
 * */
    var validateCredentials = function(){
      $scope.validateMessage = "Please enter valid credeatials";
      if($scope.email === "" || $scope.password === ""){
        return false;
      }else{
        if(util.isValidEmail($scope.email) === true && util.specialChar($scope.password) === false){
          $scope.validateMessage = "";
          return true;
        }else{
          return false;
        }
      }    
    }

    $scope.login = function () {
      console.log("SignControler");
      KHMHttpService.login( $scope.email,$scope.password,function successCallback(response) {
        if(response.data.errorCode === "200"){
          authorizeService.storeToken($scope.email,response.data.data.accessToken,response.data.data.refreshToken);
          //  $state.go('dashboard');
          $state.go('dashboard');
        }else{
          $scope.validateMessage = "Please enter valid credeatials";
        }
      }, function errorCallback(response) {
          $scope.validateMessage = "Please enter valid credeatials";
      })
      
    };
      

     $scope.signout = function () {
      authorizeService.removeToken();
      $rootScope.isLoged = false;
      $state.go('home');
      
    };
  }

})();
