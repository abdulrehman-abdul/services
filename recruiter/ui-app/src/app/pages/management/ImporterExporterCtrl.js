/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.importer')
    .controller('ImporterExporterCtrl', ImporterExporterCtrl);

  /** @ngInject */
  function ImporterExporterCtrl($scope, $window, $filter, $state, $http, editableOptions, editableThemes, authorizeService, importerAnalyzerService) {
    var importData = [];
        var prefix = "ImporterExporterCtrl";
    // $scope.projects = [];
    $scope.projects = dummyprojects;
    $scope.accounts = [];
    $scope.loading = 0;//for loader
    $scope.spinneractive = false;
    $scope.smartTablePageSize = 10;

    if (importData !== null || typeof importData !== "undefined") {
      $scope.accounts = importData.accounts;
      // $scope.projects = importData.projects;
    }
    /**
     * Starts the loader
    */
    var startSpin = function () {
      if (!$scope.spinneractive) {
        usSpinnerService.spin('spinner-1');
        $scope.startcounter++;
      }
    };
    /** Stops the loader*/
    var stopSpin = function () {
      if ($scope.spinneractive) {
        usSpinnerService.stop('spinner-1');
      }
    };


    //Accounts table functions
    /**Validates the account*/
    $scope.validateAccount = function (index) {
      var account = $scope.accounts[index];
      if (account) {
        if (account.validated === true) {
          if (confirm("Account is already validated!") == true) {
            try {
              account.action = "Validate";
              account.activate = true;
              callValidateRest(account, function (resp) { })
            } catch (error) {
              console.log(prefix + ".getAccounts:" + error)
            }
          }
        } else {
          callValidateRest(account, function (resp) { })
        }
      }
    };

    /**Invokes REST call for account validation and Returns the true or false in call back response.  */
    var callValidateRest = function (account, callBack) {
      $scope.loading = 1;
      try {
        var req = {
          method: 'POST',
          url: 'http://localhost:8080/api/data/account/validate',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': authorizeService.getToken(),
            'clientId': authorizeService.getClientID()
          },
          data: { ppmaccount: account }
        }
        $http(req).then(function successCallback(response) {
          if (response.data.status === 200 && response.data.error === false) {
            $scope.loading = 0;
            account.activate = true;
            account.validated = true;
            account.action = "Validate";
            importerAnalyzerService.getProjectByAccount(account, function (projects) {
              $scope.projects = projects;
              callBack(true)
            })
          } else if (response.data.status === 202 && response.data.message === "UN_AUTHORIZE") {
            $scope.loading = 0;
            $state.go('signin');
          }
        }, function errorCallback(response) {
          $scope.loading = 0;
          if (response.status == 403) {
            $state.go('signin');
          }
          alert("Not validated, provide the correct information");


        });
      } catch (error) {
        $scope.loading = 0;
        throw error;
      }
    }



    // $scope.addProject = function() {
    //   $scope.inserted = {
    //     id: $scope.projects.length+1,
    //     name: '',
    //     key: null,
    //     description: null
    //   };
    //   $scope.projects.push($scope.inserted);
    // };
    $scope.updateProject = function (project) {
      console.log(project)
    }
    /**
     * Imports the project
     * */
    $scope.importProject = function (index) {
      var project = $scope.projects[index];
      importProjectRest(project, function (resp) { })
      // console.log(project)
      // if(project){
      //   project.import.status = true;
      //   project.import.action = "Re-Import";
      // }
    };
    $scope.analyseProject = function (index) {
      var project = $scope.projects[index];
      if (project) {
        project.analyse.status = true;
        project.analyse.action = "Re-Analyse";
      }
    };

    //accounts 
    $scope.addAccount = function () {
      $scope.inserted_account = {
        id: $scope.accounts.length + 1,
        name: '',
        password: null,
        domain: null,
        validated: false,
        action: "Validate"
      };
      $scope.accounts.push($scope.inserted_account);
    };


    $scope.cancelAccount = function (user) {
      // console.log("---------------user")
      // console.dir(user)
      // console.dir($scope.accounts)

      var latest = $scope.accounts.splice(user.id - 1, 1);
      console.dir($scope.accounts)
      console.dir(latest)
    };


    $scope.updateAccount = function (project) {
      console.log(project)
    }


    /**Deletes the account and  */
    $scope.deleteAccount = function (index) {
      var account = $scope.accounts[index];
      console.log(account)
      console.log($scope.accounts)
      if (account) {
        if (account.validated === true && account.name && account.password && account.domain) {
          if (confirm("Do you really want to delete the Account :(!") == true) {
            try {
              callDeleteAccountRest(account, function (resp) {
                if (resp == true) {
                  $scope.accounts.splice(index, 1);
                } else {
                  var latest = $scope.accounts.splice(index, 1);
                }
              })
            } catch (error) {
              console.log(prefix + ".getAccounts:" + error)
            }
          }
        } else {
          // callDeleteAccountRest(account,function(resp){
          //   if(resp == true){
          //          $scope.accounts.splice(index,1);
          //     }
          // })
          $scope.accounts.splice(index, 1);
        }

      }
    };

    /**gets the projects assigned to this account  */
    $scope.getAccountProjects = function (index) {
      var account = $scope.accounts[index];
      if (account) {
        $scope.loading = 1;
        if (account.validated === true) {
          try {
            importerAnalyzerService.getProjectByAccount(account, function (projects) {
              $scope.projects = projects;
              $scope.loading = 0;
            })
          } catch (error) {
            console.log(prefix + ".getAccounts:" + error)
            $scope.loading = 0;
          }
        } else {
          alert("Please validate the project first :)");
          $scope.loading = 0;
        }

      }
    };
    /**Invokes REST call for account validation and Returns the true or false in call back response.  */
    var callDeleteAccountRest = function (account, callBack) {
      $scope.loading = 1;
      try {
        var req = {
          method: 'POST',
          url: 'http://localhost:8080/api/data/account/delete',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': authorizeService.getToken(),
            'clientId': authorizeService.getClientID()
          },
          data: { ppmaccount: account }
        }
        $http(req).then(function successCallback(response) {
          if (response.data.status === 200 && response.data.error === "false") {
            $scope.loading = 0;
            account.validated = true;
            account.action = "Validate";
            importerAnalyzerService.getProjects(function (projects) {
              $scope.projects = projects;
              callBack(true)
            })
          } else {
            $scope.loading = 0;
            callBack(true)
          }
        }, function errorCallback(response) {
          $scope.loading = 0;
          callBack(false)
        });

      } catch (error) {
        $scope.loading = 0;
        throw error;
      }
    }



    /**Invokes REST call for account validation and Returns the true or false in call back response.  */
    var importProjectRest = function (project, callBack) {
      try {
        $scope.loading = 1;
        var req = {
          method: 'POST',
          url: 'http://localhost:8080/api/data/import',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': authorizeService.getToken(),
            'clientId': authorizeService.getClientID()
          },
          data: { project_id: project.id }
        }
        $http(req).then(function successCallback(response) {
          if (response.data.status === 200 && response.data.error === false) {
            $scope.loading = 0;
            callBack(true)
            project.import.status = true;
          } else if (response.data.status === 202 || response.data.status === 403 || response.data.message === "UN_AUTHORIZE") {
            $scope.loading = 0;
            $state.go('signin');
          }
        }, function errorCallback(response) {
          console.log(prefix + ".importProjectRest:" + response)
          $scope.loading = 0;
          callBack(false)
        });

      } catch (error) {
        throw error;
      }
    }

    //--SEARCH---recruiter
    var server_address = 'http://localhost:8080/recruiter';
     //Trade List
     $scope.tradeList = [
      "Security guard", "Air Craft Cleaner"
    ];

    //English Level
    //English Level
    $scope.englishLevel = [
      {"level":"Low","value":1},
      {"level":"Average","value":2},
      {"level":"Good","value":3},
      {"level":"Excelent","value":2}
    ];

    //Qualification List
    $scope.qualifictionList = [
      "Under Matriculation", "Matriculation", "Enter", "Graduation", "Masters", "Mphil","PHD"
    ];
   
  /**@author AbdulRehman
   * Method generates any ints array till the last int is provided
   * @param {lastInt} lastInt Last int till you want to generate
   */
    var generateInts = function(lastInt){
      var intArr = [];
      for(var i = 0; i < lastInt; i++){
        intArr[i] = i;
      }
      return intArr;
    }
     //Integers to be used anywher in the application
    $scope.integers =  generateInts(100);


    $scope.weight;
    $scope.age;
    $scope.height;
    $scope.phone_num;
    $scope.cnic;
    $scope.english_speaking_level;
    $scope.english_writing_level;
    $scope.trade;
    $scope.qualification;

    $scope.searcObj = {
      age : 0,
      height : 0,
      phone_num :0,
      cnic : 0,
      english_speaking_level: 0,
      english_writing_level : 0,
      trade : "",
      qualification : "",
    }

    console.log("--in -> importerExporter Controller")

    $scope.searchUser = function(searcObj,isResultSearch){

      //checking is this for user-result search inputs
      if(isResultSearch== true && (!searcObj || searcObj.phone_num === null || searcObj.phone_num < 1 )
        && (searcObj.cnic === null || searcObj.cnic < 1)){
        alert("Please enter value");
        return;
      }


      
      console.log("--logging -> searcObj")
      console.dir(searcObj)
      /**
     * @author Abdul
     * Will submit the form to DB */
    var req = {
      method: 'POST',
      url: server_address+'/api/bio/search',
      headers: {
        'Content-Type': 'application/json',
        'token': authorizeService.getToken(),
        'client_id': authorizeService.getClientID()
      },
      data: {
        age : searcObj.age,
        height : searcObj.height,
        phone_num :searcObj.phone_num,
        cnic : searcObj.cnic,
        english_speaking_level: searcObj.english_speaking_level?searcObj.english_speaking_level.value:0,
        english_writing_level : searcObj.english_writing_level?searcObj.english_writing_level.value:0,
        trade : searcObj.trade,
        qualification : searcObj.qualification        
      }
    }
    $http(req).then(function successCallback(response) {
      if (response.data.errorCode === "200" && response.data.message === "") {
        $scope.loading = 0;
        $scope.searchTable = response.data.data;
      } else{
        console.dir(response);
      }
    }, function errorCallback(response) {
      console.log(prefix + ".importProjectRest:" + response)
      $scope.loading = 0;
      callBack(false)
    });
    }


    $scope.downloadSearch = function(searchObj){
      var url = server_address+"/api/bio/search2"+prepareSearchParams(searchObj,"XLSX")+prepareAuthParams;
      window.location.href=url;
    
    }

    $scope.downloadOverall = function(){
      window.location.href=server_address+'/api/bio/get/all?responseType=XLSX';
    }

    var prepareSearchParams = function(searchObj,responseTypej){
      var writing = searchObj.english_writing_level?searchObj.english_writing_level.value:0;
      var speaking = searchObj.english_speaking_level?searchObj.english_speaking_level.value:0;
      var uri = "?trade="+searchObj.trade+"&age="+searchObj.age+"&height="+searchObj.height+"&qualification="+searchObj.qualification+"&english_speaking_level="+speaking+
      "&english_writing_level="+writing+"&phone_num="+searchObj.phone_num+"&cnic="+searchObj.cnic+"&responseType="+responseTypej;
      return uri;
    }

    var prepareAuthParams = function(){
      var uri = "?token="+authorizeService.getToken()+"&client_id="+searchObj.authorizeService.getClientID();
      return uri;
    }


    


    
 

    $scope.searchTable = [];
    $scope.searchTable_1 = [
      {
        id: 1,
        firstName: 'Mark',
        lastName: 'Otto',
        username: '@mdo',
        email: 'mdo@gmail.com',
        age: '28'
      },
      {
        "date_of_birth" :123,
        "passport_expiry" :123,
        "name" : "Saim Rashid",
        "age" : 30,
        "height" : 6,
        "email" : "abdulrehman@gmail.com",
        "trade" : "eng",
        "qualification" : " MIT",
        "weight" : 140,
        "city" : " isd",
        "country" : " pk",
        "passport" : 77777,
        "overseas_experience" : " overseas",
        "local_experience" : " local",
        "phone_num" : 30356,
        "second_phone_num" : 3035659264,
        "english_speaking_level" : 2,
        "english_writing_level" : 1,
        "english_listening_level" : 3,
        "social_media_interaction" : 1,
        "cnic" : 1
      }]


    $scope.smartTableData = [
      {
        id: 1,
        firstName: 'Mark',
        lastName: 'Otto',
        username: '@mdo',
        email: 'mdo@gmail.com',
        age: '28'
      },
      {
        id: 2,
        firstName: 'Jacob',
        lastName: 'Thornton',
        username: '@fat',
        email: 'fat@yandex.ru',
        age: '45'
      },
      {
        id: 3,
        firstName: 'Larry',
        lastName: 'Bird',
        username: '@twitter',
        email: 'twitter@outlook.com',
        age: '18'
      },
      {
        id: 4,
        firstName: 'John',
        lastName: 'Snow',
        username: '@snow',
        email: 'snow@gmail.com',
        age: '20'
      },
      {
        id: 5,
        firstName: 'Jack',
        lastName: 'Sparrow',
        username: '@jack',
        email: 'jack@yandex.ru',
        age: '30'
      },
      {
        id: 6,
        firstName: 'Ann',
        lastName: 'Smith',
        username: '@ann',
        email: 'ann@gmail.com',
        age: '21'
      },
      {
        id: 7,
        firstName: 'Barbara',
        lastName: 'Black',
        username: '@barbara',
        email: 'barbara@yandex.ru',
        age: '43'
      },
      {
        id: 8,
        firstName: 'Sevan',
        lastName: 'Bagrat',
        username: '@sevan',
        email: 'sevan@outlook.com',
        age: '13'
      },
      {
        id: 9,
        firstName: 'Ruben',
        lastName: 'Vardan',
        username: '@ruben',
        email: 'ruben@gmail.com',
        age: '22'
      },
      {
        id: 10,
        firstName: 'Karen',
        lastName: 'Sevan',
        username: '@karen',
        email: 'karen@yandex.ru',
        age: '33'
      },
      {
        id: 11,
        firstName: 'Mark',
        lastName: 'Otto',
        username: '@mark',
        email: 'mark@gmail.com',
        age: '38'
      },
      {
        id: 12,
        firstName: 'Jacob',
        lastName: 'Thornton',
        username: '@jacob',
        email: 'jacob@yandex.ru',
        age: '48'
      },
      {
        id: 13,
        firstName: 'Haik',
        lastName: 'Hakob',
        username: '@haik',
        email: 'haik@outlook.com',
        age: '48'
      },
      {
        id: 14,
        firstName: 'Garegin',
        lastName: 'Jirair',
        username: '@garegin',
        email: 'garegin@gmail.com',
        age: '40'
      },
      {
        id: 15,
        firstName: 'Krikor',
        lastName: 'Bedros',
        username: '@krikor',
        email: 'krikor@yandex.ru',
        age: '32'
      },
      {
        "id": 16,
        "firstName": "Francisca",
        "lastName": "Brady",
        "username": "@Gibson",
        "email": "franciscagibson@comtours.com",
        "age": 11
      },
      {
        "id": 17,
        "firstName": "Tillman",
        "lastName": "Figueroa",
        "username": "@Snow",
        "email": "tillmansnow@comtours.com",
        "age": 34
      },
      {
        "id": 18,
        "firstName": "Jimenez",
        "lastName": "Morris",
        "username": "@Bryant",
        "email": "jimenezbryant@comtours.com",
        "age": 45
      },
      {
        "id": 19,
        "firstName": "Sandoval",
        "lastName": "Jacobson",
        "username": "@Mcbride",
        "email": "sandovalmcbride@comtours.com",
        "age": 32
      },
      {
        "id": 20,
        "firstName": "Griffin",
        "lastName": "Torres",
        "username": "@Charles",
        "email": "griffincharles@comtours.com",
        "age": 19
      },
      {
        "id": 21,
        "firstName": "Cora",
        "lastName": "Parker",
        "username": "@Caldwell",
        "email": "coracaldwell@comtours.com",
        "age": 27
      },
      {
        "id": 22,
        "firstName": "Cindy",
        "lastName": "Bond",
        "username": "@Velez",
        "email": "cindyvelez@comtours.com",
        "age": 24
      },
      {
        "id": 23,
        "firstName": "Frieda",
        "lastName": "Tyson",
        "username": "@Craig",
        "email": "friedacraig@comtours.com",
        "age": 45
      },
      {
        "id": 24,
        "firstName": "Cote",
        "lastName": "Holcomb",
        "username": "@Rowe",
        "email": "coterowe@comtours.com",
        "age": 20
      },
      {
        "id": 25,
        "firstName": "Trujillo",
        "lastName": "Mejia",
        "username": "@Valenzuela",
        "email": "trujillovalenzuela@comtours.com",
        "age": 16
      },
      {
        "id": 26,
        "firstName": "Pruitt",
        "lastName": "Shepard",
        "username": "@Sloan",
        "email": "pruittsloan@comtours.com",
        "age": 44
      },
      {
        "id": 27,
        "firstName": "Sutton",
        "lastName": "Ortega",
        "username": "@Black",
        "email": "suttonblack@comtours.com",
        "age": 42
      },
      {
        "id": 28,
        "firstName": "Marion",
        "lastName": "Heath",
        "username": "@Espinoza",
        "email": "marionespinoza@comtours.com",
        "age": 47
      },
      {
        "id": 29,
        "firstName": "Newman",
        "lastName": "Hicks",
        "username": "@Keith",
        "email": "newmankeith@comtours.com",
        "age": 15
      },
      {
        "id": 30,
        "firstName": "Boyle",
        "lastName": "Larson",
        "username": "@Summers",
        "email": "boylesummers@comtours.com",
        "age": 32
      },
      {
        "id": 31,
        "firstName": "Haynes",
        "lastName": "Vinson",
        "username": "@Mckenzie",
        "email": "haynesmckenzie@comtours.com",
        "age": 15
      },
      {
        "id": 32,
        "firstName": "Miller",
        "lastName": "Acosta",
        "username": "@Young",
        "email": "milleryoung@comtours.com",
        "age": 55
      },
      {
        "id": 33,
        "firstName": "Johnston",
        "lastName": "Brown",
        "username": "@Knight",
        "email": "johnstonknight@comtours.com",
        "age": 29
      },
      {
        "id": 34,
        "firstName": "Lena",
        "lastName": "Pitts",
        "username": "@Forbes",
        "email": "lenaforbes@comtours.com",
        "age": 25
      },
      {
        "id": 35,
        "firstName": "Terrie",
        "lastName": "Kennedy",
        "username": "@Branch",
        "email": "terriebranch@comtours.com",
        "age": 37
      },
      {
        "id": 36,
        "firstName": "Louise",
        "lastName": "Aguirre",
        "username": "@Kirby",
        "email": "louisekirby@comtours.com",
        "age": 44
      },
      {
        "id": 37,
        "firstName": "David",
        "lastName": "Patton",
        "username": "@Sanders",
        "email": "davidsanders@comtours.com",
        "age": 26
      },
      {
        "id": 38,
        "firstName": "Holden",
        "lastName": "Barlow",
        "username": "@Mckinney",
        "email": "holdenmckinney@comtours.com",
        "age": 11
      },
      {
        "id": 39,
        "firstName": "Baker",
        "lastName": "Rivera",
        "username": "@Montoya",
        "email": "bakermontoya@comtours.com",
        "age": 47
      },
      {
        "id": 40,
        "firstName": "Belinda",
        "lastName": "Lloyd",
        "username": "@Calderon",
        "email": "belindacalderon@comtours.com",
        "age": 21
      },
      {
        "id": 41,
        "firstName": "Pearson",
        "lastName": "Patrick",
        "username": "@Clements",
        "email": "pearsonclements@comtours.com",
        "age": 42
      },
      {
        "id": 42,
        "firstName": "Alyce",
        "lastName": "Mckee",
        "username": "@Daugherty",
        "email": "alycedaugherty@comtours.com",
        "age": 55
      },
      {
        "id": 43,
        "firstName": "Valencia",
        "lastName": "Spence",
        "username": "@Olsen",
        "email": "valenciaolsen@comtours.com",
        "age": 20
      },
      {
        "id": 44,
        "firstName": "Leach",
        "lastName": "Holcomb",
        "username": "@Humphrey",
        "email": "leachhumphrey@comtours.com",
        "age": 28
      },
      {
        "id": 45,
        "firstName": "Moss",
        "lastName": "Baxter",
        "username": "@Fitzpatrick",
        "email": "mossfitzpatrick@comtours.com",
        "age": 51
      },
      {
        "id": 46,
        "firstName": "Jeanne",
        "lastName": "Cooke",
        "username": "@Ward",
        "email": "jeanneward@comtours.com",
        "age": 59
      },
      {
        "id": 47,
        "firstName": "Wilma",
        "lastName": "Briggs",
        "username": "@Kidd",
        "email": "wilmakidd@comtours.com",
        "age": 53
      },
      {
        "id": 48,
        "firstName": "Beatrice",
        "lastName": "Perry",
        "username": "@Gilbert",
        "email": "beatricegilbert@comtours.com",
        "age": 39
      },
      {
        "id": 49,
        "firstName": "Whitaker",
        "lastName": "Hyde",
        "username": "@Mcdonald",
        "email": "whitakermcdonald@comtours.com",
        "age": 35
      },
      {
        "id": 50,
        "firstName": "Rebekah",
        "lastName": "Duran",
        "username": "@Gross",
        "email": "rebekahgross@comtours.com",
        "age": 40
      },
      {
        "id": 51,
        "firstName": "Earline",
        "lastName": "Mayer",
        "username": "@Woodward",
        "email": "earlinewoodward@comtours.com",
        "age": 52
      },
      {
        "id": 52,
        "firstName": "Moran",
        "lastName": "Baxter",
        "username": "@Johns",
        "email": "moranjohns@comtours.com",
        "age": 20
      },
      {
        "id": 53,
        "firstName": "Nanette",
        "lastName": "Hubbard",
        "username": "@Cooke",
        "email": "nanettecooke@comtours.com",
        "age": 55
      },
      {
        "id": 54,
        "firstName": "Dalton",
        "lastName": "Walker",
        "username": "@Hendricks",
        "email": "daltonhendricks@comtours.com",
        "age": 25
      },
      {
        "id": 55,
        "firstName": "Bennett",
        "lastName": "Blake",
        "username": "@Pena",
        "email": "bennettpena@comtours.com",
        "age": 13
      },
      {
        "id": 56,
        "firstName": "Kellie",
        "lastName": "Horton",
        "username": "@Weiss",
        "email": "kellieweiss@comtours.com",
        "age": 48
      },
      {
        "id": 57,
        "firstName": "Hobbs",
        "lastName": "Talley",
        "username": "@Sanford",
        "email": "hobbssanford@comtours.com",
        "age": 28
      },
      {
        "id": 58,
        "firstName": "Mcguire",
        "lastName": "Donaldson",
        "username": "@Roman",
        "email": "mcguireroman@comtours.com",
        "age": 38
      },
      {
        "id": 59,
        "firstName": "Rodriquez",
        "lastName": "Saunders",
        "username": "@Harper",
        "email": "rodriquezharper@comtours.com",
        "age": 20
      },
      {
        "id": 60,
        "firstName": "Lou",
        "lastName": "Conner",
        "username": "@Sanchez",
        "email": "lousanchez@comtours.com",
        "age": 16
      }
    ];

    $scope.editableTableData = $scope.smartTableData.slice(0, 36);

    $scope.peopleTableData = [
      {
        id: 1,
        firstName: 'Mark',
        lastName: 'Otto',
        username: '@mdo',
        email: 'mdo@gmail.com',
        age: '28',
        status: 'info'
      },
      {
        id: 2,
        firstName: 'Jacob',
        lastName: 'Thornton',
        username: '@fat',
        email: 'fat@yandex.ru',
        age: '45',
        status: 'primary'
      },
      {
        id: 3,
        firstName: 'Larry',
        lastName: 'Bird',
        username: '@twitter',
        email: 'twitter@outlook.com',
        age: '18',
        status: 'success'
      },
      {
        id: 4,
        firstName: 'John',
        lastName: 'Snow',
        username: '@snow',
        email: 'snow@gmail.com',
        age: '20',
        status: 'danger'
      },
      {
        id: 5,
        firstName: 'Jack',
        lastName: 'Sparrow',
        username: '@jack',
        email: 'jack@yandex.ru',
        age: '30',
        status: 'warning'
      }
    ];

    $scope.metricsTableData = [
      {
        image: 'app/browsers/chrome.svg',
        browser: 'Google Chrome',
        visits: '10,392',
        isVisitsUp: true,
        purchases: '4,214',
        isPurchasesUp: true,
        percent: '45%',
        isPercentUp: true
      },
      {
        image: 'app/browsers/firefox.svg',
        browser: 'Mozilla Firefox',
        visits: '7,873',
        isVisitsUp: true,
        purchases: '3,031',
        isPurchasesUp: false,
        percent: '28%',
        isPercentUp: true
      },
      {
        image: 'app/browsers/ie.svg',
        browser: 'Internet Explorer',
        visits: '5,890',
        isVisitsUp: false,
        purchases: '2,102',
        isPurchasesUp: false,
        percent: '17%',
        isPercentUp: false
      },
      {
        image: 'app/browsers/safari.svg',
        browser: 'Safari',
        visits: '4,001',
        isVisitsUp: false,
        purchases: '1,001',
        isPurchasesUp: false,
        percent: '14%',
        isPercentUp: true
      },
      {
        image: 'app/browsers/opera.svg',
        browser: 'Opera',
        visits: '1,833',
        isVisitsUp: true,
        purchases: '83',
        isPurchasesUp: true,
        percent: '5%',
        isPercentUp: false
      }
    ];

    $scope.users = [
      {
        "id": 1,
        "name": "Esther Vang",
        "status": 4,
        "group": 3
      },
      {
        "id": 2,
        "name": "Leah Freeman",
        "status": 3,
        "group": 1
      },
      {
        "id": 3,
        "name": "Mathews Simpson",
        "status": 3,
        "group": 2
      },
      {
        "id": 4,
        "name": "Buckley Hopkins",
        "group": 4
      },
      {
        "id": 5,
        "name": "Buckley Schwartz",
        "status": 1,
        "group": 1
      },
      {
        "id": 6,
        "name": "Mathews Hopkins",
        "status": 4,
        "group": 2
      },
      {
        "id": 7,
        "name": "Leah Vang",
        "status": 4,
        "group": 1
      },
      {
        "id": 8,
        "name": "Vang Schwartz",
        "status": 4,
        "group": 2
      },
      {
        "id": 9,
        "name": "Hopkin Esther",
        "status": 1,
        "group": 2
      },
      {
        "id": 10,
        "name": "Mathews Schwartz",
        "status": 1,
        "group": 3
      }
    ];

    $scope.statuses = [
      { value: 1, text: 'Good' },
      { value: 2, text: 'Awesome' },
      { value: 3, text: 'Excellent' },
    ];

    $scope.groups = [
      { id: 1, text: 'user' },
      { id: 2, text: 'customer' },
      { id: 3, text: 'vip' },
      { id: 4, text: 'admin' }
    ];

    $scope.showGroup = function (user) {
      if (user.group && $scope.groups.length) {
        var selected = $filter('filter')($scope.groups, { id: user.group });
        return selected.length ? selected[0].text : 'Not set';
      } else return 'Not set'
    };

    $scope.showStatus = function (user) {
      var selected = [];
      if (user.status) {
        selected = $filter('filter')($scope.statuses, { value: user.status });
      }
      return selected.length ? selected[0].text : 'Not set';
    };


    $scope.removeUser = function (index) {
      $scope.users.splice(index, 1);
    };

    $scope.addUser = function () {
      $scope.inserted = {
        id: $scope.users.length + 1,
        name: '',
        status: null,
        group: null
      };
      $scope.users.push($scope.inserted);
    };

    editableOptions.theme = 'bs3';
    editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
    editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';






    //Dummy data
    var dummyprojects = [
      {
        "id": 1003,
        "name": "Project1",
        "key": "P1",
        "description": "This is projectt one",
        "domain": "www.jira.com",
        "import": {
          "status": false,
          "action": "Import"
        },
        "analyse": {
          "status": false,
          "action": "Analyse"
        }
      },
      {
        "id": 2002,
        "name": "Leah Freeman",
        "key": "LF",
        "description": "This is second project",
        "domain": "www.assembla.com",
        "import": {
          "status": false,
          "action": "Import"
        },
        "analyse": {
          "status": false,
          "action": "Analyse"
        }
      }
    ];
    var dummyAccounts = [{
      "id": 1003,
      "name": "abdul@gmail.com",
      "password": "P1",
      "domain": "www.jira.com",
      "validated": false,
      "action": "Validate"
    },
    {
      "id": 1002,
      "name": "saeed@gmail.com",
      "password": "P1",
      "domain": "www.assembla.com",
      "validated": false,
      "action": "Validate"
    }
    ];
  }




})();
