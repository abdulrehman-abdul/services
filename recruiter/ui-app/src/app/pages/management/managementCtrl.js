/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.importer')
    .controller('ManagementCtrl', ManagementCtrl);

  /** @ngInject */
  function ManagementCtrl($scope, $window, $filter, $state, $http, editableOptions, editableThemes, authorizeService, KHMHttpService, jobs) {


     














    var prefix = "ManagementCtrl";
    $scope.loading = 0;//for loader

    /**
     * Starts the loader
    */
    var startSpin = function () {
      if (!$scope.spinneractive) {
        usSpinnerService.spin('spinner-1');
        $scope.startcounter++;
      }
    };
    /** Stops the loader*/
    var stopSpin = function () {
      if ($scope.spinneractive) {
        usSpinnerService.stop('spinner-1');
      }
    };

    //--SEARCH---recruiter
    var server_address = 'http://localhost:8080/recruiter';
    //Trade List
    $scope.tradeList = [
      "Security guard", "Air Craft Cleaner"
    ];

    //English Level
    //English Level
    $scope.englishLevel = [
      { "level": "Low", "value": 1 },
      { "level": "Average", "value": 2 },
      { "level": "Good", "value": 3 },
      { "level": "Excelent", "value": 2 }
    ];

    $scope.seenLevels = [
      { "level": "UNSEEN", "value": "UNSEEN" }
    ];

    //Qualification List
    $scope.qualifictionList = [
      "Under Matriculation", "Matriculation", "Enter", "Graduation", "Masters", "Mphil", "PHD"
    ];

    /**@author AbdulRehman
     * Method generates any ints array till the last int is provided
     * @param {lastInt} lastInt Last int till you want to generate
     */
    var generateInts = function (lastInt) {
      var intArr = [];
      for (var i = 0; i < lastInt; i++) {
        intArr[i] = i;
      }
      return intArr;
    }
    //Integers to be used anywher in the application
    $scope.integers = generateInts(100);


    $scope.weight;
    $scope.age;
    $scope.height;
    $scope.phone_num;
    $scope.cnic;
    $scope.english_speaking_level;
    $scope.english_writing_level;
    $scope.seenLevel;
    $scope.trade;
    $scope.qualification;

    $scope.searcObj = {
      age: 0,
      height: 0,
      phone_num: 0,
      cnic: 0,
      english_speaking_level: 0,
      english_writing_level: 0,
      seenLevel:"",
      trade: "",
      qualification: "",
    }

    console.log("--in -> ManagementController")



    $scope.searchUser = function (searcObj, isResultSearch) {
      //checking is this for user-result search inputs
      if (isResultSearch == true && (!searcObj || searcObj.phone_num === null || searcObj.phone_num < 1)
        && (searcObj.cnic === null || searcObj.cnic < 1)) {
        alert("Please enter value");
        return;
      }
      var data = {
        age: searcObj.age ?  searcObj.age : 0,
        height: searcObj.height ? searcObj.height : 0,
        phone_num: searcObj.phone_num ? searcObj.phone_num : 0,
        cnic: searcObj.cnic ? searcObj.cnic : 0,
        english_speaking_level: searcObj.english_speaking_level ? searcObj.english_speaking_level.value : 0,
        english_writing_level: searcObj.english_writing_level ? searcObj.english_writing_level.value : 0,
        seen_by:searcObj.seenLevel? searcObj.seenLevel.value : "",
        trade: searcObj.trade,
        qualification: searcObj.qualification
      }

      KHMHttpService.searchApplicant(data, isResultSearch, function successCallback(response) {
        if (response.data.errorCode === "401") {
          $state.go('signin');
        } else if (response.data.errorCode === "200" && response.data.message === "") {
          $scope.loading = 0;
          $scope.searchTable = response.data.data;
        } else {
          console.dir(response);
        }
      }, function errorCallback(response) {
        console.log(prefix + ".importProjectRest:" + response)
        $scope.loading = 0;
        callBack(false)
      });
    }

    $scope.downloadSearch = function (searchObj) {
      KHMHttpService.downloadSearchApplicants(searchObj);
    }

    $scope.downloadOverall = function () {
      KHMHttpService.downloadAllApplicants();
    }

    $scope.isLogin = function () {
      if (authorizeService.getClientID() !== null && authorizeService.getToken() !== null) {
        return true;
      }
      return false;
    }


    $scope.searchTable = [];
    $scope.searchTable_1 = [
      {
        "date_of_birth": 123,
        "passport_expiry": 123,
        "name": "Saim Rashid",
        "age": 30,
        "height": 6,
        "email": "abdulrehman@gmail.com",
        "trade": "eng",
        "qualification": " MIT",
        "weight": 140,
        "city": " isd",
        "country": " pk",
        "passport": 77777,
        "overseas_experience": " overseas",
        "local_experience": " local",
        "phone_num": 30356,
        "second_phone_num": 3035659264,
        "english_speaking_level": 2,
        "english_writing_level": 1,
        "english_listening_level": 3,
        "social_media_interaction": 1,
        "cnic": 1
      }];

    /***********************Manage Jobs */

    $scope.jobs = jobs;

    //accounts 
    $scope.addJob = function () {
      $scope.job = {
        id: $scope.jobs.length + 1,
        title: '',
        salary: "",
        company: '',
        location: '',
        requirements:''
      };
      $scope.jobs.push($scope.job);
    };

    $scope.applyJob = function () {
      $state.go('apply');
    };

    $scope.cancelJob = function (index) {
      $scope.jobs.splice(index, 1);
    };

    $scope.updateJobs_ = function (project) {
      console.log(project)
    }


    /**Deletes the account and  */
    $scope.updateJob = function (job) {
      if (job) {
        try {
          KHMHttpService.addJob(job, function successCallback(response) {
            if (response.data.errorCode === "401") {
              $state.go('signin');
            } else if (response.data.errorCode === "200" && response.data.message === "") {
              $scope.loading = 0;
            } else {
              console.dir(response);
            }
          }, function errorCallback(response) {
            console.log(prefix + ".importProjectRest:" + response)
            $scope.loading = 0;
            callBack(false)
          });
        } catch (error) {
          console.log(prefix + ".getAccounts:" + error)
        }
      }
    };

    /**Deletes the account and  */
    $scope.deleteJob = function (index) {
      var account = $scope.jobs[index];
      console.log(account)
      if (account) {
        if (confirm("Do you really want to delete the Job :(") == true) {
          try {
            KHMHttpService.deleteJob(account, function successCallback(response) {
              if (response.data.errorCode === "401") {
                $state.go('signin');
              } else if (response.data.errorCode === "200" && response.data.message === "") {
                $scope.loading = 0;
                $scope.jobs.splice(index, 1);
              } else {
                console.dir(response);
              }
            }, function errorCallback(response) {
              console.log(prefix + ".importProjectRest:" + response)
              $scope.loading = 0;
              callBack(false)
            });
          } catch (error) {
            console.log(prefix + ".getAccounts:" + error)
          }
        }
      }
    };

    //Updateing UI controls
    KHMHttpService.getUIControls(function successCallback(response) {
      if (response.data.errorCode === "401") {
        alert("Please refresh the page");
      } else if (response.data.errorCode === "200" && response.data.message === "") {
        var data = response.data.data;
        $scope.tradeList = data.trade;
        $scope.englishLevel = data.speaking_level;
        $scope.integers = data.height;
        $scope.qualifictionList = data.qualification;
        
      } else {
        console.dir(response);
        deferred.resolve(jobs);
      }
    }, function errorCallback(response) {
      console.log(prefix + ".importProjectRest:" + response)
      $scope.loading = 0;
      callBack(false)
    }
    )


  }




})();
