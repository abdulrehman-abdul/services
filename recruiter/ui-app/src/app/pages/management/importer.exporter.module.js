/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.importer', ['angular-spinkit'])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('importer', {
          url: '/importer',
          abstract:true,
          resolve:{
            'jobs': function($q,$state,authorizeService,importerAnalyzerService,KHMHttpService){
              console.log("validating user");
               var deferred = $q.defer();
               authorizeService.authorizeToken(function(response){
                    if(response === false){
                      $state.go('signin');
                    }
                    else{
                      var jobs = [];
                      // deferred.resolve(accounProjects);

                      console.log("---------------------------------defffereeed");
                      KHMHttpService.getJobs(function successCallback(response) {
                        if (response.data.errorCode === "401") {
                          $state.go('signin');
                        }else if (response.data.errorCode === "200" && response.data.message === "") {
                          jobs = response.data.data;
                          deferred.resolve(jobs);
                        } else{
                          console.dir(response);
                          deferred.resolve(jobs);
                        }
                      }, function errorCallback(response) {
                        console.log(prefix + ".importProjectRest:" + response)
                        $scope.loading = 0;
                        callBack(false)
                      }
                        
                      //   function(jobs){
                        
                      //   if(jobs.length>0){
                      //     importerAnalyzerService.getProjects(function(projects){
                      //       accounProjects.accounts = accounts;
                      //       accounProjects.projects = projects;
                      //       deferred.resolve(accounProjects);
                      //     })
                      //   }else{
                      //     deferred.resolve(accounProjects);
                      //   }
                      // }
                      
                      
                      )
                    }
              })
              return deferred.promise;
            }
          },
          template : '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
          controller: 'ManagementCtrl'     ,
          title: 'Management',
          sidebarMeta: {
            icon: 'ion-grid',
            order: 100,
          }
     
        }).state('importer.search', {
          url: '/search',
          templateUrl: 'app/pages/management/search/tables.html',
          title: 'Search',
          sidebarMeta: {
            order: 0,
          },
        }).state('importer.jobs', {
          url: '/managejobs',
          templateUrl: 'app/pages/management/search/managejobs.html',
          title: 'Manage Jobs',
          sidebarMeta: {
            order: 0,
          },
        })
        ;
        ;
    $urlRouterProvider.when('/importer','/importer/search');
  }

})();
