'use strict';

var app = angular.module('BlurAdmin');

app.service('importerAnalyzerService', function ($http,authorizeService) {
    this.getAccounts = function(callBack){
      try {
          var req = {
            method: 'GET',
            url: 'http://localhost:8080/api/data/get/account',
            headers: {
              'Content-Type': 'application/json',
              'Authorization':authorizeService.getToken(),
              'clientId': authorizeService.getClientID()
            }
          }
          $http(req).then(function successCallback(response) {
              if(response.data.status === 200){
                var accounts = response.data.data.ppmaccounts.filter(function(account){
                  return account.activate === true;
                })
                callBack(accounts)
              }else if(response.data.status === 202 && response.data.message === "UN_AUTHORIZE"){
                  callBack([])
              }else{
                callBack([])
              }
            }, function errorCallback(response) {
              callBack([])
          });
      
      } catch (error) {
        console.log(error)
        callBack([])
      }
    };

/**
 * Gets the all projects and stores in $scope.projects
 */
    this.getProjects = function(callBack){
      try {
          var req = {
            method: 'GET',
            url: 'http://localhost:8080/api/data/project/get/all',
            headers: {
              'Content-Type': 'application/json',
              'Authorization':authorizeService.getToken(),
              'clientId': authorizeService.getClientID()
            }
          }
          $http(req).then(function successCallback(response) {
              if(response.data.status === 200){

                var data = response.data.data.filter(function(project){
                  return project["account"]["activate"] === true;
                })

                //var data = response.data.data;
                var projects = [];
                for(var i=0; i<data.length; i++){
                  var obj = data[i];
                  var project = {};
                  project.id = obj.project_id;
                  project.name = obj.project.name;
                  project.description = obj.project.expand;
                  project.key = obj.project.key;
                  project.domain = obj.project.self;
                  project.import = {};
                  project.import.status = obj.import_status;
                  project.import.action = "Import";
                  project.analyse = {};
                  project.analyse.status = obj.search_status;
                  project.analyse.action = "Analyse";
                  projects.push(project);
                }
                callBack(projects)
              }else if(response.data.status === 202 && response.data.message === "UN_AUTHORIZE"){
                  callBack([])
              }else{
                callBack([])
              }
            }, function errorCallback(response) {
              callBack([])
          });
      
      } catch (error) {
        callBack([])
      }
    };

    /**Returns the projects by assigned to ppm account**/
    this.getProjectByAccount = function(ppmAccount, callBack){
      try {
          var req = {
            method: 'POST',
            url: 'http://localhost:8080/api/data/project/get',
            headers: {
              'Content-Type': 'application/json',
              'Authorization':authorizeService.getToken(),
              'clientId': authorizeService.getClientID()
            },
            data:{ppmaccount:ppmAccount}
          }
          $http(req).then(function successCallback(response) {
              if(response.data.status === 200){

                var data = response.data.data.filter(function(project){
                  return project["account"]["activate"] === true;
                })

                //var data = response.data.data;
                var projects = [];
                for(var i=0; i<data.length; i++){
                  var obj = data[i];
                  var project = {};
                  project.id = obj.project_id;
                  project.name = obj.project.name;
                  project.description = obj.project.expand;
                  project.key = obj.project.key;
                  project.domain = obj.project.self;
                  project.import = {};
                  project.import.status = obj.import_status;
                  project.import.action = "Import";
                  project.analyse = {};
                  project.analyse.status = obj.search_status;
                  project.analyse.action = "Analyse";
                  projects.push(project);
                }
                callBack(projects)
              }else if(response.data.status === 202 && response.data.message === "UN_AUTHORIZE"){
                  callBack([])
              }else{
                callBack([])
              }
            }, function errorCallback(response) {
              callBack([])
          });
      
      } catch (error) {
        callBack([])
      }
    } 
    
});