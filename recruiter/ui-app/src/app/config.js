'use strict';

var app = angular.module('BlurAdmin');

app.constant('config', (function() {
  // Define your variable
  var SERVER = '34.205.246.52';
  // var SERVER = 'localhost';
  var PORT = '8080';
  var PROTOCOL = "http://";
  // Use the variable in your constants
  return {
    SERVER: SERVER,
    PORT: PORT,
    // BASE_URL: PROTOCOL+SERVER+":"+PORT+"/"
    BASE_URL: PROTOCOL+SERVER+":"+PORT+"/recruiter/"
  }
})());