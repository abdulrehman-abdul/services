/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
      .controller('MsgCenterCtrl', MsgCenterCtrl);



      
  /** @ngInject */
  function MsgCenterCtrl($scope, $sce, KHMHttpService, $state) {
    $scope.users = {
      0: {
        name: 'Vlad',
      },
      1: {
        name: 'Kostya',
      },
      2: {
        name: 'Andrey',
      },
      3: {
        name: 'Nasta',
      }
    };


   var prefix = "MsgCenterCtrl";

    $scope.notifications = [
      {
        userId: 0,
        template: '&name AbdulAbdulAbdulAbdulAbdulAbdul.',
        time: '1 min ago'
      },
      {
        image: 'assets/img/shopping-cart.svg',
        template: 'New orders receivedNew orders receivedNew orders received.',
        time: '5 hrs ago'
      },
      {
        image: 'assets/img/shopping-cart.svg',
        template: '&name orders receivedNew orders receivedNew orders received.',
        time: '5 hrs ago'
      },
      {
        image: 'assets/img/shopping-cart.svg',
        template: 'New orders receivedNew orders receivedNew orders received.',
        time: '5 hrs ago'
      },
      {
        image: 'assets/img/shopping-cart.svg',
        template: 'New orders receivedNew orders receivedNew orders received.',
        time: '5 hrs ago'
      }
    ];



    //These notifications are not used @Abdul
    $scope.notifications_unused = [
      {
        userId: 0,
        template: '&name AbdulAbdulAbdulAbdulAbdulAbdul.',
        time: '1 min ago'
      },
      {
        userId: 1,
        template: '&name changedchangedchangedchangedchangedchanged.',
        time: '2 hrs ago'
      },
      {
        image: 'assets/img/shopping-cart.svg',
        template: 'New orders receivedNew orders receivedNew orders received.',
        time: '5 hrs ago'
      },
      {
        userId: 2,
        template: '&name replied to your comment.',
        time: '1 day ago'
      },
      {
        userId: 3,
        template: 'Today is &name\'s birthday.',
        time: '2 days ago'
      },
      {
        image: 'assets/img/comments.svg',
        template: 'New comments on your post.',
        time: '3 days ago'
      },
      {
        userId: 1,
        template: '&name invited you to join the event.',
        time: '1 week ago'
      }
    ];

    $scope.messages = [
      {
        userId: 3,
        text: 'After you get up and running, you can place Font Awesome icons just about...',
        time: '1 min ago'
      },
      {
        userId: 0,
        text: 'You asked, Font Awesome delivers with 40 shiny new icons in version 4.2.',
        time: '2 hrs ago'
      },
      {
        userId: 1,
        text: 'Want to request new icons? Here\'s how. Need vectors or want to use on the...',
        time: '10 hrs ago'
      },
      {
        userId: 2,
        text: 'Explore your passions and discover new ones by getting involved. Stretch your...',
        time: '1 day ago'
      },
      {
        userId: 3,
        text: 'Get to know who we are - from the inside out. From our history and culture, to the...',
        time: '1 day ago'
      },
      {
        userId: 1,
        text: 'Need some support to reach your goals? Apply for scholarships across a variety of...',
        time: '2 days ago'
      },
      {
        userId: 0,
        text: 'Wrap the dropdown\'s trigger and the dropdown menu within .dropdown, or...',
        time: '1 week ago'
      }
    ];

    $scope.getMessage = function(msg) {
      var text = msg.template;
      if (msg.userId || msg.userId === 0) {
        text = text.replace('&name', '<strong>' + $scope.users[msg.userId].name + '</strong>');
      }
      return $sce.trustAsHtml(text);
    };



    $scope.notifications = [
      // {
      //   userId: 0,
      //   template: '&name AbdulAbdulAbdulAbdulAbdulAbdul.',
      //   time: '1 min ago'
      // },
      // {
      //   image: 'assets/img/shopping-cart.svg',
      //   template: 'New orders receivedNew orders receivedNew orders received.',
      //   time: '5 hrs ago'
      // },
      // {
      //   image: 'assets/img/shopping-cart.svg',
      //   template: '&name orders receivedNew orders receivedNew orders received.',
      //   time: '5 hrs ago'
      // },
      // {
      //   image: 'assets/img/shopping-cart.svg',
      //   template: 'New orders receivedNew orders receivedNew orders received.',
      //   time: '5 hrs ago'
      // },
      // {
      //   image: 'assets/img/shopping-cart.svg',
      //   template: 'New orders receivedNew orders receivedNew orders received.',
      //   time: '5 hrs ago'
      // }
    ];


    $scope.getUnSeenNotif = function(msg) {
      
      
    KHMHttpService.getUnSeenApplicants(function successCallback(response) {
      console.log("---message centre---");
      if (response.data.errorCode === "401") {
        $state.go('signin');
      } else
       if (response.data.errorCode === "200" && response.data.message === "") {
        var dataObj = response.data.data;
        var newNotifications = [];
        for(var i=0; i<dataObj.length; i++){
          var applicantInfo =  {
            image: 'assets/img/shopping-cart.svg',
            template: 'New orders receivedNew orders receivedNew orders received.',
            time: '5 hrs ago'
          };
          
          var candidate = dataObj[i];
          applicantInfo.template = candidate.name+" From "+candidate.city+", "+candidate.country;
          applicantInfo.time = new Date(candidate.created_at);
          newNotifications.push(applicantInfo);
        }
        $scope.notifications = newNotifications;
      } else {
        console.dir(response);
      }
    }, function errorCallback(response) {
      console.log(prefix + ".KHMHttpService.getUnSeenApplicants:" + response)
      $scope.loading = 0;
      callBack(false)
    }
    )
    };
    $scope.getUnSeenNotif();
    setInterval($scope.getUnSeenNotif, 4000);


    $scope.goToNotificationDetails = function(msg) {
      $state.go('importer.search');
    };


    // function sayHi() {
    //   alert('Hello');
    // }
    
 


    /*

    0:
age: 85
city: "Gujr?nw?la"
cnic: 33333333
country: "Pakistan"
created_at: 1547935648000
date_of_birth: -1113543000000
email: ""
english_listening_level: 0
english_speaking_level: 3
english_writing_level: 3
height: 7
id: 5
local_experience: null
name: "212121"
overseas_experience: null
passport: 0
passport_expiry: null
phone_num: 121221
qualification: "Masters"
second_phone_num: 0
seen_by: "UNSEEN"
social_media_interaction: 0
trade: "Security guard"
updated_at: 1547935648000
weight: 5
__proto__: Object
length: 1
__proto__: Array(0)
    */



  }
})();