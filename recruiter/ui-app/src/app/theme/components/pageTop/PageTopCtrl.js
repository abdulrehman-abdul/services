/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
      .controller('PageTopCtrl', PageTopCtrl);

  /** @ngInject */
  function PageTopCtrl($scope, $filter,$state,$http, editableOptions, editableThemes, authorizeService) {
    var prefix = "PageTopCtrl";
    var isLogedIn = false;
     $scope.loading = 0;//for loader
    $scope.spinneractive = false;

    $scope.menuToggle = "not-visible";
    $scope.toggleMenu = function(){
      console.log("--class", $scope.menuToggle)
      if($scope.menuToggle == "visible"){
        $scope.menuToggle = "not-visible";
      }else{
        $scope.menuToggle = "visible";
      }
      
    }
    
  
    /**
     * Starts the loader
    */
    var startSpin = function() {
      if (!$scope.spinneractive) {
        usSpinnerService.spin('spinner-1');
        $scope.startcounter++;
      }
    };
  /** Stops the loader*/
    var stopSpin = function() {
      if ($scope.spinneractive) {
        usSpinnerService.stop('spinner-1');
      }
    };


    //Accounts table functions
    /**Validates the account*/
    $scope.validateAccount = function(index) {
      var token = authorizeService.getToken();
     var clientId = authorizeService.getClientID()
      if(token && clientId ){
        isLogedIn = true;
      }
      return isLogedIn;
    };
    $scope.validateAccount();

    /**Invokes REST call for account validation and Returns the true or false in call back response.  */
    var callValidateRest = function(account,callBack){
      $scope.loading = 1;
      try {
          var req = {
            method: 'POST',
            url: 'http://localhost:8080/api/data/account/validate',
            headers: {
              'Content-Type': 'application/json',
              'Authorization':authorizeService.getToken(),
              'clientId': authorizeService.getClientID()
            },
            data: { ppmaccount: account}
          }
          $http(req).then(function successCallback(response) {
              if(response.data.status === 200 && response.data.error === false){
                $scope.loading = 0;
                account.activate = true;
                account.validated = true;
                account.action = "Validate";
              }else if(response.data.status === 202 && response.data.message === "UN_AUTHORIZE"){
                $scope.loading = 0;
                  $state.go('signin');
              }
            }, function errorCallback(response) {
              $scope.loading = 0;
              if(response.status == 403){
                $state.go('signin');
              }
              alert("Not validated, provide the correct information");
              
               
          });
      } catch (error) {
        $scope.loading = 0;
        throw error;
      }
    }

    
  





  }

})();
